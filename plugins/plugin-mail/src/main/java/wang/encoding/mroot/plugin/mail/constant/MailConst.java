/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.mail.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import static wang.encoding.mroot.plugin.mail.constant.MailConst.MAIL_PREFIX;

/**
 * 邮箱配置文件
 *
 * @author ErYang
 */
@Component
@PropertySource(value = {"classpath:mail.properties"})
@ConfigurationProperties(prefix = MAIL_PREFIX)
@EnableCaching
@Getter
@Setter
public class MailConst {

    /**
     * mail 配置前缀
     */
    public static final String MAIL_PREFIX = "mail";

    /**
     * 是否开启邮箱功能 true(开启)/false(关闭)
     */
    public static Boolean openMail;
    /**
     * 发送邮箱地址
     */
    private String fromMailAddress;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MailConst class

/* End of file MailConst.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/mail/constant/MailConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
