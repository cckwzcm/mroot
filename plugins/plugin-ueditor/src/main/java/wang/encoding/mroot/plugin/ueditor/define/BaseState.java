/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.define;

import java.util.HashMap;
import java.util.Map;

import wang.encoding.mroot.plugin.ueditor.Encoder;


/**
 * 状态
 *
 * @author ErYang
 */
public class BaseState implements State {

    private boolean state = false;
    private String info = null;

    private Map<String, String> infoMap = new HashMap<String, String>();

    public BaseState() {
        this.state = true;
    }

    public BaseState(boolean state) {
        this.setState(state);
    }

    // -------------------------------------------------------------------------------------------------

    public BaseState(boolean state, String info) {
        this.setState(state);
        this.info = info;
    }

    // -------------------------------------------------------------------------------------------------

    public BaseState(boolean state, int infoCode) {
        this.setState(state);
        this.info = AppInfo.getStateInfo(infoCode);
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public Boolean isSuccess() {
        return this.state;
    }

    public void setState(boolean state) {
        this.state = state;
    }

    public void setInfo(String info) {
        this.info = info;
    }

    public void setInfo(int infoCode) {
        this.info = AppInfo.getStateInfo(infoCode);
    }

    @Override
    public String toJSONString() {
        return this.toString();
    }

    @Override
    public String toString() {
        String key;
        String stateVal = this.isSuccess() ? AppInfo.getStateInfo(AppInfo.SUCCESS) : this.info;
        StringBuilder builder = new StringBuilder();
        builder.append("{\"state\": \"").append(stateVal).append("\"");
        for (String s : this.infoMap.keySet()) {
            key = s;
            builder.append(",\"").append(key).append("\": \"").append(this.infoMap.get(key)).append("\"");
        }
        builder.append("}");
        return Encoder.toUnicode(builder.toString());
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void putInfo(String name, String val) {
        this.infoMap.put(name, val);
    }

    @Override
    public void putInfo(String name, Long val) {
        this.putInfo(name, val + "");
    }

}

// -----------------------------------------------------------------------------------------------------

// End BaseState class

/* End of file BaseState.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/define/BaseState.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
