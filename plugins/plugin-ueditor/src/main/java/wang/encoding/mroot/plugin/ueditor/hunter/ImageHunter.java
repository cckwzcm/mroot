package wang.encoding.mroot.plugin.ueditor.hunter;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import wang.encoding.mroot.plugin.ueditor.define.AppInfo;
import wang.encoding.mroot.plugin.ueditor.define.BaseState;
import wang.encoding.mroot.plugin.ueditor.define.MimeType;
import wang.encoding.mroot.plugin.ueditor.define.MultiState;
import wang.encoding.mroot.plugin.ueditor.define.State;
import wang.encoding.mroot.plugin.ueditor.upload.StorageManager;

/**
 * 图片抓取器
 *
 * @author ErYang
 */
public class ImageHunter {

    private List<String> allowTypes;
    private long maxSize;

    private List<String> filters;

    /**
     * @param conf Map<String, Object>
     */
    public ImageHunter(final Map<String, Object> conf) {
        this.maxSize = (Long) conf.get("maxSize");
        this.allowTypes = Arrays.asList((String[]) conf.get("allowFiles"));
        this.filters = Arrays.asList((String[]) conf.get("filter"));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取
     *
     * @param list String[]
     * @return State
     */
    public State capture(final String[] list) {
        MultiState state = new MultiState(true);
        for (String source : list) {
            state.addState(captureRemoteData(source));
        }
        return state;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取数据
     *
     * @param urlStr String
     * @return State
     */
    private State captureRemoteData(final String urlStr) {
        HttpURLConnection connection;
        URL url;
        String suffix;
        try {
            url = new URL(urlStr);
            if (!validHost(url.getHost())) {
                return new BaseState(false, AppInfo.PREVENT_HOST);
            }
            connection = (HttpURLConnection) url.openConnection();
            connection.setInstanceFollowRedirects(true);
            connection.setUseCaches(true);
            if (!validContentState(connection.getResponseCode())) {
                return new BaseState(false, AppInfo.CONNECTION_ERROR);
            }
            suffix = MimeType.getSuffix(connection.getContentType());
            if (!validFileType(suffix)) {
                return new BaseState(false, AppInfo.NOT_ALLOW_FILE_TYPE);
            }
            if (!validFileSize(connection.getContentLength())) {
                return new BaseState(false, AppInfo.MAX_SIZE);
            }
            State state = StorageManager.saveFileByInputStream(connection.getInputStream(), urlStr);
            if (state.isSuccess()) {
                state.putInfo("source", urlStr);
            }
            return state;
        } catch (Exception e) {
            return new BaseState(false, AppInfo.REMOTE_FAIL);
        }
    }

    // -------------------------------------------------------------------------------------------------

    private boolean validHost(String hostname) {
        return !filters.contains(hostname);
    }

    // -------------------------------------------------------------------------------------------------

    private boolean validContentState(int code) {
        return HttpURLConnection.HTTP_OK == code;
    }

    // -------------------------------------------------------------------------------------------------

    private boolean validFileType(String type) {
        return this.allowTypes.contains(type);
    }

    // -------------------------------------------------------------------------------------------------

    private boolean validFileSize(int size) {
        return size < this.maxSize;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ImageHunter class

/* End of file ImageHunter.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/hunter/ImageHunter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
