/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor;


import com.alibaba.fastjson.JSONObject;
import com.alibaba.fastjson.serializer.SerializerFeature;
import org.springframework.beans.factory.annotation.Autowired;
import wang.encoding.mroot.plugin.ueditor.config.UbEditorProperties;
import wang.encoding.mroot.plugin.ueditor.define.ActionMap;
import wang.encoding.mroot.plugin.ueditor.define.AppInfo;
import wang.encoding.mroot.plugin.ueditor.define.BaseState;
import wang.encoding.mroot.plugin.ueditor.define.State;
import wang.encoding.mroot.plugin.ueditor.hunter.FileManager;
import wang.encoding.mroot.plugin.ueditor.hunter.ImageHunter;
import wang.encoding.mroot.plugin.ueditor.upload.Uploader;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;


/**
 * 处理
 *
 * @author ErYang
 */
public class ActionEnter {

    @Autowired
    private UbEditorProperties ueditorProperties;

    private ConfigManager configManager;

    private HttpServletRequest request = null;

    private String actionType = null;

    /**
     * ActionEnter
     *
     * @param configManager ConfigManager
     */
    public ActionEnter(final ConfigManager configManager) {
        this.configManager = configManager;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理
     *
     * @param request HttpServletRequest
     * @return String
     */
    public String exec(final HttpServletRequest request) {
        this.request = request;
        this.actionType = request.getParameter("action");
        String callbackName = this.request.getParameter("callback");

        if (null != callbackName) {
            if (!validCallbackName(callbackName)) {
                return new BaseState(false, AppInfo.ILLEGAL).toJSONString();
            }
            return callbackName + "(" + this.invoke() + ");";
        } else {
            return this.invoke();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理
     *
     * @return String
     */
    public String invoke() {
        if (null == actionType || !ActionMap.MAPPING.containsKey(actionType)) {
            return new BaseState(false, AppInfo.INVALID_ACTION).toJSONString();
        }
        if (null == this.configManager || !this.configManager.valid()) {
            return new BaseState(false, AppInfo.CONFIG_ERROR).toJSONString();
        }
        State state = null;
        int actionCode = ActionMap.getType(this.actionType);

        Map<String, Object> conf;

        switch (actionCode) {
            case ActionMap.CONFIG:
                return JSONObject.toJSONString(this.configManager.getAllConfig(),
                        SerializerFeature.WriteMapNullValue,
                        SerializerFeature.WriteNullListAsEmpty,
                        SerializerFeature.WriteNullStringAsEmpty,
                        SerializerFeature.WriteNullNumberAsZero,
                        SerializerFeature.WriteNullBooleanAsFalse);
            case ActionMap.UPLOAD_IMAGE:
            case ActionMap.UPLOAD_SCRAWL:
            case ActionMap.UPLOAD_VIDEO:
            case ActionMap.UPLOAD_FILE:
                conf = this.configManager.getConfig(actionCode);
                state = new Uploader(request, conf).doExec();
                break;
            case ActionMap.CATCH_IMAGE:
                conf = configManager.getConfig(actionCode);
                String[] list = this.request.getParameterValues((String) conf.get("fieldName"));
                state = new ImageHunter(conf).capture(list);
                break;
            case ActionMap.LIST_IMAGE:
            case ActionMap.LIST_FILE:
                conf = configManager.getConfig(actionCode);
                int start = this.getStartIndex();
                String marker = this.getMarker();
                state = new FileManager(conf).listFile(start, marker);
                break;
            default:
                break;
        }
        assert state != null;
        return state.toJSONString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 开始
     *
     * @return Integer
     */
    public Integer getStartIndex() {
        String start = this.request.getParameter("start");
        try {
            return Integer.parseInt(start);
        } catch (Exception e) {
            return 0;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到
     *
     * @return String
     */
    public String getMarker() {
        return this.request.getParameter("marker");
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * callback参数验证
     *
     * @param name String
     * @return boolean
     */
    private Boolean validCallbackName(final String name) {
        String match = "^[a-zA-Z_]+[\\w0-9_]*$";
        return name.matches(match);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ActionEnter class

/* End of file ActionEnter.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/ActionEnter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
