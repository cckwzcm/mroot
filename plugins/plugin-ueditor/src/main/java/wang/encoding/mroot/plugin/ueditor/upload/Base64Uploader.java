/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.ueditor.upload;

import java.util.Map;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.codec.binary.Base64;

import wang.encoding.mroot.plugin.ueditor.PathFormat;
import wang.encoding.mroot.plugin.ueditor.define.AppInfo;
import wang.encoding.mroot.plugin.ueditor.define.BaseState;
import wang.encoding.mroot.plugin.ueditor.define.FileType;
import wang.encoding.mroot.plugin.ueditor.define.State;


/**
 * Base64上传
 *
 * @author ErYang
 */
public class Base64Uploader {

    /**
     * 保存
     *
     * @param request HttpServletRequest
     * @param conf    Map<String, Object>
     * @return State
     */
    public State save(final HttpServletRequest request, final Map<String, Object> conf) {
        String filedName = (String) conf.get("fieldName");
        String fileName = request.getParameter(filedName);
        byte[] data = decode(fileName);
        long maxSize;
        maxSize = (Long) conf.get("maxSize");
        if (!validSize(data, maxSize)) {
            return new BaseState(false, AppInfo.MAX_SIZE);
        }

        String suffix = FileType.getSuffix("JPG");
        String savePath = PathFormat.parse((String) conf.get("savePath"),
                (String) conf.get("filename"));
        savePath = savePath + suffix;
        String rootPath = "";
        String physicalPath = rootPath + savePath;
        State storageState = StorageManager.saveBinaryFile(data, physicalPath);
        if (storageState.isSuccess()) {
            storageState.putInfo("url", PathFormat.format(savePath));
            storageState.putInfo("type", suffix);
            storageState.putInfo("original", "");
        }
        return storageState;
    }

    // -------------------------------------------------------------------------------------------------

    private byte[] decode(String content) {
        return Base64.decodeBase64(content);
    }

    private boolean validSize(byte[] data, long length) {
        return data.length <= length;
    }

}

// -----------------------------------------------------------------------------------------------------

// End Base64Uploader class

/* End of file Base64Uploader.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/ueditor/upload/Base64Uploader.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
