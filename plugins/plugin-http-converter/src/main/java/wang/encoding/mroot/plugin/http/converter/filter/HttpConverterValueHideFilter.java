/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.plugin.http.converter.filter;

import com.alibaba.fastjson.serializer.ValueFilter;
import wang.encoding.mroot.plugin.http.converter.filter.annotation.HttpConverterValueHide;

import java.lang.reflect.Field;


/**
 * 隐藏值过滤器
 *
 * @author ErYang
 */
public class HttpConverterValueHideFilter implements ValueFilter {


    /**
     * 处理隐藏值
     * @param object Object
     * @param name String
     * @param value Object
     * @return Object
     */
    @Override
    public Object process(Object object, String name, Object value) {
        if (null != object) {
            try {
                // 得到字段
                Field field = object.getClass().getDeclaredField(name);
                if (field.isAnnotationPresent(HttpConverterValueHide.class)) {
                    // HttpConverterValueHide 注解
                    HttpConverterValueHide valueHide = field.getDeclaredAnnotation(HttpConverterValueHide.class);
                    if (0 >= valueHide.length()) {
                        return object;
                    }
                    // 值
                    String fieldValue = String.valueOf(value);
                    char[] chars = fieldValue.toCharArray();

                    if (0 < valueHide.start()) {
                        return startHide(valueHide, chars);
                    } else {
                        return positionHide(valueHide, chars);
                    }
                }
            } catch (Exception e) {
                return value;
            }
        }
        return value;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 起始位置隐藏
     *
     * @param valueHide HttpConverterValueHide
     * @param chars     char[]
     * @return String
     */
    private String startHide(HttpConverterValueHide valueHide, char[] chars) {
        int length = chars.length;
        int i;
        for (i = 0; i < length; i++) {
            if (valueHide.start() - 1 <= i && i < valueHide.start() + valueHide.length() - 1) {
                chars[i] = valueHide.placeholder().charAt(0);
            }
        }
        return new String(chars);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 位置模型隐藏
     *
     * @param valueHide HttpConverterValueHide
     * @param chars     char[]
     * @return String
     */
    private String positionHide(HttpConverterValueHide valueHide, char[] chars) {
        int length = chars.length;
        int i;
        for (i = 0; i < length; i++) {
            switch (valueHide.position()) {
                case START:
                    // 前面
                    if (i < valueHide.length()) {
                        chars[i] = valueHide.placeholder().charAt(0);
                    }
                    break;
                case MIDDLE:
                    // 中间
                    int startPosition = chars.length / 2;
                    if (i >= startPosition - 1 && i < startPosition - 1 + valueHide.length()) {
                        chars[i] = valueHide.placeholder().charAt(0);
                    }
                    break;
                case END:
                    // 后面
                    if (i >= chars.length - valueHide.length()) {
                        chars[i] = valueHide.placeholder().charAt(0);
                    }
                    break;
                default:
                    // 中间
                    int defaultStartPosition = chars.length / 2;
                    if (i >= defaultStartPosition - 1 && i < defaultStartPosition - 1 + valueHide.length()) {
                        chars[i] = valueHide.placeholder().charAt(0);
                    }
                    break;
            }
        }
        return new String(chars);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HttpConverterValueHideFilter class

/* End of file HttpConverterValueHideFilter.java */
/* Location: ./src/main/java/wang/encoding/mroot/plugin/http/converter/filter/HttpConverterValueHideFilter.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
