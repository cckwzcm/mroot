/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.loggingevent.AdminLoggingEventGetVO;

import java.math.BigInteger;


/**
 * 后台 logback日志 Service 接口
 *
 * @author ErYang
 */
public interface AdminLoggingEventService {

    /**
     * 得到所有数目
     *
     * @return int
     */
    int getCount();

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminLoggingEventGetVO
     */
    AdminLoggingEventGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminLoggingEventGetVO>
     * @param adminLoggingEventGetVO AdminLoggingEventGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminLoggingEventGetVO>
     */
    IPage<AdminLoggingEventGetVO> list2page(@NotNull final Page<AdminLoggingEventGetVO> pageAdmin,
            @NotNull final AdminLoggingEventGetVO adminLoggingEventGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务记录
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean deleteById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 定时任务记录
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean deleteBatch(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminLoggingEventService interface

/* End of file AdminLoggingEventService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/AdminLoggingEventService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
