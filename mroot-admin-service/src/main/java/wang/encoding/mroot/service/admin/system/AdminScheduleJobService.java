/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.system;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;

import java.math.BigInteger;
import java.util.List;


/**
 * 后台 定时任务 Service 接口
 *
 * @author ErYang
 */
public interface AdminScheduleJobService {


    /**
     * 通过 ID 查询
     * @param id BigInteger ID
     * @return AdminScheduleJobGetVO
     */
    AdminScheduleJobGetVO getById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据名称查询 AdminScheduleJobGetVO
     *
     * @param title String 名称
     * @return AdminScheduleJobGetVO
     */
    AdminScheduleJobGetVO getByTitle(@NotNull final String title);

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据标识查询 AdminScheduleJobGetVO
     *
     * @param sole String 标识
     * @return AdminScheduleJobGetVO
     */
    AdminScheduleJobGetVO getBySole(@NotNull final String sole);

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 定时任务
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     * @return boolean
     */
    boolean save(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 定时任务
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     * @return boolean
     */
    boolean update(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 定时任务 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    boolean remove2StatusById(@NotNull final BigInteger id);

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除(更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray);

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminScheduleJobGetVO>
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminScheduleJobGetVO>
     */
    IPage<AdminScheduleJobGetVO> list2page(@NotNull final Page<AdminScheduleJobGetVO> pageAdmin,
            @NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO, @Nullable String orderByField, boolean isAsc);

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     *
     * @return String
     */
    String validationScheduleJob(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    int getMax2Sort();

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     *
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property String 字段
     * @param newValue Object 新值
     * @param oldValue Object 旧值
     * @return boolean true (不存在)/false(存在)
     */
    boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminScheduleJobGetVO 列表
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO
     * @return List
     */
    List<AdminScheduleJobGetVO> list(@NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO);

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminScheduleJobGetVO 列表
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO
     * @return List
     */
    List<AdminScheduleJobGetVO> list2Init(@NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobService interface

/* End of file ScheduleJobService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/system/ScheduleJob.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
