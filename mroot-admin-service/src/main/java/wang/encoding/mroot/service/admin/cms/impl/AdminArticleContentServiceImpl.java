/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.cms.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.bo.admin.entity.cms.articlecontent.AdminArticleContentBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.HibernateValidationUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.cms.ArticleContentDO;
import wang.encoding.mroot.service.admin.cms.AdminArticleContentService;
import wang.encoding.mroot.service.cms.ArticleContentService;
import wang.encoding.mroot.vo.admin.entity.cms.articlecontent.AdminArticleContentGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 后台 文章内容 Service 接口实现类
 *
 * @author ErYang
 */
@Service
public class AdminArticleContentServiceImpl implements AdminArticleContentService {


    private final ArticleContentService articleContentService;

    @Autowired
    @Lazy
    public AdminArticleContentServiceImpl(ArticleContentService articleContentService) {
        this.articleContentService = articleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return AdminArticleContentGetVO
     */
    @Override
    public AdminArticleContentGetVO getById(@NotNull final BigInteger id) {
        ArticleContentDO articleContentDO = articleContentService.getTById(id);
        if (null != articleContentDO) {
            return this.articleContentDO2AdminArticleContentGetVO(articleContentDO);
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 文章内容
     *
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     * @return boolean
     */
    @Override
    public boolean save(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO) {
        ArticleContentDO articleContentDO = BeanMapperComponent.map(adminArticleContentGetVO, ArticleContentDO.class);
        return articleContentService.saveByT(articleContentDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新 文章内容
     *
     * @param articleContentGetVO AdminArticleContentGetVO
     * @return boolean
     */
    @Override
    public boolean update(@NotNull final AdminArticleContentGetVO articleContentGetVO) {
        ArticleContentDO articleContentDO = BeanMapperComponent.map(articleContentGetVO, ArticleContentDO.class);
        return articleContentService.updateById(articleContentDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章内容 (更新状态)
     *
     * @param id BigInteger
     * @return boolean
     */
    @Override
    public boolean remove2StatusById(@NotNull final BigInteger id) {
        ArticleContentDO articleContentDO = new ArticleContentDO();
        articleContentDO.setId(id);
        articleContentDO.setState(StateEnum.DELETE.getKey());
        articleContentDO.setGmtModified(Date.from(Instant.now()));
        return articleContentService.remove2StatusById(articleContentDO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量 文章内容 (更新状态)
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        return articleContentService.removeBatch2UpdateStatus(idArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     *
     * 只有当排序字段有值，排序才会生效
     *
     * @param pageAdmin   Page<AdminArticleContentGetVO>
     * @param adminArticleContentGetVO AdminArticleContentGetVO   实体类 查询条件
     * @param orderByField   String 排序字段
     * @param isAsc  boolean 是否正序 默认正序
     *
     * @return IPage<AdminArticleContentGetVO>
     */
    @Override
    public IPage<AdminArticleContentGetVO> list2page(@NotNull final Page<AdminArticleContentGetVO> pageAdmin,
            @NotNull final AdminArticleContentGetVO adminArticleContentGetVO, @Nullable String orderByField,
            boolean isAsc) {
        Page<ArticleContentDO> articleContentDOPage = new Page<>(pageAdmin.getCurrent(), pageAdmin.getSize());
        ArticleContentDO articleContentDO = BeanMapperComponent.map(adminArticleContentGetVO, ArticleContentDO.class);
        articleContentService.list2page(articleContentDOPage, articleContentDO, orderByField, isAsc);
        if (null != articleContentDOPage.getRecords() && CollectionUtils
                .isNotEmpty(articleContentDOPage.getRecords())) {
            List<AdminArticleContentGetVO> list = ListUtils.newArrayList(articleContentDOPage.getRecords().size());
            pageAdmin.setRecords(list);
            for (ArticleContentDO articleContentInfoDO : articleContentDOPage.getRecords()) {
                AdminArticleContentGetVO articleContentGetVO = this
                        .articleContentDO2AdminArticleContentGetVO(articleContentInfoDO);
                list.add(articleContentGetVO);
            }
        }
        pageAdmin.setTotal(articleContentDOPage.getTotal());
        return pageAdmin;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hibernate Validation 验证
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     *
     * @return String
     */
    @Override
    public String validationArticleContent(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO) {
        AdminArticleContentBO articleContentBO = BeanMapperComponent
                .map(adminArticleContentGetVO, AdminArticleContentBO.class);
        return HibernateValidationUtils.validateEntity(articleContentBO);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到  AdminArticleContentGetVO 列表
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     * @return List
     */
    @Override
    public List<AdminArticleContentGetVO> list(@NotNull final AdminArticleContentGetVO adminArticleContentGetVO) {
        ArticleContentDO articleContentDO = BeanMapperComponent.map(adminArticleContentGetVO, ArticleContentDO.class);
        List<ArticleContentDO> list = articleContentService.listByT(articleContentDO);
        if (ListUtils.isNotEmpty(list)) {
            List<AdminArticleContentGetVO> listVO = new ArrayList<>();
            for (ArticleContentDO articleContentInfoDO : list) {
                AdminArticleContentGetVO articleContentGetVO = this
                        .articleContentDO2AdminArticleContentGetVO(articleContentInfoDO);
                listVO.add(articleContentGetVO);
            }
            return listVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * ArticleContentDO 转为 AdminArticleContentGetVO
     *
     * @param articleContentDO ArticleContentDO
     * @return AdminArticleContentGetVO
     */
    private AdminArticleContentGetVO articleContentDO2AdminArticleContentGetVO(
            @NotNull final ArticleContentDO articleContentDO) {
        AdminArticleContentGetVO adminArticleContentGetVO = BeanMapperComponent
                .map(articleContentDO, AdminArticleContentGetVO.class);
        if (null != adminArticleContentGetVO.getState()) {
            adminArticleContentGetVO.setStatus(StateEnum.getValueByKey(adminArticleContentGetVO.getState()));
        }
        if (null != adminArticleContentGetVO.getGmtCreateIp()) {
            adminArticleContentGetVO.setIp(IpUtils.intToIpv4String(adminArticleContentGetVO.getGmtCreateIp()));
        }
        return adminArticleContentGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminArticleContentServiceImpl class

/* End of file AdminArticleContentServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/admin/cms/impl/AdminArticleContentServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
