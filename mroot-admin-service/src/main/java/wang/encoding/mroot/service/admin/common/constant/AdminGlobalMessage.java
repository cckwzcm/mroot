/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.admin.common.constant;


import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;

/**
 * 存放全局提示变量
 *
 * @author ErYang
 */
public class AdminGlobalMessage {

    /**
     * 此用户已被禁用
     */
    public final static String LOGIN_DISABLE_FAILED = LocaleMessageSourceComponent
            .getMessage("message.user.login.disable.failed");
    /**
     * 此用户已被删除
     */
    public final static String LOGIN_DELETE_FAILED = LocaleMessageSourceComponent
            .getMessage("message.user.login.delete.failed");
    /**
     * 用户名或密码错误
     */
    public final static String LOGIN_FAILED = LocaleMessageSourceComponent.getMessage("message.user.login.failed");
    /**
     * 登录成功
     */
    public final static String LOGIN_SUCCEED = LocaleMessageSourceComponent.getMessage("message.user.login.succeed");
    /**
     * 验证码错误
     */
    public final static String VERIFY_CODE_FAILED = LocaleMessageSourceComponent
            .getMessage("login.verifyCode.required");

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminGlobalMessage class

/* End of file AdminGlobalMessage.java */
/* Location; ./src/main/java/wang/encoding/mroot/service/admin/common/constant/AdminGlobalMessage.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
