/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.system.maillog;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台电子邮箱记录实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminMailLogVO implements Serializable {

    private static final long serialVersionUID = 8433161175179347250L;


    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "system_mail_log";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "system_";
    /**
     *ID
     */
    public static final String ID = "id";
    /**
     *类型
     */
    public static final String CATEGORY = "category";
    /**
     *类型描述
     */
    public static final String CATEGORY_DESCRIPTION = "category_description";
    /**
     *模式
     */
    public static final String PATTERN = "pattern";
    /**
     *模式(文本;Html;附件;静态资源)
     */
    public static final String PATTERN_DESCRIPTION = "pattern_description";
    /**
     *发送邮箱
     */
    public static final String FROM_MAIL = "from_mail";
    /**
     *接收邮箱
     */
    public static final String TO_MAIL = "to_mail";
    /**
     *邮件标题
     */
    public static final String TITLE = "title";
    /**
     *邮件内容
     */
    public static final String CONTENT = "content";
    /**
     *耗时(单位：毫秒)
     */
    public static final String TIMES = "times";
    /**
     *发送结果(1发送中,2发送成功,3发送失败)
     */
    public static final String SEND_RESULT = "send_result";
    /**
     *状态(1是正常,2是禁用,3是删除)
     */
    public static final String STATE = "state";
    /**
     *创建时间
     */
    public static final String GMT_CREATE = "gmt_create";
    /**
     *创建IP
     */
    public static final String GMT_CREATE_IP = "gmt_create_ip";
    /**
     *修改时间
     */
    public static final String GMT_MODIFIED = "gmt_modified";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */


    /**
     * ID
     */
    private BigInteger id;
    /**
     * 类型
     */
    private Integer category;
    /**
     * 类型描述
     */
    private String categoryDescription;
    /**
     * 模式
     */
    private Integer pattern;
    /**
     * 模式(文本;Html;附件;静态资源)
     */
    private String patternDescription;
    /**
     * 发送邮箱
     */
    private String fromMail;
    /**
     * 接收邮箱
     */
    private String toMail;
    /**
     * 邮件标题
     */
    private String title;
    /**
     * 邮件内容
     */
    private String content;
    /**
     * 耗时(单位：毫秒)
     */
    private Integer times;
    /**
     * 发送结果(1发送中,2发送成功,3发送失败)
     */
    private String sendResult;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminMailLogVO class

/* End of file AdminMailLogVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/system/maillog/AdminMailLogVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
