/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.vo.admin.entity.system.schedulejoblog;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台定时任务记录实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminScheduleJobLogVO implements Serializable {


    private static final long serialVersionUID = 2573712774740075265L;

    /* 属性名称常量开始 */

    // -------------------------------------------------------------------------------------------------

    /**
     *表名
     */
    public static final String TABLE_NAME = "system_schedule_job_log";
    /**
     * 表前缀
     */
    public static final String TABLE_SUFFIX = "system_";
    /**
     *ID
     */
    public static final String ID = "id";
    /**
     *定时任务ID
     */
    public static final String SCHEDULE_JOB_ID = "schedule_job_id";
    /**
     *名称
     */
    public static final String TITLE = "title";
    /**
     *Spring Bean名称
     */
    public static final String BEAN_NAME = "bean_name";
    /**
     *方法名
     */
    public static final String METHOD_NAME = "method_name";
    /**
     *参数
     */
    public static final String PARAMS = "params";
    /**
     *Cron表达式
     */
    public static final String CRON_EXPRESSION = "cron_expression";
    /**
     *失败信息
     */
    public static final String ERROR_MESSAGE = "error_message";
    /**
     *耗时(单位：毫秒)
     */
    public static final String TIMES = "times";
    /**
     *执行结果(成功，失败)
     */
    public static final String EXECUTION_RESULT = "execution_result";
    /**
     *状态(1是正常,2是禁用,3是删除)
     */
    public static final String STATE = "state";
    /**
     *创建时间
     */
    public static final String GMT_CREATE = "gmt_create";
    /**
     *创建IP
     */
    public static final String GMT_CREATE_IP = "gmt_create_ip";
    /**
     *修改时间
     */
    public static final String GMT_MODIFIED = "gmt_modified";
    /**
     *备注
     */
    public static final String REMARK = "remark";

    // -------------------------------------------------------------------------------------------------

    /* 属性名称常量结束 */


    /**
     * ID
     */
    private BigInteger id;
    /**
     * 定时任务ID
     */
    private BigInteger scheduleJobId;
    /**
     * 名称
     */
    private String title;
    /**
     * Spring Bean名称
     */
    private String beanName;
    /**
     * 方法名
     */
    private String methodName;
    /**
     * 参数
     */
    private String params;
    /**
     * Cron表达式
     */
    private String cronExpression;
    /**
     * 失败信息
     */
    private String errorMessage;
    /**
     * 耗时(单位：毫秒)
     */
    private Integer times;
    /**
     * 执行结果(成功，失败)
     */
    private String executionResult;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;
    /**
     * 备注
     */
    private String remark;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminScheduleJobLogVO class

/* End of file AdminScheduleJobLogVO.java */
/* Location: ./src/main/java/wang/encoding/mroot/vo/admin/entity/system/schedulejoblog/AdminScheduleJobLogVO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
