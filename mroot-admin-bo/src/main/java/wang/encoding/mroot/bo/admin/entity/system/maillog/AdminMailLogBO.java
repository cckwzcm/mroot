/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.bo.admin.entity.system.maillog;


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台电子邮箱记录实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminMailLogBO implements Serializable {


    private static final long serialVersionUID = -3237760495059714362L;
    /**
     * ID
     */
    private BigInteger id;
    /**
     * 类型
     */
    @NotNull(message = "validation.category.range")
    @Range(min = 1, max = 4, message = "validation.category.range")
    private Integer category;
    /**
     * 类型描述
     */
    private String categoryDescription;
    /**
     * 模式
     */
    private Integer pattern;
    /**
     * 模式(文本;Html;附件;静态资源)
     */
    private String patternDescription;
    /**
     * 发送邮箱
     */
    private String fromMail;
    /**
     * 接收邮箱
     */
    private String toMail;
    /**
     * 邮件标题
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    private String title;
    /**
     * 邮件内容
     */
    private String content;
    /**
     * 耗时(单位：毫秒)
     */
    private Integer times;
    /**
     * 发送结果(1发送中,2发送成功,3发送失败)
     */
    private String sendResult;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
    private Integer state;
    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    private Date gmtCreate;
    /**
     * 创建IP
     */
    @NotNull(message = "validation.gmtCreateIp.pattern")
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MailLogBO class

/* End of file MailLogBO.java */
/* Location: ./src/main/java/wang/encoding/mroot/bo/admin/entity/system/maillog/MailLogBO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
