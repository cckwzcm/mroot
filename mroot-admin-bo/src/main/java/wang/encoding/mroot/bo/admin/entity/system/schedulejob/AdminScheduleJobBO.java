/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.bo.admin.entity.system.schedulejob;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import javax.validation.constraints.Pattern;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台定时任务实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class AdminScheduleJobBO implements Serializable {


    private static final long serialVersionUID = 986662667265307709L;
    /**
     * ID
     */
    private BigInteger id;
    /**
     * 标识
     */
    @Pattern(regexp = "^[a-zA-Z0-9_]{2,80}$", message = "validation.sole.pattern")
    private String sole;
    /**
     * 名称
     */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
    private String title;
    /**
     * Spring Bean名称
     */
    private String beanName;
    /**
     * 方法名
     */
    private String methodName;
    /**
     * 参数
     */
    private String params;
    /**
     * Cron表达式
     */
    private String cronExpression;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
    private Integer state;
    /**
     * 备注
     */
    @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
    private String remark;
    /**
     * 创建时间
     */
    @Past(message = "validation.gmtCreate.past")
    private Date gmtCreate;
    /**
     * 创建IP
     */
    @NotNull(message = "validation.gmtCreateIp.pattern")
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    @Past(message = "validation.gmtModified.past")
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobBO class

/* End of file ScheduleJobBO.java */
/* Location: ./src/main/java/wang/encoding/mroot/bo/admin/entity/system/schedulejob/ScheduleJobBO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
