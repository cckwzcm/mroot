/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.util.net.NetUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * request工具类
 *
 * @author ErYang
 */
public class HttpRequestUtils {

    /**
     * ajax标识
     */
    private static final String AJAX_HEADER = "x-requested-with";
    /**
     * ajax值
     */
    private static final String AJAX_HEADER_VALUE = "XMLHttpRequest";

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否ajax请求
     *
     * @param request request
     * @return Boolean
     */
    public static boolean isAjaxRequest(@NotNull final HttpServletRequest request) {
        // 如果是ajax请求响应头会有x-requested-with
        return null != request.getHeader(AJAX_HEADER) && request.getHeader(AJAX_HEADER)
                .equalsIgnoreCase(AJAX_HEADER_VALUE);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到用户的 ip
     *
     * @param request HttpServletRequest
     * @return String ip地址
     */
    public static String getIp(@NotNull final HttpServletRequest request) {
        String ip;
        if (null == request) {
            ip = LocaleMessageSourceComponent.getMessage("message.exception.data");
        } else {
            try {
                ip = request.getHeader("x-forwarded-for");
                String unknown = "unknown";
                if (null == ip || StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
                    ip = request.getHeader("Proxy-Client-IP");
                }

                if (null == ip || StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
                    ip = request.getHeader("WL-Proxy-Client-IP");
                }
                // natapp 内网穿透获取 IP
                if (null == ip || StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
                    ip = request.getHeader("X-Real-IP");
                }
                if (null == ip || StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
                    ip = request.getHeader("X-Natapp-Ip");
                }
                if (null == ip || StringUtils.isBlank(ip) || unknown.equalsIgnoreCase(ip)) {
                    ip = request.getRemoteAddr();
                }
            } catch (IllegalStateException e) {
                ip = LocaleMessageSourceComponent.getMessage("message.exception.data");
            }
        }
        String localHost = "127.0.0.1";
        String localHost2 = "0:0:0:0:0:0:0:1";
        if (localHost.equals(ip) || localHost2.equals(ip)) {
            // 根据网卡取本机配置的IP
            ip = NetUtils.getLocalHost();
        }
        return ip;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End HttpRequestUtils class

/* End of file HttpRequestUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/HttpRequestUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
