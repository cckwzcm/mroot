/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.config;


import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import com.baomidou.mybatisplus.extension.plugins.PerformanceInterceptor;
import com.baomidou.mybatisplus.extension.plugins.SqlExplainInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * mybatis plus 配置
 *
 * @author ErYang
 */
@Configuration
@EnableCaching
@MapperScan("wang.encoding.mroot.dao")
@Slf4j
public class MybatisPlusConfiguration {


    /**
     * 执行分析插件【生产环境开启】
     *
     * 设置 dev test 环境关闭
     */
    @Bean
    @Profile(value = {"dev", "test"})
    public SqlExplainInterceptor sqlExplainInterceptor() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>SqlExplainInterceptor设置<<<<<<<<");
        }
        return new SqlExplainInterceptor();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * SQL执行效率插件【生产环境关闭】
     * 设置 dev test 环境开启
     */
    @Bean
    @Profile(value = {"dev", "test"})
    public PerformanceInterceptor performanceInterceptor() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>PerformanceInterceptor设置<<<<<<<<");
        }
        return new PerformanceInterceptor();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>PaginationInterceptor设置<<<<<<<<");
        }
        return new PaginationInterceptor();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End MybatisPlusConfiguration class

/* End of file MybatisPlusConfiguration.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/config/MybatisPlusConfiguration.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
