/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.collection;

import com.google.common.collect.ObjectArrays;
import com.google.common.primitives.Doubles;
import com.google.common.primitives.Ints;
import com.google.common.primitives.Longs;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.number.NumberUtils;

import java.lang.reflect.Array;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Random;

/**
 * 数组工具类
 * 1. 创建Array的函数
 * 2. 数组的乱序与contact相加
 * 3. 从Array转换到Guava的底层为原子类型的List
 *
 * JDK Arrays的其他函数，如sort(), toString() 请直接调用
 * Common Lang3 ArrayUtils的其他函数，如subarray(),reverse(),indexOf(), 请直接调用
 *
 * @author ErYang
 *
 */
public class ArrayUtils {

    /**
     * 传入类型与大小创建数组
     *
     * Array.newInstance()的性能并不差
     *
     * @param type Class
     * @param length int
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] newArray(final Class<T> type, final int length) {
        return (T[]) Array.newInstance(type, length);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 从collection转为Array, 以 list.toArray(new String[0]); 最快 不需要创建list.size()的数组
     *
     * 本函数等价于list.toArray(new String[0]); 用户也可以直接用后者
     * https://shipilev.net/blog/2016/arrays-wisdom-ancients/
     *
     * @param col Collection
     * @param type Class
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] toArray(@NotNull final Collection<T> col, @NotNull final Class<T> type) {
        return col.toArray((T[]) Array.newInstance(type, 0));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Swaps the two specified elements in the specified array
     *
     * @param arr Object[]
     * @param i int
     * @param j int
     */
    private static void swap(@NotNull final Object[] arr, final int i, final int j) {
        Object tmp = arr[i];
        arr[i] = arr[j];
        arr[j] = tmp;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将传入的数组乱序
     *
     * @param array T[]
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] shuffle(@NotNull final T[] array) {
        if (null != array && 1 < array.length) {
            Random rand = new Random();
            return shuffle(array, rand);
        } else {
            return array;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 将传入的数组乱序
     *
     * @param array T[]
     * @param random Random
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] shuffle(@NotNull final T[] array, @NotNull final Random random) {
        if (null != array && 1 < array.length && null != random) {
            int i;
            for (i = array.length; i > 1; i--) {
                swap(array, i - 1, random.nextInt(i));
            }
        }
        return array;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加元素到数组头
     *
     * @param element T
     * @param array  T[]
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] concat(@Nullable final T element, @NotNull final T[] array) {
        return ObjectArrays.concat(element, array);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加元素到数组末尾
     *
     * @param array T[]
     * @param element T
     * @param <T> T
     * @return T[]
     */
    public static <T> T[] concat(@NotNull final T[] array, @Nullable final T element) {
        return ObjectArrays.concat(array, element);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 原版将数组转换为List
     * 注意转换后的List不能写入, 否则抛出UnsupportedOperationException
     *
     * guava Arrays#asList(Object...)
     *
     * @param a T
     * @param <T> T
     * @return List
     */
    @SafeVarargs
    public static <T> List<T> asList(final @NotNull T... a) {
        return Arrays.asList(a);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Arrays.asList()的加强版, 返回一个底层为原始类型int的List
     * 与保存Integer相比节约空间，同时只在读取数据时AutoBoxing
     *
     * Arrays#asList(Object...)
     * com.google.common.primitives.Ints#asList(int...)
     *
     *
     * @param backingArray int
     * @return List
     */
    public static List<Integer> intAsList(final int... backingArray) {
        return Ints.asList(backingArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Arrays.asList()的加强版, 返回一个底层为原始类型long的List
     * 与保存Long相比节约空间，同时只在读取数据时AutoBoxing
     *
     * Arrays#asList(Object...)
     * com.google.common.primitives.Longs#asList(long...)
     *
     * @param backingArray long
     * @return List
     */
    public static List<Long> longAsList(final long... backingArray) {
        return Longs.asList(backingArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Arrays.asList()的加强版, 返回一个底层为原始类型double的List
     * 与保存Double相比节约空间，同时也避免了AutoBoxing
     *
     * Arrays#asList(Object...)
     * com.google.common.primitives.Doubles#asList(double...)
     *
     * @param backingArray double
     * @return List
     */
    public static List<Double> doubleAsList(final double... backingArray) {
        return Doubles.asList(backingArray);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * String 数组 转换成 BigInteger 数组
     *
     * @param array String 数组
     * @return BigInteger 数组
     */
    public static BigInteger[] string2BigInteger(@NotNull final String[] array) {
        BigInteger[] idArray = ArrayUtils.newArray(BigInteger.class, array.length);
        BigInteger id;
        int i;
        for (i = 0; i < array.length; i++) {
            if (NumberUtils.isNumber(array[i])) {
                id = new BigInteger(array[i]);
                idArray[i] = id;
            }
        }
        return idArray;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArrayUtils class

/* End of file ArrayUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/collection/ArrayUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
