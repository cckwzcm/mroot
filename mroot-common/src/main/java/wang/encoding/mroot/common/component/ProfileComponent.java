/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.component;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.util.ObjectUtils;
import org.springframework.util.StringUtils;
import wang.encoding.mroot.common.enums.ProfileEnum;

import javax.annotation.PostConstruct;

/**
 * 环境配置
 *
 * @author ErYang
 */
@Component
@EnableCaching
@Slf4j
public class ProfileComponent {


    private final Environment environment;

    private static ProfileComponent profileComponent;

    @Autowired
    public ProfileComponent(Environment environment) {
        this.environment = environment;
    }

    /**
     * 得到当前环境 key
     *
     * @return String 环境 key
     */
    public static String getActiveProfile() {
        String active;
        String[] activeProfiles = profileComponent.environment.getActiveProfiles();
        if (ObjectUtils.isEmpty(activeProfiles)) {
            String[] defaultProfiles = profileComponent.environment.getDefaultProfiles();
            active = StringUtils.arrayToCommaDelimitedString(defaultProfiles);
        } else {
            active = StringUtils.arrayToCommaDelimitedString(activeProfiles);
        }
        return active;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到当前环境 描述
     *
     * @return String 环境 描述
     */
    public static String getActiveProfile2Description() {
        return ProfileEnum.getValueByKey(ProfileComponent.getActiveProfile());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 检验是否是开发环境
     *
     * @return Boolean
     */
    public static Boolean isDevProfile() {
        // 环境
        String activeProfile = ProfileComponent.getActiveProfile();
        return ProfileEnum.DEV.getKey().equals(activeProfile);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 赋值
     */
    @PostConstruct
    public void init() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>初始化[ProfileComponent]<<<<<<<<");
        }
        profileComponent = this;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ProfileComponent class

/* End of file ProfileComponent.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/component/ProfileComponent.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
