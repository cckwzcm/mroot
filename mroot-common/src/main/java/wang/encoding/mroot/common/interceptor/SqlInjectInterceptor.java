/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.interceptor;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Enumeration;


/**
 * 防止 sql 注入拦截器
 *
 * @author ErYang
 */
@Component
@Slf4j
public class SqlInjectInterceptor implements HandlerInterceptor {


    @Override
    public boolean preHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o) {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>SqlInjectInterceptor[{}]<<<<<<<<", httpServletRequest.getRequestURI());
        }
        Enumeration<String> names = httpServletRequest.getParameterNames();
        int i;
        int size;
        while (names.hasMoreElements()) {
            String name = names.nextElement();
            String[] values = httpServletRequest.getParameterValues(name);
            if (ArrayUtils.isNotEmpty(values)) {
                size = values.length;
                for (i = 0; i < size; i++) {
                    values[i] = this.clearXss(values[i]);
                }
            }
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void postHandle(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Object o,
            ModelAndView modelAndView) {

    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void afterCompletion(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse,
            Object o, Exception e) {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理字符转义
     *
     * @param value String
     * @return String
     */
    private String clearXss(String value) {
        if (StringUtils.isBlank(value)) {
            return value;
        }
        value = value.replace("《", "<").replace("》", ">");
        value = value.replace("（", ")").replace("）", ")");
        value = value.replace("\\(", "(").replace("\\)", ")");
        value = value.replace("‘", "'");
        value = value.replace("eval\\((.*)\\)", "");
        value = value.replace("[\\\"\\\'][\\s]*javascript:(.*)[\\\"\\\']", "\"\"");
        value = value.replace("script", "");
        return value;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End SqlInjectInterceptor class

/* End of file SqlInjectInterceptor.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/interceptor/SqlInjectInterceptor.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
