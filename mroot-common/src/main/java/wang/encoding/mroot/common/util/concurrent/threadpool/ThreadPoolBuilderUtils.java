package wang.encoding.mroot.common.util.concurrent.threadpool;


import org.apache.commons.lang3.Validate;

import java.util.concurrent.*;

/**
 * ThreadPool创建的工具类
 *
 * 对比JDK Executors中的newFixedThreadPool(), newCachedThreadPool(),newScheduledThreadPool, 提供更多有用的配置项
 *
 * @author ErYang
 */
public class ThreadPoolBuilderUtils {

    private static RejectedExecutionHandler defaultRejectHandler = new ThreadPoolExecutor.AbortPolicy();

    // -------------------------------------------------------------------------------------------------

    /**
     * @see FixedThreadPoolBuilder
     *
     * @return FixedThreadPoolBuilder
     */
    public static FixedThreadPoolBuilder fixedPool() {
        return new FixedThreadPoolBuilder();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * @see CachedThreadPoolBuilder
     *
     * @return CachedThreadPoolBuilder
     */
    public static CachedThreadPoolBuilder cachedPool() {
        return new CachedThreadPoolBuilder();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * @see ScheduledThreadPoolBuilder
     *
     * @return ScheduledThreadPoolBuilder
     */
    public static ScheduledThreadPoolBuilder scheduledPool() {
        return new ScheduledThreadPoolBuilder();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * @see QueueableCachedThreadPoolBuilder
     *
     * @return QueuableCachedThreadPoolBuilder
     */
    public static QueueableCachedThreadPoolBuilder queuableCachedPool() {
        return new QueueableCachedThreadPoolBuilder();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建FixedThreadPool.建议必须设置queueSize保证有界。
     *
     * 1. 任务提交时, 如果线程数还没达到poolSize即创建新线程并绑定任务(即poolSize次提交后线程总数必达到poolSize，不会重用之前的线程)
     *
     * poolSize默认为1，即singleThreadPool.
     *
     * 2. 第poolSize次任务提交后, 新增任务放入Queue中, Pool中的所有线程从Queue中take任务执行.
     *
     * Queue默认为无限长的LinkedBlockingQueue, 但建议设置queueSize换成有界的队列.
     *
     * 如果使用有界队列, 当队列满了之后,会调用RejectHandler进行处理, 默认为AbortPolicy，抛出RejectedExecutionException异常.
     * 其他可选的Policy包括静默放弃当前任务(Discard)，放弃Queue里最老的任务(DisacardOldest)，或由主线程来直接执行(CallerRuns).
     *
     * 3. 因为线程全部为core线程，所以不会在空闲时回收.
     */
    public static class FixedThreadPoolBuilder {

        private int poolSize = 1;
        private int queueSize = -1;

        private ThreadFactory threadFactory;
        private String threadNamePrefix;
        private Boolean daemon;

        private RejectedExecutionHandler rejectHandler;

        /**
         * Pool大小，默认为1，即singleThreadPool
         *
         * @param poolSize int
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setPoolSize(int poolSize) {
            Validate.isTrue(poolSize >= 1);
            this.poolSize = poolSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 不设置时默认为-1, 使用无限长的LinkedBlockingQueue.
         *
         * 为正数时使用ArrayBlockingQueue.
         *
         * @param queueSize int
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setQueueSize(int queueSize) {
            this.queueSize = queueSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadNamePrefix互斥, 优先使用ThreadFactory
         *
         * @param threadFactory ThreadFactory
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setThreadFactory(ThreadFactory threadFactory) {
            this.threadFactory = threadFactory;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与ThreadFactory互斥, 优先使用ThreadFactory
         *
         * @param threadNamePrefix String
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setThreadNamePrefix(String threadNamePrefix) {
            this.threadNamePrefix = threadNamePrefix;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadFactory互斥, 优先使用ThreadFactory
         *
         * 默认为NULL，不进行设置，使用JDK的默认值.
         *
         * @param daemon Boolean
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setDaemon(Boolean daemon) {
            this.daemon = daemon;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @param rejectHandler RejectedExecutionHandler
         * @return FixedThreadPoolBuilder
         */
        public FixedThreadPoolBuilder setRejectHanlder(RejectedExecutionHandler rejectHandler) {
            this.rejectHandler = rejectHandler;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        public ThreadPoolExecutor build() {
            BlockingQueue<Runnable> queue;
            if (queueSize < 1) {
                queue = new LinkedBlockingQueue<>();
            } else {
                queue = new ArrayBlockingQueue<>(queueSize);
            }

            threadFactory = createThreadFactory(threadFactory, threadNamePrefix, daemon);

            if (rejectHandler == null) {
                rejectHandler = defaultRejectHandler;
            }

            return new ThreadPoolExecutor(poolSize, poolSize, 0L, TimeUnit.MILLISECONDS, queue, threadFactory,
                    rejectHandler);
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建CachedThreadPool, maxSize建议设置
     *
     * 1. 任务提交时, 如果线程数还没达到minSize即创建新线程并绑定任务(即minSize次提交后线程总数必达到minSize, 不会重用之前的线程)
     *
     * minSize默认为0, 可设置保证有基本的线程处理请求不被回收.
     *
     * 2. 第minSize次任务提交后, 新增任务提交进SynchronousQueue后，如果没有空闲线程立刻处理，则会创建新的线程, 直到总线程数达到上限.
     *
     * maxSize默认为Integer.Max, 可以进行设置.
     *
     * 如果设置了maxSize, 当总线程数达到上限, 会调用RejectHandler进行处理, 默认为AbortPolicy, 抛出RejectedExecutionException异常.
     * 其他可选的Policy包括静默放弃当前任务(Discard)，或由主线程来直接执行(CallerRuns).
     *
     * 3. minSize以上, maxSize以下的线程, 如果在keepAliveTime中都poll不到任务执行将会被结束掉, keeAliveTimeJDK默认为10秒.
     * JDK默认值60秒太高，如高达1000线程时，要低于16QPS时才会开始回收线程, 因此改为默认10秒.
     */
    public static class CachedThreadPoolBuilder {

        private int minSize = 0;
        private int maxSize = Integer.MAX_VALUE;
        private int keepAliveSecs = 10;

        private ThreadFactory threadFactory;
        private String threadNamePrefix;
        private Boolean daemon;

        private RejectedExecutionHandler rejectHandler;

        public CachedThreadPoolBuilder setMinSize(int minSize) {
            this.minSize = minSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * Max默认Integer.MAX_VALUE的，建议设置
         *
         * @param maxSize int
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setMaxSize(int maxSize) {
            this.maxSize = maxSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * JDK默认值60秒太高，如高达1000线程时，要低于16QPS时才会开始回收线程, 因此改为默认10秒.
         *
         * @param keepAliveSecs int
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setKeepAliveSecs(int keepAliveSecs) {
            this.keepAliveSecs = keepAliveSecs;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadNamePrefix互斥, 优先使用ThreadFactory
         *
         * @param threadFactory ThreadFactory
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setThreadFactory(ThreadFactory threadFactory) {
            this.threadFactory = threadFactory;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadFactory互斥, 优先使用ThreadFactory
         *
         * @param threadNamePrefix String
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setThreadNamePrefix(String threadNamePrefix) {
            this.threadNamePrefix = threadNamePrefix;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadFactory互斥, 优先使用ThreadFactory
         *
         * 默认为NULL，不进行设置，使用JDK的默认值.
         *
         * @param daemon Boolean
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setDaemon(Boolean daemon) {
            this.daemon = daemon;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @param rejectHandler RejectedExecutionHandler
         * @return CachedThreadPoolBuilder
         */
        public CachedThreadPoolBuilder setRejectHanlder(RejectedExecutionHandler rejectHandler) {
            this.rejectHandler = rejectHandler;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @return ThreadPoolExecutor
         */
        public ThreadPoolExecutor build() {

            threadFactory = createThreadFactory(threadFactory, threadNamePrefix, daemon);

            if (rejectHandler == null) {
                rejectHandler = defaultRejectHandler;
            }

            return new ThreadPoolExecutor(minSize, maxSize, keepAliveSecs, TimeUnit.SECONDS, new SynchronousQueue<>(),
                    threadFactory, rejectHandler);
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建ScheduledPool
     */
    public static class ScheduledThreadPoolBuilder {

        private int poolSize = 1;
        private ThreadFactory threadFactory;
        private String threadNamePrefix;

        /**
         * 默认为1
         * @param poolSize int
         * @return ScheduledThreadPoolBuilder
         */
        public ScheduledThreadPoolBuilder setPoolSize(int poolSize) {
            this.poolSize = poolSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadNamePrefix互斥, 优先使用ThreadFactory
         *
         * @param threadFactory ThreadFactory
         * @return ScheduledThreadPoolBuilder
         */
        public ScheduledThreadPoolBuilder setThreadFactory(ThreadFactory threadFactory) {
            this.threadFactory = threadFactory;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @param threadNamePrefix String
         * @return ScheduledThreadPoolBuilder
         */
        public ScheduledThreadPoolBuilder setThreadNamePrefix(String threadNamePrefix) {
            this.threadNamePrefix = threadNamePrefix;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @return ScheduledThreadPoolExecutor
         */
        public ScheduledThreadPoolExecutor build() {
            threadFactory = createThreadFactory(threadFactory, threadNamePrefix, Boolean.TRUE);
            return new ScheduledThreadPoolExecutor(poolSize, threadFactory);
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 从Tomcat移植过来的可扩展可用Queue缓存任务的ThreadPool
     *
     * @see QueueableCachedThreadPoolUtils
     */
    public static class QueueableCachedThreadPoolBuilder {

        private int minSize = 0;
        private int maxSize = Integer.MAX_VALUE;
        private int keepAliveSecs = 10;
        private int queueSize = 100;

        private ThreadFactory threadFactory;
        private String threadNamePrefix;
        private Boolean daemon;

        private RejectedExecutionHandler rejectHandler;

        public QueueableCachedThreadPoolBuilder setMinSize(int minSize) {
            this.minSize = minSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        public QueueableCachedThreadPoolBuilder setMaxSize(int maxSize) {
            this.maxSize = maxSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * LinkedQueue长度, 默认100
         *
         * @param queueSize int
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setQueueSize(int queueSize) {
            this.queueSize = queueSize;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @param keepAliveSecs int
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setKeepAliveSecs(int keepAliveSecs) {
            this.keepAliveSecs = keepAliveSecs;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadNamePrefix互斥, 优先使用ThreadFactory
         *
         * @param threadFactory ThreadFactory
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setThreadFactory(ThreadFactory threadFactory) {
            this.threadFactory = threadFactory;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadFactory互斥, 优先使用ThreadFactory
         *
         * @param threadNamePrefix String
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setThreadNamePrefix(String threadNamePrefix) {
            this.threadNamePrefix = threadNamePrefix;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         * 与threadFactory互斥, 优先使用ThreadFactory
         *
         * 默认为NULL，不进行设置，使用JDK的默认值.
         *
         * @param daemon Boolean
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setDaemon(Boolean daemon) {
            this.daemon = daemon;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        /**
         *
         * @param rejectHandler RejectedExecutionHandler
         * @return QueueableCachedThreadPoolBuilder
         */
        public QueueableCachedThreadPoolBuilder setRejectHanlder(RejectedExecutionHandler rejectHandler) {
            this.rejectHandler = rejectHandler;
            return this;
        }

        // -------------------------------------------------------------------------------------------------

        public QueueableCachedThreadPoolUtils build() {

            threadFactory = createThreadFactory(threadFactory, threadNamePrefix, daemon);

            if (rejectHandler == null) {
                rejectHandler = defaultRejectHandler;
            }

            return new QueueableCachedThreadPoolUtils(minSize, maxSize, keepAliveSecs, TimeUnit.SECONDS,
                    new QueueableCachedThreadPoolUtils.ControllableQueue(queueSize), threadFactory, rejectHandler);
        }

        // -------------------------------------------------------------------------------------------------

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 优先使用threadFactory，否则如果threadNamePrefix不为空则使用自建ThreadFactory，否则使用defaultThreadFactory
     *
     * @param threadFactory ThreadFactory
     * @param threadNamePrefix String
     * @param daemon Boolean
     * @return ThreadFactory
     */
    private static ThreadFactory createThreadFactory(ThreadFactory threadFactory, String threadNamePrefix,
            Boolean daemon) {
        if (threadFactory != null) {
            return threadFactory;
        }
        if (threadNamePrefix != null) {
            if (daemon != null) {
                return ThreadPoolUtils.buildThreadFactory(threadNamePrefix, daemon);
            } else {
                return ThreadPoolUtils.buildThreadFactory(threadNamePrefix);
            }
        }
        return Executors.defaultThreadFactory();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ThreadPoolBuilderUtils class

/* End of file ThreadPoolBuilderUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/threadpool/ThreadPoolBuilderUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
