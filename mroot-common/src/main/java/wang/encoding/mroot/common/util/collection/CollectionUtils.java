/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.collection;


import com.google.common.collect.Iterables;
import com.google.common.collect.Iterators;
import com.google.common.collect.Ordering;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.util.PairUtils;

import java.util.*;

/**
 * 通用Collection的工具集
 * 1. 集合是否为空，取得集合中首个及最后一个元素，判断集合是否完全相等
 * 2. 集合的最大最小值，及Top N, Bottom N
 * 关于List, Map, Queue, Set的特殊工具集，另见特定的Util
 *
 * 另JDK中缺少ComparableComparator和NullComparator，直到JDK8才补上
 * 因此平时请使用guava的Ordering.natural(),fluentable的API更好用，可以链式设置nullFirst，nullLast,reverse
 *
 * @author ErYang
 *
 */
public class CollectionUtils {


    /**
     * 判断是否为空
     * @param collection Collection
     * @return boolean
     */
    public static boolean isEmpty(@Nullable final Collection<?> collection) {
        return (null == collection) || collection.isEmpty();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断是否不为空
     *
     * @param collection Collection
     * @return boolean
     */
    public static boolean isNotEmpty(@Nullable final Collection<?> collection) {
        return (null != collection) && !(collection.isEmpty());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 取得Collection的第一个元素，如果collection为空返回null
     *
     * @param collection Collection
     * @param <T> T
     * @return T
     */
    public static <T> T getFirst(@Nullable final Collection<T> collection) {
        if (isEmpty(collection)) {
            return null;
        }
        if (collection instanceof List) {
            return ((List<T>) collection).get(0);
        }
        return collection.iterator().next();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获取Collection的最后一个元素，如果collection为空返回null
     *
     * @param collection Collection
     * @param <T> T
     * @return T
     */
    public static <T> T getLast(@Nullable final Collection<T> collection) {
        if (isEmpty(collection)) {
            return null;
        }

        // 当类型List时，直接取得最后一个元素
        if (collection instanceof List) {
            List<T> list = (List<T>) collection;
            return list.get(list.size() - 1);
        }

        return Iterators.getLast(collection.iterator());
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 两个集合中的所有元素按顺序相等
     *
     * @param iterable1 Iterable
     * @param iterable2 Iterable
     * @return boolean
     */
    public static boolean elementsEqual(@NotNull final Iterable<?> iterable1, @NotNull final Iterable<?> iterable2) {
        return Iterables.elementsEqual(iterable1, iterable2);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回无序集合中的最小值，使用元素默认排序
     *
     * @param coll Collection
     * @param <T> T
     * @return T
     */
    public static <T extends Object & Comparable<? super T>> T min(@NotNull final Collection<? extends T> coll) {
        return Collections.min(coll);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回无序集合中的最小值
     *
     * @param coll Collection
     * @param comp Comparator
     * @param <T> T
     * @return T
     */
    public static <T> T min(@NotNull final Collection<? extends T> coll, @NotNull final Comparator<? super T> comp) {
        return Collections.min(coll, comp);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回无序集合中的最大值，使用元素默认排序
     *
     * @param coll Collection
     * @param <T> T
     * @return T
     */
    public static <T extends Object & Comparable<? super T>> T max(@NotNull final Collection<? extends T> coll) {
        return Collections.max(coll);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回无序集合中的最大值
     *
     * @param coll Collection
     * @param comp Comparator
     * @param <T> T
     * @return T
     */
    public static <T> T max(@NotNull final Collection<? extends T> coll, @NotNull final Comparator<? super T> comp) {
        return Collections.max(coll, comp);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 同时返回无序集合中的最小值和最大值，使用元素默认排序
     * 在返回的 PairUtils 中，第一个为最小值，第二个为最大值
     *
     * @param coll Collection
     * @param <T> T
     * @return T
     */
    public static <T extends Object & Comparable<? super T>> PairUtils<T, T> minAndMax(
            @NotNull final Collection<? extends T> coll) {
        Iterator<? extends T> i = coll.iterator();
        T minCandidate = i.next();
        T maxCandidate = minCandidate;

        while (i.hasNext()) {
            T next = i.next();
            if (next.compareTo(minCandidate) < 0) {
                minCandidate = next;
            } else if (next.compareTo(maxCandidate) > 0) {
                maxCandidate = next;
            }
        }
        return PairUtils.of(minCandidate, maxCandidate);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回无序集合中的最小值和最大值
     * 在返回的 PairUtils 中，第一个为最小值，第二个为最大值
     *
     * @param coll Collection
     * @param comp Comparator
     * @param <T> T
     * @return T
     */
    public static <T> PairUtils<T, T> minAndMax(@NotNull final Collection<? extends T> coll,
            @NotNull final Comparator<? super T> comp) {

        Iterator<? extends T> i = coll.iterator();
        T minCandidate = i.next();
        T maxCandidate = minCandidate;

        while (i.hasNext()) {
            T next = i.next();
            if (comp.compare(next, minCandidate) < 0) {
                minCandidate = next;
            } else if (comp.compare(next, maxCandidate) > 0) {
                maxCandidate = next;
            }
        }

        return PairUtils.of(minCandidate, maxCandidate);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回Iterable中最大的N个对象
     * guava
     *
     * @param coll Iterable
     * @param n int
     * @param <T> T
     * @return List
     */
    public static <T extends Comparable<?>> List<T> topN(@NotNull final Iterable<T> coll, int n) {
        return Ordering.natural().greatestOf(coll, n);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回Iterable中最大的N个对象
     * guava
     *
     * @param coll Iterable
     * @param n int
     * @param comp Comparator
     * @param <T> T
     * @return List
     */
    public static <T> List<T> topN(@NotNull final Iterable<T> coll, int n, @NotNull final Comparator<? super T> comp) {
        return Ordering.from(comp).greatestOf(coll, n);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回Iterable中最小的N个对象
     * guava
     *
     * @param coll Iterable
     * @param n int
     * @param <T> T
     * @return List
     */
    public static <T extends Comparable<?>> List<T> bottomN(@NotNull final Iterable<T> coll, int n) {
        return Ordering.natural().leastOf(coll, n);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 返回Iterable中最小的N个对象
     * guava
     *
     * @param coll Iterable
     * @param n int
     * @param comp Comparator
     * @param <T> T
     * @return List
     */
    public static <T> List<T> bottomN(@NotNull final Iterable<T> coll, int n,
            @NotNull final Comparator<? super T> comp) {
        return Ordering.from(comp).leastOf(coll, n);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CollectionUtils class

/* End of file CollectionUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/collection/CollectionUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
