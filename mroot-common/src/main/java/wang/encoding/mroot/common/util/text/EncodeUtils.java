/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util.text;


import com.google.common.io.BaseEncoding;
import wang.encoding.mroot.common.annotation.NotNull;

/**
 * String/url -> hex/base64 编解码工具集 (guava BaseEncoding)
 *
 * @author ErYang
 */
public class EncodeUtils {


    /**
     * Base64编码
     *
     * @param input byte[] 待 base64 编码字符
     * @return String Base64编码后的字符串
     */
    public static String encodeBase64(@NotNull final byte[] input) {
        return BaseEncoding.base64().encode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Base64解码
     * 如果字符不合法，抛出IllegalArgumentException
     *
     * @param input CharSequence 待 base64 解码字符
     * @return String Base64解码的字符串
     */
    public static byte[] decodeBase64(@NotNull final CharSequence input) {
        return BaseEncoding.base64().decode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Base32Hex 编码
     *
     * @param input byte[] 待 Base32Hex 编码字符
     * @return String Base32Hex 编码后的字符串
     */
    public static String encodeBase32Hex(@NotNull final byte[] input) {
        return BaseEncoding.base32Hex().encode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Base32Hex 解码
     * 如果字符不合法，抛出IllegalArgumentException
     *
     * @param input CharSequence 待 Base32Hex 解码字符
     * @return String Base32Hex 解码的字符串
     */
    public static byte[] decodeBase32Hex(@NotNull final CharSequence input) {
        return BaseEncoding.base32Hex().decode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Base64编码, URL安全.(将Base64中的URL非法字符'+'和'/'转为'-'和'_', 见RFC3548).
     *
     * @param input byte[] 待 base64 编码字符
     * @return String Base64编码的字符串
     */
    public static String encodeBase64UrlSafe(@NotNull final byte[] input) {
        return BaseEncoding.base64Url().encode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Base64解码, URL安全(将Base64中的URL非法字符'+'和'/'转为'-'和'_', 见RFC3548).
     * 如果字符不合法，抛出IllegalArgumentException
     *
     * @param input CharSequence 待 base64 编码字符
     * @return String Base64解码的字符串
     */
    public static byte[] decodeBase64UrlSafe(@NotNull final CharSequence input) {
        return BaseEncoding.base64Url().decode(input);
    }

    // -------------------------------------------------------------------------------------------------


    /**
     * Hex编码, 将byte[]编码为String，默认为ABCDEF为大写字母
     *
     * @param input byte[] 待 Hex 编码字符
     * @return String Hex编码的字符串
     */
    public static String encodeHex(@NotNull final byte[] input) {
        return BaseEncoding.base16().encode(input);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * Hex解码, 将String解码为byte[].
     * 字符串有异常时抛出IllegalArgumentException.
     *
     * @param input CharSequence 待 base64 编码字符
     * @return String Hex解码的字符串
     */
    public static byte[] decodeHex(@NotNull final CharSequence input) {
        return BaseEncoding.base16().decode(input);
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End EncodeUtils class

/* End of file EncodeUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/text/EncodeUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
