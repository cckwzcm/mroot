/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import com.google.common.base.Objects;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;

import java.util.Arrays;

/**
 * 1. Object打印优化，主要解决数组的打印
 * 2. 多个对象的HashCode串联
 *
 * @author ErYang
 *
 */
public class ObjectUtils {

    private static final String NULL = "null";

    /**
     * JDK7 引入的Null安全的equals
     *
     * @param a Object
     * @param b Object
     * @return boolean
     */
    public static boolean equals(@Nullable final Object a, @Nullable final Object b) {
        return Objects.equal(a, b);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 多个对象的HashCode串联, 组成新的HashCode
     *
     * @param objects Object
     * @return int
     */
    public static int hashCode(@NotNull final Object... objects) {
        return Arrays.hashCode(objects);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对象的toString(), 处理了对象为数组的情况，JDK的默认toString()只打数组的地址如 Ljava.lang.Integer;@490d6c15
     *
     * @param value Object
     * @return String
     */
    public static String toPrettyString(@NotNull final Object value) {
        if (null == value) {
            return NULL;
        }

        Class<?> type = value.getClass();

        if (type.isArray()) {
            Class componentType = type.getComponentType();

            if (componentType.isPrimitive()) {
                return primitiveArrayToString(value, componentType);
            } else {
                return objectArrayToString(value);
            }
        } else if (value instanceof Iterable) {
            // 因为Collection的处理也是默认调用元素的toString()
            // 为了处理元素是数组的情况，同样需要重载
            return collectionToString(value);
        }

        return value.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 基本转为 String
     *
     * @param value Object
     * @param componentType Class
     * @return String
     */
    private static String primitiveArrayToString(@NotNull final Object value, @NotNull final Class componentType) {
        StringBuilder sb = new StringBuilder();

        if (componentType == int.class) {
            sb.append(Arrays.toString((int[]) value));
        } else if (componentType == long.class) {
            sb.append(Arrays.toString((long[]) value));
        } else if (componentType == double.class) {
            sb.append(Arrays.toString((double[]) value));
        } else if (componentType == float.class) {
            sb.append(Arrays.toString((float[]) value));
        } else if (componentType == boolean.class) {
            sb.append(Arrays.toString((boolean[]) value));
        } else if (componentType == short.class) {
            sb.append(Arrays.toString((short[]) value));
        } else if (componentType == byte.class) {
            sb.append(Arrays.toString((byte[]) value));
        } else if (componentType == char.class) {
            sb.append(Arrays.toString((char[]) value));
        } else {
            throw new IllegalArgumentException(">>>>>>>>未知数组类型<<<<<<<<");
        }

        return sb.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 对象数组
     *
     * @param value Object
     * @return String
     */
    private static String objectArrayToString(@NotNull final Object value) {
        StringBuilder sb = new StringBuilder();
        sb.append('[');

        Object[] array = (Object[]) value;
        int i;
        int length = array.length;
        for (i = 0; i < length; i++) {
            if (i > 0) {
                sb.append(", ");
            }
            sb.append(toPrettyString(array[i]));
        }
        sb.append(']');
        return sb.toString();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 集合
     *
     * @param value Object
     * @return String
     */
    private static String collectionToString(@NotNull final Object value) {
        Iterable iterable = (Iterable) value;
        StringBuilder sb = new StringBuilder();
        sb.append('{');
        int i = 0;
        for (Object o : iterable) {
            if (i > 0) {
                sb.append(',');
            }
            sb.append(toPrettyString(o));
            i++;
        }
        sb.append('}');
        return sb.toString();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ObjectUtils class

/* End of file ObjectUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/ObjectUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
