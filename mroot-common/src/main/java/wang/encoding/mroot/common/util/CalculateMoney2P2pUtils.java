/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                                 2017/4/24
 * // +-------------------------------------------------------------------------------------------------
 * // |                                    木头
 * // +-------------------------------------------------------------------------------------------------
 * // |                               联系:   <2833725009@qq.com>
 * // +-------------------------------------------------------------------------------------------------
 */


package wang.encoding.mroot.common.util;


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import wang.encoding.mroot.common.annotation.NotNull;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.HashMap;
import java.util.Map;

/**
 * 计算金额相关工具类
 *
 * @author ErYang
 *
 */
public class CalculateMoney2P2pUtils {

    private static final Logger logger = LoggerFactory.getLogger(CalculateMoney2P2pUtils.class);

    /**
     * 禁止实例化
     */
    private CalculateMoney2P2pUtils() {

    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按天计算利息，一年按照360天算
     *
     * @param money BigDecimal     金额
     * @param annualInterestRate BigDecimal 年化利率
     * @return BigDecimal 利息
     */
    public static BigDecimal interest2Day360(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate) {
        try {
            // 每天的利率
            BigDecimal dayInterestRate = (annualInterestRate.divide(new BigDecimal(12), 16, RoundingMode.HALF_EVEN)
                    .divide(new BigDecimal(30), 16, RoundingMode.HALF_EVEN))
                    .divide(new BigDecimal(100), 16, RoundingMode.HALF_EVEN);

            // 每天的利息
            BigDecimal dayInterest = money.multiply(dayInterestRate);
            return dayInterest.setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按天计算利息出错[{},{}]<<<<<<<<", money, annualInterestRate);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按天计算利息，一年按照360天算
     *
     * @param money BigDecimal     金额
     * @param annualInterestRate BigDecimal 年化利率
     * @param timeLimit int 期限
     * @return BigDecimal 总利息
     */
    public static BigDecimal interest2Day360(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int timeLimit) {
        try {
            // 每天的利息
            BigDecimal dayInterest = CalculateMoney2P2pUtils.interest2Day360(money, annualInterestRate);
            // 全部利息
            BigDecimal totalInterest = dayInterest.multiply(new BigDecimal(timeLimit));
            return totalInterest.setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按天计算利息出错[{},{},{}]<<<<<<<<", money, annualInterestRate, timeLimit);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月计算利息 每月利息 月还息到期还本
     *
     * @param money BigDecimal 金额
     * @param annualInterestRate BigDecimal年化利率
     * @return BigDecimal 每月利息
     */
    public static BigDecimal interest2Monthly(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate) {
        try {
            // 月利率
            BigDecimal monthInterestRate = CalculateMoney2P2pUtils.getMonthlyInterestRate(annualInterestRate);
            // 每个月的利息
            BigDecimal monthInterest = money.multiply(monthInterestRate);
            return monthInterest.setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按月计算利息,每月利息,月还息到期还本出错:[{},{}]<<<<<<<<", money, annualInterestRate);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月计算利息 月还息到期还本
     *
     * @param money BigDecimal 金额
     * @param annualInterestRate BigDecimal年化利率
     * @param totalPeriod int  总期数
     * @return BigDecimal 总利息
     */
    public static BigDecimal totalInterest2Monthly(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        try {
            // 每个月的利息
            BigDecimal monthInterest = CalculateMoney2P2pUtils.interest2Monthly(money, annualInterestRate);
            // 全部利息
            BigDecimal totalInterest = monthInterest.multiply(new BigDecimal(totalPeriod));
            return totalInterest.setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按月计算利息,总利息,月还息到期还本出错:[{},{},{}]<<<<<<<<", money, annualInterestRate, totalPeriod);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为按月还本息的每月偿还本金和利息(等额本息)
     *
     * 公式：每月偿还本息=〔贷款本金×月利率×(1＋月利率)＾还款月数〕÷〔(1＋月利率)＾还款月数-1〕
     *
     * @param money 金额
     * @param annualInterestRate 年化利率
     * @param totalPeriod 总期数
     * @return BigDecimal 每月偿还本金和利息
     */
    public static BigDecimal monthMoney2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        try {
            // 月利率
            BigDecimal monthInterestRate = CalculateMoney2P2pUtils.getMonthlyInterestRate(annualInterestRate);
            double monthInterestRateDouble = monthInterestRate.doubleValue();
            // 月利率+1
            BigDecimal monthInterestRateAdd1 = monthInterestRate.add(BigDecimal.ONE);
            double monthInterestRateAdd1Double = monthInterestRateAdd1.doubleValue();
            // 每个月偿还的本金和利息
            BigDecimal monthMoney = money.multiply(
                    new BigDecimal(monthInterestRateDouble * Math.pow(monthInterestRateAdd1Double, totalPeriod)))
                    .divide(new BigDecimal(Math.pow(monthInterestRateAdd1Double, totalPeriod) - 1), 16,
                            RoundingMode.HALF_EVEN);
            return monthMoney.setScale(2, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按月还本息,每月还金额出错:[{},{},{}]<<<<<<<<", money, annualInterestRate, totalPeriod);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为月还本息的每月偿还利息(等额本息)
     *
     * 公式：每月偿还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
     * @param money BigDecimal 金额
     * @param annualInterestRate BigDecimal 年化利率
     * @param totalPeriod 总期数
     * @return Map 每月偿还利息
     */
    public static Map<Integer, BigDecimal> mapInterest2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        try {
            Map<Integer, BigDecimal> map = new HashMap<>(16);
            // 月利率
            BigDecimal monthInterestRate = CalculateMoney2P2pUtils.getMonthlyInterestRate(annualInterestRate);
            // 月利率+1
            BigDecimal monthInterestRateAdd1 = monthInterestRate.add(BigDecimal.ONE);
            // 月利率+1
            double monthInterestRateAdd1Double = monthInterestRateAdd1.doubleValue();
            // 月利息
            BigDecimal monthInterest;
            int i;
            BigDecimal interest;
            BigDecimal power;
            for (i = 1; i < totalPeriod + 1; i++) {
                interest = money.multiply(monthInterestRate);
                power = new BigDecimal(Math.pow(monthInterestRateAdd1Double, totalPeriod))
                        .subtract(new BigDecimal(Math.pow(monthInterestRateAdd1Double, i - 1)));
                monthInterest = interest.multiply(power)
                        .divide(new BigDecimal(Math.pow(monthInterestRateAdd1Double, totalPeriod) - 1), 16,
                                RoundingMode.HALF_EVEN);
                monthInterest = monthInterest.setScale(2, RoundingMode.HALF_EVEN);
                map.put(i, monthInterest);
            }
            return map;
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按月还本息,每月还利息出错:[{},{},{}]<<<<<<<<", money, annualInterestRate, totalPeriod);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为月还本息的每月偿还利息(等额本息) 当前期数的利息
     *
     * 公式：每月偿还利息=贷款本金×月利率×〔(1+月利率)^还款月数-(1+月利率)^(还款月序号-1)〕÷〔(1+月利率)^还款月数-1〕
     * @param money BigDecimal 金额
     * @param annualInterestRate BigDecimal 年化利率
     * @param totalPeriod 期限
     * @param currentPeriod 当前期数
     * @return BigDecimal 当前期数还的利息
     */
    public static BigDecimal currentPeriodInterest2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod, final int currentPeriod) {
        // 每月还的本金
        Map<Integer, BigDecimal> map = CalculateMoney2P2pUtils
                .mapInterest2MonthlyInterest(money, annualInterestRate, totalPeriod);
        return map.get(currentPeriod);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为按月还本息的每月偿还本金(等额本息)
     *
     * @param money 金额
     * @param annualInterestRate 年化利率
     * @param totalPeriod 总期数
     * @return Map 每月偿还本金
     */
    public static Map<Integer, BigDecimal> mapCapital2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        try {
            // 每月还的本金和利息
            BigDecimal monthMoney = CalculateMoney2P2pUtils
                    .monthMoney2MonthlyInterest(money, annualInterestRate, totalPeriod);
            // 每月还的利息
            Map<Integer, BigDecimal> mapInterest = CalculateMoney2P2pUtils
                    .mapInterest2MonthlyInterest(money, annualInterestRate, totalPeriod);
            // 每月还的本金
            Map<Integer, BigDecimal> mapCapital = new HashMap<>(16);
            for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
                mapCapital.put(entry.getKey(), monthMoney.subtract(entry.getValue()));
            }
            return mapCapital;
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>按月还本息,每月还本金出错:[{},{},{}]<<<<<<<<", money, annualInterestRate, totalPeriod);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为按月还本息的每月偿还本金(等额本息) 当前期数
     *
     * @param money 金额
     * @param annualInterestRate 年化利率
     * @param totalPeriod 总期数
     * @param currentPeriod 当前期数
     * @return BigDecimal 当前期数还的本金
     */
    public static BigDecimal currentPeriodCapital2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod, final int currentPeriod) {
        Map<Integer, BigDecimal> map = CalculateMoney2P2pUtils
                .mapCapital2MonthlyInterest(money, annualInterestRate, totalPeriod);
        return map.get(currentPeriod);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息计算获取还款方式为按月还本息的总利息(等额本息)
     *
     * @param money 总借款额（贷款本金）
     * @param annualInterestRate 年化利率
     * @param totalPeriod 总期数
     * @return BigDecimal 总利息
     */
    public static BigDecimal totalInterest2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        BigDecimal count = BigDecimal.ZERO;
        // 每个月还的利息
        Map<Integer, BigDecimal> mapInterest = CalculateMoney2P2pUtils
                .mapInterest2MonthlyInterest(money, annualInterestRate, totalPeriod);
        for (Map.Entry<Integer, BigDecimal> entry : mapInterest.entrySet()) {
            count = count.add(entry.getValue());
        }
        return count.setScale(2, RoundingMode.HALF_EVEN);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 按月还本息应还本息总和(等额本息)
     * @param money BigDecimal 金额
     * @param annualInterestRate BigDecimal 年化利率
     * @param totalPeriod int 总期数
     * @return BigDecimal 应还本息总和
     */
    public static BigDecimal totalMoney2MonthlyInterest(@NotNull final BigDecimal money,
            @NotNull final BigDecimal annualInterestRate, final int totalPeriod) {
        // 每月还的本息
        BigDecimal monthMoney = CalculateMoney2P2pUtils
                .monthMoney2MonthlyInterest(money, annualInterestRate, totalPeriod);
        BigDecimal totalMoney = monthMoney.multiply(new BigDecimal(totalPeriod));
        return totalMoney.setScale(2, RoundingMode.HALF_EVEN);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据年化利率计算月利率 12个月
     * @param annualInterestRate BigDecimal 年化利率
     *
     * @return BigDecimal 月利率
     */
    public static BigDecimal getMonthlyInterestRate(@NotNull final BigDecimal annualInterestRate) {
        try {
            BigDecimal month = new BigDecimal(12);
            BigDecimal interestRate = annualInterestRate.divide(month, 16, RoundingMode.HALF_EVEN);
            // 月利率
            return interestRate.divide(new BigDecimal(100), 16, RoundingMode.HALF_EVEN);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>根据年化利率计算月利率错误:[{}]<<<<<<<<", annualInterestRate);
            }
            throw ExceptionUtils.unchecked(e);
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CalculateMoney2P2pUtils class

/* End of file CalculateMoney2P2pUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/CalculateMoney2P2pUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
