/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import lombok.extern.slf4j.Slf4j;

import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.mapper.SuperMapper;
import wang.encoding.mroot.common.util.collection.ListUtils;

import java.io.Serializable;
import java.math.BigInteger;
import java.time.Instant;
import java.util.*;


/**
 * 基类 service 实现
 *
 * @author ErYang
 */
@Slf4j
public class BaseServiceImpl<M extends SuperMapper<T>, T> extends ServiceImpl<M, T> implements BaseService<T> {

    /**
     * 查询总数
     *
     * @param t T 查询条件
     * @return T
     */
    @Override
    public int countByT(@NotNull final T t) {
        // 构建 QueryWrapper
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        return super.count(queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t  T 实体类 查询条件
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    @Override
    public IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @Nullable final String orderByField,
            final boolean isAsc) {
        this.buildPageOrder(page, orderByField, isAsc);
        return super.page(page, new QueryWrapper<>(t));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param orderByList   排序字段集合
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    @Override
    public IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final List<String> orderByList,
            final boolean isAsc) {
        if (CollectionUtils.isNotEmpty(orderByList)) {
            if (isAsc) {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.asc(column));
                }
            } else {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.desc(column));
                }
            }
        }
        return super.page(page, new QueryWrapper<>(t));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param columnArray 查询字段集合
     * @param orderByField   排序字段
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    @Override
    public IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final String[] columnArray,
            @NotNull final String orderByField, final boolean isAsc) {
        this.buildPageOrder(page, orderByField, isAsc);
        // 查询指定字段
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        queryWrapper.select(columnArray);
        return super.page(page, queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 Page 结果集
     * 只有当排序字段有值，排序才会生效
     *
     * @param page   Page
     * @param t   实体类 查询条件
     * @param columnArray 查询字段集合
     * @param orderByList   排序字段集合
     * @param isAsc  是否正序 默认正序
     *
     * @return IPage
     */
    @Override
    public IPage<T> list2page(@NotNull final Page<T> page, @NotNull final T t, @NotNull final String[] columnArray,
            @NotNull final List<String> orderByList, final boolean isAsc) {
        if (CollectionUtils.isNotEmpty(orderByList)) {
            if (isAsc) {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.asc(column));
                }
            } else {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.desc(column));
                }
            }
        }
        // 查询指定字段
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        queryWrapper.select(columnArray);
        return super.page(page, queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  T 集合
     * @param t  T 查询条件
     * @return List 集合
     */
    @Override
    public List<T> listByT(@NotNull final T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        return super.list(queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到所有  T 集合
     * @param t  T 查询条件
     * @param count int 数量
     * @param column String 排序字段
     * @param isAsc boolean 是否正序
     * @return List 集合
     */
    @Override
    public List<T> listByT(@NotNull final T t, final int count, @NotNull final String column, final boolean isAsc) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        if (isAsc) {
            queryWrapper.orderByAsc(column);
        } else {
            queryWrapper.orderByDesc(column);
        }
        // 这个慎重
        queryWrapper.last("LIMIT " + count);
        return super.list(queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查询 list 结果集
     * @param t  T 查询条件
     * @param columnArray 查询字段集合
     *
     * @return List
     */
    @Override
    public List<T> listByT(@NotNull final T t, @NotNull final String[] columnArray) {
        // 构建 QueryWrapper
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        queryWrapper.select(columnArray);
        return super.list(queryWrapper);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 查询 T
     *
     * @param id Serializable
     * @return T
     */
    @Override
    public T getTById(@NotNull final Serializable id) {
        return super.getById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 t 对象查询 T
     * @param t 对象
     * @return T
     */
    @Override
    public T getByModel(@NotNull final T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        try {
            return super.getOne(queryWrapper, true);
        } catch (Exception e) {
            if (logger.isErrorEnabled()) {
                logger.info(">>>>>>>>查询的结果不止一条[{}]<<<<<<<<", t);
            }
            return null;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 t 对象查询 T 如果查询到多条记录不抛出异常 返回最初的数据
     * @param t 对象
     * @return T
     */
    @Override
    public T getByModelNoException(@NotNull final T t) {
        QueryWrapper<T> queryWrapper = new QueryWrapper<>(t);
        return super.getOne(queryWrapper, false);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 判断对象的属性值是否唯一
     * 在修改对象的情景下
     * 如果属性新修改的值 value 等于属性原来的值 oldValue 则不作比较
     *
     * @param property 字段
     * @param newValue 新值
     * @param oldValue 旧值
     * @return true (不存在)/false(存在)
     */
    @Override
    public boolean propertyUnique(@NotNull final String property, @NotNull final Object newValue,
            @NotNull final Object oldValue) {
        if (newValue.equals(oldValue)) {
            return true;
        }
        Map<String, Object> map = new HashMap<>(16);
        map.put(property, newValue);
        Collection<T> list = super.listByMap(map);
        return !CollectionUtils.isNotEmpty(list);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到最大 sort 值
     *
     * @return int
     */
    @Override
    public int getMax2Sort() {
        Integer index = baseMapper.getMax2Sort();
        if (null == index) {
            return 0;
        }
        int max = index;
        if (0 > max) {
            max = 0;
        }
        return max;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 新增 T
     * @param t   T
     *
     * @return boolean
     */
    @Override
    public boolean saveByT(@NotNull final T t) {
        return super.save(t);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 更新 T
     * @param t   T
     *
     * @return boolean
     */
    @Override
    public boolean updateById(@NotNull final T t) {
        return super.updateById(t);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 删除 T
     * @param id   Serializable
     *
     * @return boolean
     */
    @Override
    public boolean deleteById(@NotNull final Serializable id) {
        return super.removeById(id);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 批量删除
     * @param idArray   Serializable[]
     *
     * @return boolean
     */
    @Override
    public boolean deleteBatchById(@NotNull final Serializable[] idArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("idArray", idArray);
        return SqlHelper.retBool(baseMapper.deleteBatchById(params));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 删除 T 更新状态
     *
     * @param t   T
     *
     * @return int
     */
    @Override
    public boolean remove2StatusById(@NotNull final T t) {
        return super.updateById(t);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 更新状态
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean removeBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("state", StateEnum.DELETE.getKey());
        params.put("idArray", idArray);
        params.put("gmtModified", Date.from(Instant.now()));
        return baseMapper.updateBatchStatusById(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 id 恢复 T 更新状态
     *
     * @param t   T
     *
     * @return boolean
     */
    @Override
    public boolean recover2StatusById(final T t) {
        return super.updateById(t);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量恢复 更新状态
     *
     * @param idArray BigInteger[]
     * @return boolean
     */
    @Override
    public boolean recoverBatch2UpdateStatus(@NotNull final BigInteger[] idArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("state", StateEnum.NORMAL.getKey());
        params.put("idArray", idArray);
        params.put("gmtModified", Date.from(Instant.now()));
        return baseMapper.updateBatchStatusById(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 构建排序
     * @param page  Page<T>
     * @param orderByField String
     * @param isAsc boolean
     */
    private void buildPageOrder(@NotNull final Page<T> page, @Nullable final String orderByField, final boolean isAsc) {
        if (StringUtils.isNotBlank(orderByField)) {
            List<String> orderByList = ListUtils.newArrayList();
            orderByList.add(orderByField);
            if (isAsc) {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.asc(column));
                }
            } else {
                for (String column : orderByList) {
                    page.addOrder(OrderItem.desc(column));
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseServiceImpl class

/* End of file BaseServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/service/BaseServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
