/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.util.security.DigestUtils;
import wang.encoding.mroot.common.util.text.EncodeUtils;

import java.util.HashMap;
import java.util.Map;

/**
 *
 * 自定义 cache 缓存 key 名称 工具类
 *
 * @author ErYang
 *
 */
@Slf4j
public class CacheKeyGeneratorUtils {

    /**
     * 生成 key 名称
     * @param cacheName 缓存名称
     * @param methodName 方法名称
     * @param params 参数
     * @return String
     */
    public static String getRedisGenerateKey(@NotNull final String cacheName, @NotNull final String methodName,
            @NotNull final Object... params) {
        return cacheName + CacheNameConst.REDIS_CACHE_SEPARATOR + CacheKeyGeneratorUtils.generate(methodName, params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 生成 key 名称
     * @param methodName 方法名称
     * @param params 参数
     * @return String
     */
    public static String generate(@NotNull final String methodName, @NotNull final Object... params) {
        Map<String, Object> map = new HashMap<>(16);
        // 方法名称
        map.put("methodName", methodName);
        // 参数列表
        for (int i = 0; i < params.length; i++) {
            map.put(String.valueOf(i), params[i]);
        }
        String key = JSONObject.toJSON(map).toString();
        String keyDigest = EncodeUtils.encodeHex(DigestUtils.getMd5(key));
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>自动生成key[key:{},keyDigest:{}]<<<<<<<", key, keyDigest);
        }
        return keyDigest;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CacheKeyGeneratorUtils class

/* End of file CacheKeyGeneratorUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/CacheKeyGeneratorUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
