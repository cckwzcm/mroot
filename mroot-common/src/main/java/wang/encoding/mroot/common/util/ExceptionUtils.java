/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.common.util;


import wang.encoding.mroot.common.annotation.Nullable;
import wang.encoding.mroot.common.exception.UncheckedException;

import java.lang.reflect.UndeclaredThrowableException;


/**
 * 关于异常的工具类
 * 1. Checked/Uncheked及Wrap(如ExecutionException)的转换
 * 2. 打印Exception的辅助函数. (其中一些来自Common Lang ExceptionUtils)
 * 3. 查找Cause(其中一些来自Guava Throwables)
 * 4. StackTrace性能优化相关，尽量使用静态异常避免异常生成时获取StackTrace(Netty)
 *
 * @author ErYang
 */
public class ExceptionUtils {


    /**
     * 将CheckedException转换为RuntimeException重新抛出, 可以减少函数签名中的CheckExcetpion定义
     * CheckedException会用UndeclaredThrowableException包裹，RunTimeException和Error则不会被转变
     * copy from Commons Lang 3.5 ExceptionUtils.
     * 虽然unchecked()里已直接抛出异常，但仍然定义返回值，方便欺骗Sonar。因此本函数也改变了一下返回值
     * 示例代码:
     * try{ ... }catch(Exception e){ throw ExceptionUtils.unchecked(t); }
     */
    public static RuntimeException unchecked(@Nullable final Throwable t) {
        if (t instanceof RuntimeException) {
            throw (RuntimeException) t;
        }
        if (t instanceof Error) {
            throw (Error) t;
        }
        throw new UncheckedException(t);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 如果是著名的包裹类，从cause中获得真正异常. 其他异常则不变.
     * Future中使用的ExecutionException 与 反射时定义的InvocationTargetException， 真正的异常都封装在Cause中
     * 前面 unchecked() 使用的UncheckedException同理.
     */
    public static Throwable unwrap(@Nullable final Throwable t) {
        if (t instanceof UncheckedException || t instanceof java.util.concurrent.ExecutionException
                || t instanceof java.lang.reflect.InvocationTargetException
                || t instanceof UndeclaredThrowableException) {
            return t.getCause();
        }
        return t;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 组合unwrap与unchecked，用于处理反射/Callable的异常
     */
    public static RuntimeException unwrapAndUnchecked(@Nullable final Throwable t) {
        throw ExceptionUtils.unchecked(ExceptionUtils.unwrap(t));
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ExceptionUtils class

/* End of file ExceptionUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/ExceptionUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
