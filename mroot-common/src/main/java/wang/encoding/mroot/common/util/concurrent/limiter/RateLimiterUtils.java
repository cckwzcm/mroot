package wang.encoding.mroot.common.util.concurrent.limiter;

import com.google.common.util.concurrent.RateLimiter;
import wang.encoding.mroot.common.annotation.NotNull;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.Method;


/**
 * 令牌桶
 *
 * @author ErYang
 */
public class RateLimiterUtils {

    /**
     * 一个用来定制RateLimiter的方法，默认一开始就桶里就装满token。
     *
     * @param permitsPerSecond 每秒允许的请求数，可看成QPS。
     * @param maxBurstSeconds 可看成桶的容量，Guava中最大的突发流量缓冲时间，默认是1s, permitsPerSecond * maxBurstSeconds，就是闲置时累积的缓冲token最大值。
     *
     * @return RateLimiter
     * @throws ReflectiveOperationException
     */
    public static RateLimiter create(final double permitsPerSecond, final double maxBurstSeconds)
            throws ReflectiveOperationException {
        return create(permitsPerSecond, maxBurstSeconds, true);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 一个用来定制RateLimiter的方法。
     *
     * @param permitsPerSecond 每秒允许的请求书，可看成QPS
     * @param maxBurstSeconds 最大的突发缓冲时间。用来应对突发流量。Guava的实现默认是1s。permitsPerSecond * maxBurstSeconds的数量，就是闲置时预留的缓冲token数量
     * @param filledWithToken 是否需要创建时就保留有permitsPerSecond * maxBurstSeconds的token
     *
     * @return RateLimiter
     * @throws ReflectiveOperationException
     */
    public static RateLimiter create(final double permitsPerSecond, final double maxBurstSeconds,
            final boolean filledWithToken) throws ReflectiveOperationException {
        Class<?> sleepingStopwatchClass = Class
                .forName("com.google.common.util.concurrent.RateLimiter$SleepingStopwatch");
        Method createStopwatchMethod = sleepingStopwatchClass.getDeclaredMethod("createFromSystemTimer");
        createStopwatchMethod.setAccessible(true);
        Object stopwatch = createStopwatchMethod.invoke(null);

        Class<?> burstRateLimiterClass = Class
                .forName("com.google.common.util.concurrent.SmoothRateLimiter$SmoothBursty");
        Constructor<?> burstyRateLimiterConstructor = burstRateLimiterClass.getDeclaredConstructors()[0];
        burstyRateLimiterConstructor.setAccessible(true);

        // set maxBurstSeconds
        RateLimiter rateLimiter = (RateLimiter) burstyRateLimiterConstructor.newInstance(stopwatch, maxBurstSeconds);
        rateLimiter.setRate(permitsPerSecond);

        if (filledWithToken) {
            // set storedPermits
            setField(rateLimiter, "storedPermits", permitsPerSecond * maxBurstSeconds);
        }
        return rateLimiter;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置
     * @param targetObject Object
     * @param fieldName String
     * @param fieldValue Object
     * @return
     */
    private static boolean setField(@NotNull final Object targetObject, @NotNull final String fieldName,
            @NotNull final Object fieldValue) {
        Field field;
        try {
            field = targetObject.getClass().getDeclaredField(fieldName);
        } catch (NoSuchFieldException e) {
            field = null;
        }
        Class superClass = targetObject.getClass().getSuperclass();
        while (null == field && null != superClass) {
            try {
                field = superClass.getDeclaredField(fieldName);
            } catch (NoSuchFieldException e) {
                superClass = superClass.getSuperclass();
            }
        }
        if (null == field) {
            return false;
        }
        field.setAccessible(true);
        try {
            field.set(targetObject, fieldValue);
            return true;
        } catch (IllegalAccessException e) {
            return false;
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RateLimiterUtils class

/* End of file RateLimiterUtils.java */
/* Location: ./src/main/java/wang/encoding/mroot/common/util/concurrent/limiter/RateLimiterUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
