/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.schedulejob;


import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.quartz.CronExpression;
import org.springframework.beans.factory.NoSuchBeanDefinitionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.ReflectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.admin.common.enums.ScheduleJobOperationTypeEnum;
import wang.encoding.mroot.admin.common.event.ScheduleJobOperationEvent;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.system.AdminScheduleJobService;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;

import java.lang.reflect.Method;
import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;


/**
 * 后台 定时任务 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/system/schedulejob")
public class ScheduleJobController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/schedulejob";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/schedulejob";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "scheduleJob";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 运行
     */
    private static final String RUN_NAME = "run";
    private static final String RUN = "/run";
    private static final String RUN_URL = MODULE_NAME + RUN;
    /**
     * 暂停
     */
    private static final String PAUSE = "/pause";
    private static final String PAUSE_NAME = "pause";
    private static final String PAUSE_URL = MODULE_NAME + PAUSE;
    /**
     * 恢复
     */
    private static final String RESUME = "/resume";
    private static final String RESUME_NAME = "resume";
    private static final String RESUME_URL = MODULE_NAME + RESUME;

    private final AdminScheduleJobService adminScheduleJobService;

    @Autowired
    public ScheduleJobController(AdminScheduleJobService adminScheduleJobService) {
        this.adminScheduleJobService = adminScheduleJobService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminScheduleJobGetVO adminScheduleJobGetVO = new AdminScheduleJobGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminScheduleJobGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminScheduleJobGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminScheduleJobGetVO> pageInt = new Page<>();
        IPage<AdminScheduleJobGetVO> pageAdmin = adminScheduleJobService
                .list2page(super.initPage(pageInt), adminScheduleJobGetVO, AdminScheduleJobGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);

        modelAndView.addObject(RUN_NAME, contextPath + RUN_URL);
        modelAndView.addObject(PAUSE_NAME, contextPath + PAUSE_URL);
        modelAndView.addObject(RESUME_NAME, contextPath + RESUME_URL);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_SAVE)
    @FormToken(remove = true)
    public Object save(AdminScheduleJobGetVO scheduleJobGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();

            // 创建 AdminScheduleJobGetVO 对象
            AdminScheduleJobGetVO adminScheduleJobGetVO = this.initAddData(scheduleJobGetVO);

            try {
                Object bean = super.applicationContext.getBean(adminScheduleJobGetVO.getBeanName());
                Method method = ReflectionUtils.findMethod(bean.getClass(), adminScheduleJobGetVO.getMethodName());
                if (null == method) {
                    return super.initErrorJSONObject(failResult, "message.system.scheduleJob.methodName.error");
                }
            } catch (NoSuchBeanDefinitionException e) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.beanName.error");
            }

            // 检查 cron 表达式
            boolean flagCron = CronExpression.isValidExpression(adminScheduleJobGetVO.getCronExpression());
            if (!flagCron) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.cronExpression.error");
            }

            // Hibernate Validation  验证数据
            String validationResult = adminScheduleJobService.validationScheduleJob(adminScheduleJobGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminScheduleJobGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增 定时任务
            boolean result = adminScheduleJobService.save(adminScheduleJobGetVO);
            if (result) {
                AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService
                        .getBySole(adminScheduleJobGetVO.getSole());
                // 异步添加定时任务
                ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                        ScheduleJobOperationTypeEnum.ADD);
                super.applicationContext.publishEvent(scheduleJobOperationEvent);
            }
            return super.initSaveJSONObject(result);

        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
        if (null == scheduleJobGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == scheduleJobGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }

        modelAndView.addObject(VIEW_MODEL_NAME, scheduleJobGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminScheduleJobGetVO scheduleJobGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 数据真实性
            AdminScheduleJobGetVO scheduleJobGetVOBefore = adminScheduleJobService.getById(idValue);
            if (null == scheduleJobGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == scheduleJobGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }

            // 创建 AdminScheduleJobGetVO 对象
            AdminScheduleJobGetVO validationScheduleJob = BeanMapperComponent
                    .map(scheduleJobGetVOBefore, AdminScheduleJobGetVO.class);
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationScheduleJob.setState(StateEnum.NORMAL.getKey());
            } else {
                validationScheduleJob.setState(StateEnum.DISABLE.getKey());
            }

            this.initEditData(validationScheduleJob, scheduleJobGetVO);

            try {
                Object bean = super.applicationContext.getBean(validationScheduleJob.getBeanName());
                Method method = ReflectionUtils.findMethod(bean.getClass(), validationScheduleJob.getMethodName());
                if (null == method) {
                    return super.initErrorJSONObject(failResult, "message.system.scheduleJob.methodName.error");
                }
            } catch (NoSuchBeanDefinitionException e) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.beanName.error");
            }

            // 检查 cron 表达式
            boolean flagCron = CronExpression.isValidExpression(validationScheduleJob.getCronExpression());
            if (!flagCron) {
                return super.initErrorJSONObject(failResult, "message.system.scheduleJob.cronExpression.error");
            }

            // Hibernate Validation 验证数据
            String validationResult = adminScheduleJobService.validationScheduleJob(validationScheduleJob);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationEditData(validationScheduleJob, scheduleJobGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 修改 定时任务
            boolean result = adminScheduleJobService.update(validationScheduleJob);
            if (result) {
                AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService
                        .getBySole(validationScheduleJob.getSole());
                // 异步更新定时任务
                ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                        ScheduleJobOperationTypeEnum.UPDATE);
                super.applicationContext.publishEvent(scheduleJobOperationEvent);
            }
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
        if (null == scheduleJobGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, scheduleJobGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);


        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
            if (null == scheduleJobGetVO) {
                return super.initErrorCheckJSONObject();
            }

            // 删除 定时任务
            boolean result = adminScheduleJobService.remove2StatusById(scheduleJobGetVO.getId());
            if (result) {
                AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService.getBySole(scheduleJobGetVO.getSole());
                // 异步删除定时任务
                ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                        ScheduleJobOperationTypeEnum.DELETE);
                super.applicationContext.publishEvent(scheduleJobOperationEvent);
            }
            return super.initDeleteJSONObject(result);
        }

        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除
     *
     * @return JSONObject
     */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
            BigInteger[] idArray = super.getIdArray();
            // 验证数据
            if (null == idArray || ArrayUtils.isEmpty(idArray)) {
                return this.initErrorCheckJSONObject();
            }
            boolean flag = adminScheduleJobService.removeBatch2UpdateStatus(idArray);
            if (flag) {
                for (BigInteger id : idArray) {
                    AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService.getById(id);
                    // 异步删除定时任务
                    ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(
                            scheduleJobDONew, ScheduleJobOperationTypeEnum.DELETE);
                    super.applicationContext.publishEvent(scheduleJobOperationEvent);
                }
                return this.initDeleteJSONObject(true);
            } else {
                return this.initDeleteJSONObject(false);
            }
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 运行
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(RUN_URL)
    @RequestMapping(RUN + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_RUN)
    public JSONObject run(@PathVariable(ID_NAME) String id) {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
            if (null == scheduleJobGetVO) {
                return super.initErrorCheckJSONObject();
            }

            AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService.getBySole(scheduleJobGetVO.getSole());
            // 异步运行定时任务
            ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                    ScheduleJobOperationTypeEnum.RUN);
            super.applicationContext.publishEvent(scheduleJobOperationEvent);
            ResultData resultData = ResultData.ok();
            return super.initSucceedJSONObject(resultData, "message.run.succeed");
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 暂停
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(PAUSE_URL)
    @RequestMapping(PAUSE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_PAUSE)
    public JSONObject pause(@PathVariable(ID_NAME) String id) {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
            if (null == scheduleJobGetVO) {
                return super.initErrorCheckJSONObject();
            }

            AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService.getBySole(scheduleJobGetVO.getSole());
            // 异步暂停定时任务
            ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                    ScheduleJobOperationTypeEnum.PAUSE);
            super.applicationContext.publishEvent(scheduleJobOperationEvent);
            ResultData resultData = ResultData.ok();
            return super.initSucceedJSONObject(resultData, "message.pause.succeed");
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 恢复
     *
     * @param id String
     * @return JSONObject
     */
    @RequiresPermissions(RESUME_URL)
    @RequestMapping(RESUME + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_SYSTEM_SCHEDULEJOB_RESUME)
    public JSONObject resume(@PathVariable(ID_NAME) String id) {
        if (super.isAjaxRequest()) {
            BigInteger idValue = super.getId(id);
            // 验证数据
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject();
            }
            // 验证真实性
            AdminScheduleJobGetVO scheduleJobGetVO = adminScheduleJobService.getById(idValue);
            if (null == scheduleJobGetVO) {
                return super.initErrorCheckJSONObject();
            }

            AdminScheduleJobGetVO scheduleJobDONew = adminScheduleJobService.getBySole(scheduleJobGetVO.getSole());
            // 异步恢复定时任务
            ScheduleJobOperationEvent scheduleJobOperationEvent = new ScheduleJobOperationEvent(scheduleJobDONew,
                    ScheduleJobOperationTypeEnum.RESUME);
            super.applicationContext.publishEvent(scheduleJobOperationEvent);
            ResultData resultData = ResultData.ok();
            return super.initSucceedJSONObject(resultData, "message.resume.succeed");
        }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminScheduleJobGetVO AdminScheduleJobGetVO   定时任务
     * @return AdminScheduleJobGetVO
     */
    private AdminScheduleJobGetVO initAddData(@NotNull final AdminScheduleJobGetVO adminScheduleJobGetVO) {
        AdminScheduleJobGetVO addScheduleJobGetVO = BeanMapperComponent
                .map(adminScheduleJobGetVO, AdminScheduleJobGetVO.class);
        // IP
        addScheduleJobGetVO.setGmtCreateIp(super.getIp());
        addScheduleJobGetVO.setState(StateEnum.NORMAL.getKey());
        addScheduleJobGetVO.setGmtCreate(Date.from(Instant.now()));
        return addScheduleJobGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editScheduleJobGetVO AdminScheduleJobGetVO  定时任务
    # @param scheduleJobGetVO AdminScheduleJobGetVO  定时任务
     */
    private void initEditData(@NotNull final AdminScheduleJobGetVO editScheduleJobGetVO,
            @NotNull final AdminScheduleJobGetVO scheduleJobGetVO) {
        editScheduleJobGetVO.setSole(scheduleJobGetVO.getSole());
        editScheduleJobGetVO.setTitle(scheduleJobGetVO.getTitle());
        editScheduleJobGetVO.setBeanName(scheduleJobGetVO.getBeanName());
        editScheduleJobGetVO.setMethodName(scheduleJobGetVO.getMethodName());
        editScheduleJobGetVO.setParams(scheduleJobGetVO.getParams());
        editScheduleJobGetVO.setCronExpression(scheduleJobGetVO.getCronExpression());
        editScheduleJobGetVO.setRemark(scheduleJobGetVO.getRemark());
        editScheduleJobGetVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminScheduleJobGetVO scheduleJobGetVO,
            @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminScheduleJobGetVO titleExist = adminScheduleJobService.getByTitle(scheduleJobGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        AdminScheduleJobGetVO soleExist = adminScheduleJobService.getBySole(scheduleJobGetVO.getSole());
        if (null != soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param scheduleJobGetVO AdminScheduleJobGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminScheduleJobGetVO newScheduleJobGetVO,
            @NotNull final AdminScheduleJobGetVO scheduleJobGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminScheduleJobService
                .propertyUnique(AdminScheduleJobGetVO.TITLE, newScheduleJobGetVO.getTitle(),
                        scheduleJobGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        boolean soleExist = adminScheduleJobService
                .propertyUnique(AdminScheduleJobGetVO.SOLE, newScheduleJobGetVO.getSole(), scheduleJobGetVO.getSole());
        if (!soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ScheduleJobController class

/* End of file ScheduleJobController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/schedulejob/ScheduleJobController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
