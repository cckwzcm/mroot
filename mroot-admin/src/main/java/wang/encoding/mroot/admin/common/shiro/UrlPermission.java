/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.shiro;


import lombok.extern.slf4j.Slf4j;
import org.apache.shiro.authz.Permission;
import org.apache.shiro.util.AntPathMatcher;
import org.apache.shiro.util.PatternMatcher;


/**
 * 权限是否匹配
 *
 * @author ErYang
 */
@Slf4j
public class UrlPermission implements Permission {

    /**
     * 在 Realm 的授权方法中 由数据库查询出来的权限字符串
     */
    private String url;


    UrlPermission(String url) {
        this.url = url;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 用户判断 Realm 中设置的权限和从数据库传递进来的权限信息是否匹配
     * 如果 Realm 的授权方法中 一个认证主体有多个权限 会进行遍历 直到匹配成功为止
     * this.url 是在遍历状态中变化的
     *
     * urlPermission.url 是从 subject.isPermitted(url)
     * 传递到 UrlPermissionResolver 中传递过来的 就一个固定值
     *
     * @param permission Permission
     * @return boolean
     */
    @Override
    public boolean implies(Permission permission) {
        if (!(permission instanceof UrlPermission)) {
            return false;
        }
        UrlPermission urlPermission = (UrlPermission) permission;
        PatternMatcher patternMatcher = new AntPathMatcher();
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>来自数据库中存放数据,在Realm的授权方法中注入的url[{}]<<<<<<<<", this.url);
            logger.debug(">>>>>>>>UrlPermission.url,来自浏览器正在访问的url[{}]<<<<<<<<", urlPermission.url);
        }
        boolean matches = patternMatcher.matches(this.url, urlPermission.url);
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>权限匹配结果[{}]<<<<<<<<", matches);
        }
        if (matches) {
            return true;
        } else {
            boolean flag = urlPermission.url.startsWith(this.url);
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>权限截取匹配结果[{}]<<<<<<<<", flag);
            }
            return flag;
        }
    }

    // -------------------------------------------------------------------------------------------------

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UrlPermission class

/* End of file UrlPermission.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/common/shiro/UrlPermission.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
