/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.cms.blog;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.cms.AdminArticleContentService;
import wang.encoding.mroot.service.admin.cms.AdminArticleService;
import wang.encoding.mroot.service.admin.cms.AdminCategoryService;
import wang.encoding.mroot.vo.admin.entity.cms.article.AdminArticleGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.articlecontent.AdminArticleContentGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.category.AdminCategoryGetVO;


import java.math.BigInteger;


/**
 * 后台 博客 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/cms/blog")
public class BlogController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/cms/blog";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/cms/blog";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "article";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;


    private final AdminArticleService adminArticleService;
    private final AdminCategoryService adminCategoryService;
    private final AdminArticleContentService adminArticleContentService;

    @Autowired
    public BlogController(AdminArticleService adminArticleService, AdminCategoryService adminCategoryService,
            AdminArticleContentService adminArticleContentService) {
        this.adminArticleService = adminArticleService;
        this.adminCategoryService = adminCategoryService;
        this.adminArticleContentService = adminArticleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_BLOG_INDEX)
    public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminArticleGetVO adminArticleGetVO = new AdminArticleGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminArticleGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminArticleGetVO.setState(StateEnum.NORMAL.getKey());
        adminArticleGetVO.setDisplay(BooleanEnum.YES.getKey());

        Page<AdminArticleGetVO> pageInt = new Page<>();
        IPage<AdminArticleGetVO> pageAdmin = adminArticleService
                .list2page(super.initPage(pageInt), adminArticleGetVO, AdminArticleGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CMS_BLOG_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminArticleGetVO articleGetVO = adminArticleService.getById(idValue);
        if (null == articleGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, articleGetVO);
        // 文章内容
        AdminArticleContentGetVO adminArticleContentGetVO = adminArticleContentService.getById(idValue);
        modelAndView.addObject("articleContent", adminArticleContentGetVO);
        // 文章分类
        AdminCategoryGetVO adminCategoryGetVO = adminCategoryService.getById(articleGetVO.getCategoryId());
        modelAndView.addObject("category", adminCategoryGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);
        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());
        // 更新阅读量
        AdminArticleGetVO adminArticleGetVOUpdate = new AdminArticleGetVO();
        adminArticleGetVOUpdate.setId(articleGetVO.getId());
        adminArticleGetVOUpdate.setPageView(articleGetVO.getPageView() + 1);
        adminArticleService.update(adminArticleGetVOUpdate);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogController class

/* End of file BlogController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/cms/blog/BlogController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
