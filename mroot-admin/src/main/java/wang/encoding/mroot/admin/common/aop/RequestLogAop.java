/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.aop;


import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.event.AdminRequestLogAopEvent;
import wang.encoding.mroot.admin.common.util.ShiroSessionUtils;
import wang.encoding.mroot.bo.admin.entity.system.requestlog.AdminRequestLogBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.exception.BaseException;
import wang.encoding.mroot.common.util.HttpRequestUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.common.util.time.ClockUtils;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Method;
import java.time.Instant;
import java.util.Arrays;
import java.util.Date;
import java.util.Map;

/**
 * 请求日志 aop
 *
 * @author ErYang
 */
@Slf4j
@Aspect
@Component
public class RequestLogAop {


    private static final String USER_AGENT = "User-Agent";
    private static final String GET = "GET";


    private final ApplicationContext applicationContext;
    private final ConfigConst configConst;

    @Autowired
    public RequestLogAop(ApplicationContext applicationContext, ConfigConst configConst) {
        this.applicationContext = applicationContext;
        this.configConst = configConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截  wang.encoding.mroot.admin.controller 包下方法存在 RequestLogAnnotation 注解的方法
     */
    @Pointcut("execution(public * wang.encoding.mroot.*.controller..*(..))"
            + "&& (@annotation(wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation))")
    private void pointCutMethod() {
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 保存请求日志
     *
     * @param joinPoint ProceedingJoinPoint
     * @return Object
     */
    @Around(value = "pointCutMethod()")
    public Object requestLog(ProceedingJoinPoint joinPoint) {
        long startTime = ClockUtils.currentTimeMillis();
        Object returnResult;
        try {
            returnResult = joinPoint.proceed();
        } catch (Throwable throwable) {
            throwable.printStackTrace();
            throw new BaseException(GlobalMessage.EXCEPTION_INFO);
        }

        Object target = joinPoint.getTarget();
        // 方法名称
        String methodName = joinPoint.getSignature().getName();
        Class[] parameterTypes = ((MethodSignature) joinPoint.getSignature()).getMethod().getParameterTypes();
        Method method;
        // 通过反射获得拦截的 method
        try {
            method = target.getClass().getMethod(methodName, parameterTypes);
        } catch (NoSuchMethodException e) {
            if (logger.isErrorEnabled()) {
                logger.error(GlobalMessage.EXCEPTION_INFO, e);
            }
            return returnResult;
        }

        ServletRequestAttributes attributes = (ServletRequestAttributes) RequestContextHolder.getRequestAttributes();
        if (null == attributes) {
            return returnResult;
        }
        HttpServletRequest request = attributes.getRequest();

        RequestLogAnnotation requestLogAnnotation = method.getAnnotation(RequestLogAnnotation.class);
        // 如果方法上没有注解 返回
        if (null == requestLogAnnotation) {
            return returnResult;
        }

        // 请求日志
        AdminRequestLogBO requestLog = this
                .initRequestLog(request, target, requestLogAnnotation, methodName, returnResult, startTime);

        // 异步新增数据
        AdminRequestLogAopEvent requestLogAopEvent = new AdminRequestLogAopEvent(this, requestLog);
        applicationContext.publishEvent(requestLogAopEvent);
        return returnResult;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建 AdminRequestLogBO
     *
     * @param request HttpServletRequest
     * @param target Object
     * @param requestLogAnnotation RequestLogAnnotation
     * @param methodName String
     * @param returnResult Object
     * @param startTime long
     * @return AdminRequestLogBO
     */
    private AdminRequestLogBO initRequestLog(@NotNull final HttpServletRequest request, @NotNull final Object target,
            @NotNull final RequestLogAnnotation requestLogAnnotation, @NotNull final String methodName,
            @NotNull final Object returnResult, long startTime) {
        // sessionID
        String sessionName = request.getSession().getId();
        // 类名
        String className = target.getClass().getName();
        // 方法类型
        String methodType = request.getMethod();
        // 请求日志
        AdminRequestLogBO requestLog = new AdminRequestLogBO();
        AdminUserGetVO adminUserGetVO = (AdminUserGetVO) ShiroSessionUtils
                .getAttribute(configConst.getAdminSessionName());
        if (null != adminUserGetVO) {
            requestLog.setUserId(adminUserGetVO.getId());
            requestLog.setUsername(adminUserGetVO.getUsername());
            if (null != adminUserGetVO.getCategory()) {
                requestLog.setUserType(adminUserGetVO.getType());
            }
        }
        requestLog.setModel(LocaleMessageSourceComponent.getMessage(requestLogAnnotation.model()));
        requestLog.setTitle(LocaleMessageSourceComponent.getMessage(requestLogAnnotation.title()));
        requestLog.setClassName(className);
        requestLog.setMethodName(methodName);
        requestLog.setSessionName(sessionName);
        requestLog.setUrl(String.valueOf(request.getRequestURL()));
        requestLog.setUserAgent(request.getHeader(USER_AGENT));
        requestLog.setMethodType(methodType);
        if (GET.equalsIgnoreCase(methodType)) {
            requestLog.setParams(request.getQueryString());
        } else {
            // 获取所有请求参数 封装到 Map 中
            Map<String, String[]> map = request.getParameterMap();
            requestLog.setParams(this.initPostParameter(map));
        }
        requestLog.setResult(returnResult.toString());
        requestLog.setRemark(requestLogAnnotation.remark());
        requestLog.setGmtCreate(Date.from(Instant.now()));
        int ip = IpUtils.ipv4StringToInt(HttpRequestUtils.getIp(request));
        requestLog.setGmtCreateIp(ip);
        requestLog.setExecuteTime(ClockUtils.elapsedTime(startTime));
        return requestLog;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 创建 post 请求参数
     *
     * @param map Map<String, String[]>
     * @return 参数
     */
    private String initPostParameter(@NotNull final Map<String, String[]> map) {
        StringBuilder stringBuilder = new StringBuilder();
        map.forEach((k, v) -> {
            stringBuilder.append("{");
            stringBuilder.append(k);
            stringBuilder.append("=");
            stringBuilder.append(Arrays.toString(v));
            stringBuilder.append("}");
            stringBuilder.append(",");
        });

        if (1 < stringBuilder.length()) {
            stringBuilder.deleteCharAt(stringBuilder.toString().length() - 1);
        }
        return stringBuilder.toString();
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RequestLogAop class

/* End of file RequestLogAop.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/aop/RequestLogAop.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
