/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.language;


import com.alibaba.fastjson.JSONObject;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.support.RequestContextUtils;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;
import java.util.Optional;

/**
 * 语言控制器
 *
 * @author ErYang
 */
@RestController
public class LanguageController {


    private static final String ZH = "zh";
    private static final String CN = "CN";
    private static final String EN = "en";
    private static final String US = "US";

    // -------------------------------------------------------------------------------------------------

    /**
     * 改变语言
     * @param request HttpServletRequest
     * @param response HttpServletResponse
     * @param lang 语言
     * @return JSONObject
     */
    @RequestMapping("/language/{lang}")
    @ResponseBody
    public JSONObject changeLanguage(HttpServletRequest request, HttpServletResponse response,
            @PathVariable("lang") String lang) {
        JSONObject result = new JSONObject();
        Optional<LocaleResolver> localeResolverOptional = Optional
                .ofNullable(RequestContextUtils.getLocaleResolver(request));
        if (localeResolverOptional.isPresent()) {
            LocaleResolver localeResolver = localeResolverOptional.get();
            if (ZH.equalsIgnoreCase(lang)) {
                localeResolver.setLocale(request, response, new Locale(ZH, CN));
            } else if (EN.equalsIgnoreCase(lang)) {
                localeResolver.setLocale(request, response, new Locale(EN, US));
            } else {
                localeResolver.setLocale(request, response, new Locale(ZH, CN));
            }
        }
        return result;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LanguageController class

/* End of file LanguageController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/language/LanguageController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
