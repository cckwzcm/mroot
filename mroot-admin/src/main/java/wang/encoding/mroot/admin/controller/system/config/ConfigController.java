/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.controller.system.config;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.service.admin.system.AdminConfigService;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Future;


/**
 * 配置控制器
 *
 * @author ErYang
 */
@RestController
@RequestMapping("/system/config")
public class ConfigController extends AdminBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/system/config";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/system/config";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "config";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;

    private final AdminConfigService adminConfigService;

    @Autowired
    public ConfigController(AdminConfigService adminConfigService) {
        this.adminConfigService = adminConfigService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 列表页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_INDEX)
    public ModelAndView index() throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

        AdminConfigGetVO adminConfigGetVO = new AdminConfigGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminConfigGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminConfigGetVO.setState(StateEnum.NORMAL.getKey());

        Page<AdminConfigGetVO> pageInt = new Page<>();
        IPage<AdminConfigGetVO> pageAdmin = adminConfigService
                .list2page(super.initPage(pageInt), adminConfigGetVO, AdminConfigGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 添加页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 配置类型
        modelAndView.addObject("configTypeMap", this.listConfigType());
        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理保存
     *
     * @param configGetVO AdminConfigGetVO
     * @return Object
     */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_SAVE)
    @FormToken(remove = true)
    public Object save(AdminConfigGetVO configGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 创建 AdminConfigGetVO 对象
            AdminConfigGetVO adminConfigGetVO = this.initAddData(configGetVO);

            // Hibernate Validation  验证数据
            String validationResult = adminConfigService.validationConfig(adminConfigGetVO);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationAddData(adminConfigGetVO, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 新增配置
            boolean result = adminConfigService.save(adminConfigGetVO);
            if (result) {
                // 重载系统配置
                Future<String> asyncResult = adminControllerAsyncTask.reloadConfig();
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "重新设置系统配置");
            }
            return super.initSaveJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 编辑页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);

        BigInteger idValue = super.getId(id);
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminConfigGetVO configGetVO = adminConfigService.getById(idValue);
        if (null == configGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        if (StateEnum.DELETE.getKey() == configGetVO.getState()) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, configGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        // 配置类型
        modelAndView.addObject("configTypeMap", this.listConfigType());

        // 正常状态
        modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());

        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理更新
     *
     * @param configGetVO AdminConfigGetVO
     * @return Object
     */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_UPDATE)
    @FormToken(remove = true)
    public Object update(AdminConfigGetVO configGetVO) throws ControllerException {
        if (super.isAjaxRequest()) {
            ResultData failResult = ResultData.fail();
            // 验证数据
            BigInteger idValue = super.getId();
            if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 数据真实性
            AdminConfigGetVO configGetVOBefore = adminConfigService.getById(idValue);
            if (null == configGetVOBefore) {
                return super.initErrorCheckJSONObject(failResult);
            }
            if (StateEnum.DELETE.getKey() == configGetVOBefore.getState()) {
                return super.initErrorCheckJSONObject(failResult);
            }
            // 标识不能更改
            configGetVO.setSole(configGetVOBefore.getSole());

            // 创建 AdminConfigGetVO 对象
            AdminConfigGetVO validationConfig = BeanMapperComponent.map(configGetVOBefore, AdminConfigGetVO.class);
            String stateStr = super.getStatusStr();
            if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
                validationConfig.setState(StateEnum.NORMAL.getKey());
            } else {
                validationConfig.setState(StateEnum.DISABLE.getKey());
            }

            this.initEditData(validationConfig, configGetVO);

            // Hibernate Validation 验证数据
            String validationResult = adminConfigService.validationConfig(validationConfig);
            if (StringUtils.isNotBlank(validationResult)) {
                return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
            }

            // 验证数据唯一性
            boolean flag = this.validationEditData(validationConfig, configGetVOBefore, failResult);
            if (!flag) {
                return super.initErrorValidationJSONObject(failResult);
            }

            // 修改配置
            boolean result = adminConfigService.update(validationConfig);
            if (result) {
                // 重载系统配置
                adminControllerAsyncTask.reloadConfig();
                if (databaseConst.getQiniuCdnName().equals(validationConfig.getSole())) {
                    // 静态文件配置
                    Future<String> asyncResult = adminControllerAsyncTask.reloadResource();
                    adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "静态文件配置");
                }
                if (databaseConst.getQiniuUploadName().equals(validationConfig.getSole())) {
                    // 上传配置重置
                    Future<String> asyncResult = adminControllerAsyncTask.reloadUpload();
                    adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "上传配置");
                }
                if (databaseConst.getMailName().equals(validationConfig.getSole())) {
                    // 邮箱配置重置
                    Future<String> asyncResult = adminControllerAsyncTask.reloadMail();
                    adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "邮箱配置");
                }
                // 异步 清理Config缓存
                Future<String> asyncResult = adminControllerAsyncTask.removeConfigCacheById(validationConfig.getId());
                adminControllerAsyncTaskResultUtils.doAdminAsyncResult(asyncResult, "清理Config缓存");
            }
            return super.initUpdateJSONObject(result);
        } else {
            return super.initErrorRedirectUrl();
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @param redirectAttributes RedirectAttributes
     * @return ModelAndView
     */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
            throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        // 数据真实性
        AdminConfigGetVO configGetVO = adminConfigService.getById(idValue);
        if (null == configGetVO) {
            return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
        }
        modelAndView.addObject(VIEW_MODEL_NAME, configGetVO);
        // 设置上个请求地址
        super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 回收站页面
     *
     * @return ModelAndView
     */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL, title = RequestLogConstant.ADMIN_MODEL_CONFIG_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {

        ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);

        AdminConfigGetVO adminConfigGetVO = new AdminConfigGetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            adminConfigGetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        adminConfigGetVO.setState(StateEnum.DELETE.getKey());

        Page<AdminConfigGetVO> pageInt = new Page<>();
        IPage<AdminConfigGetVO> pageAdmin = adminConfigService
                .list2page(super.initPage(pageInt), adminConfigGetVO, AdminConfigGetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);


        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 所有配置类型
     *
     * @return Map
     */
    private Map<String, String> listConfigType() {
        Map<String, String> map = new HashMap<>(16);
        for (ConfigTypeEnum configTypeEnum : ConfigTypeEnum.values()) {
            map.put(String.valueOf(configTypeEnum.getKey()), configTypeEnum.getValue());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param adminConfigGetVO AdminConfigGetVO  配置
     * @return AdminConfigGetVO
     */
    private AdminConfigGetVO initAddData(@NotNull final AdminConfigGetVO adminConfigGetVO) {
        AdminConfigGetVO addConfigGetVO = BeanMapperComponent.map(adminConfigGetVO, AdminConfigGetVO.class);
        // IP
        addConfigGetVO.setGmtCreateIp(super.getIp());
        addConfigGetVO.setState(StateEnum.NORMAL.getKey());
        addConfigGetVO.setGmtCreate(Date.from(Instant.now()));
        return addConfigGetVO;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 合法性
     *
     * @param editConfigVO AdminConfigGetVO   配置
     * @param configGetVO AdminConfigGetVO   配置
     */
    private void initEditData(@NotNull AdminConfigGetVO editConfigVO, @NotNull final AdminConfigGetVO configGetVO) {
        editConfigVO.setCategory(configGetVO.getCategory());
        editConfigVO.setSole(configGetVO.getSole());
        editConfigVO.setTitle(configGetVO.getTitle());
        editConfigVO.setContent(configGetVO.getContent());
        editConfigVO.setRemark(configGetVO.getRemark());
        editConfigVO.setGmtModified(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param configGetVO AdminConfigGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationAddData(@NotNull final AdminConfigGetVO configGetVO,
            @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        AdminConfigGetVO titleExist = adminConfigService.getByTitle(configGetVO.getTitle());
        if (null != titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        // 标识是否存在
        AdminConfigGetVO soleExist = adminConfigService.getBySole(configGetVO.getSole());
        if (null != soleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }

        return true;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 验证数据 数据的唯一性
     *
     * @param configGetVO AdminConfigGetVO 配置
     * @param failResult  ResultData
     * @return boolean true(通过)/false(未通过)
     */
    private boolean validationEditData(@NotNull final AdminConfigGetVO newConfigGetVO,
            @NotNull final AdminConfigGetVO configGetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = adminConfigService
                .propertyUnique(AdminConfigGetVO.TITLE, newConfigGetVO.getTitle(), configGetVO.getTitle());
        if (!titleExist) {
            message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
            failResult.set(GlobalMessage.MESSAGE, message);
            return false;
        }
        return true;
    }

    // -------------------------------------------------------------------------------------------------

}

// End ConfigController class

/* End of file ConfigController.java */
/* Location: ./src/main/java/wang/encoding/mroot/admin/controller/system/config/ConfigController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
