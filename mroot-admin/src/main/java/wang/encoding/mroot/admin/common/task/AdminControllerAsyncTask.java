/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.task;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.session.InvalidSessionException;
import org.apache.shiro.session.Session;
import org.quartz.CronExpression;
import org.quartz.CronTrigger;
import org.quartz.Scheduler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Lazy;
import org.springframework.scheduling.annotation.Async;
import org.springframework.scheduling.annotation.AsyncResult;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.admin.common.constant.ConfigConst;
import wang.encoding.mroot.admin.common.constant.DynamicData;
import wang.encoding.mroot.admin.common.constant.ResourceConst;
import wang.encoding.mroot.admin.common.quartz.QuartzScheduleUtils;
import wang.encoding.mroot.bo.admin.entity.system.role.AdminRoleBO;
import wang.encoding.mroot.bo.admin.entity.system.rule.AdminRuleBO;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.constant.DatabaseConst;
import wang.encoding.mroot.common.enums.ConfigTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.CacheKeyGeneratorUtils;
import wang.encoding.mroot.common.util.FastJsonUtils;
import wang.encoding.mroot.common.util.QiNiuUtils;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.common.util.collection.MapUtils;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.plugin.mail.component.MailComponent;
import wang.encoding.mroot.plugin.mail.constant.MailConst;
import wang.encoding.mroot.plugin.ueditor.hunter.FileManager;
import wang.encoding.mroot.plugin.ueditor.upload.StorageManager;
import wang.encoding.mroot.service.admin.cms.AdminArticleContentService;
import wang.encoding.mroot.service.admin.system.*;
import wang.encoding.mroot.vo.admin.entity.cms.article.AdminArticleGetVO;
import wang.encoding.mroot.vo.admin.entity.cms.articlecontent.AdminArticleContentGetVO;
import wang.encoding.mroot.vo.admin.entity.system.config.AdminConfigGetVO;
import wang.encoding.mroot.vo.admin.entity.system.rule.AdminRuleGetVO;
import wang.encoding.mroot.vo.admin.entity.system.schedulejob.AdminScheduleJobGetVO;
import wang.encoding.mroot.vo.admin.entity.system.user.AdminUserGetVO;


import javax.servlet.ServletContext;
import java.math.BigInteger;
import java.util.*;
import java.util.concurrent.Future;

/**
 * 异步调用
 *
 * @author ErYang
 */
@Slf4j
@Component
public class AdminControllerAsyncTask {

    @Value("${server.servlet.contextPath}")
    private String contextPath;
    @Value("${server.port}")
    private String serverPort;
    private final Scheduler scheduler;

    private final ConfigConst configConst;
    private final ResourceConst resourceConst;
    private final DatabaseConst databaseConst;

    private final ServletContext servletContext;
    private final AdminUserService adminUserService;
    private final AdminRoleService adminRoleService;
    private final AdminRuleService adminRuleService;
    private final AdminConfigService adminConfigService;
    private final AdminRequestLogService adminRequestLogService;
    private final AdminScheduleJobService adminScheduleJobService;
    private final AdminArticleContentService adminArticleContentService;

    @Autowired
    @Lazy
    public AdminControllerAsyncTask(AdminUserService adminUserService, AdminRoleService adminRoleService,
            AdminRuleService adminRuleService, AdminConfigService adminConfigService, ConfigConst configConst,
            ResourceConst resourceConst, DatabaseConst databaseConst, ServletContext servletContext,
            AdminRequestLogService adminRequestLogService, AdminScheduleJobService adminScheduleJobService,
            Scheduler scheduler, AdminArticleContentService adminArticleContentService) {
        this.adminUserService = adminUserService;
        this.adminRoleService = adminRoleService;
        this.adminRuleService = adminRuleService;
        this.adminConfigService = adminConfigService;
        this.configConst = configConst;
        this.resourceConst = resourceConst;
        this.databaseConst = databaseConst;
        this.servletContext = servletContext;
        this.adminRequestLogService = adminRequestLogService;
        this.adminScheduleJobService = adminScheduleJobService;
        this.scheduler = scheduler;
        this.adminArticleContentService = adminArticleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动启动时 初始化配置
     *
     */
    @Async("adminThreadPoolTaskExecutor")
    public void init() {
        // 数据库系统配置
        this.initConfig();
        // 资源信息
        this.initResource();
        // 上传配置
        this.initUpload();
        // 邮箱配置
        this.initMail();
        // 初始化定时任务
        this.initScheduleJobs();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动启动成功后 清空缓存
     *
     */
    @Async("adminThreadPoolTaskExecutor")
    public void clearCache() {
        if (ProfileComponent.isDevProfile()) {
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存<<<<<<<<");
            }
            boolean flag = RedisUtils.flushAll2Pattern(CacheNameConst.ADMIN_PREFIX);
            if (flag) {
                if (logger.isInfoEnabled()) {
                    logger.info(">>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存成功<<<<<<<<");
                }
            } else {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>开发环境下,服务器启动时,清空后台Redis缓存失败<<<<<<<<");
                }
            }

            // 公共缓存
            flag = RedisUtils.flushAll2Pattern(CacheNameConst.COMMON_PREFIX);
            if (flag) {
                if (logger.isInfoEnabled()) {
                    logger.info(">>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存成功<<<<<<<<");
                }
            } else {
                if (logger.isErrorEnabled()) {
                    logger.error(">>>>>>>>开发环境下,服务器启动时,清空公共Redis缓存失败<<<<<<<<");
                }
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 重载系统配置信息
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> reloadConfig() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>重新设置数据库配置开始<<<<<<<<");
        }
        String result;
        try {
            this.initConfigMap();
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>重新设置数据库配置结束<<<<<<<<");
            }
            result = ">>>>>>>>异步重新设置数据库配置成功<<<<<<<<";
            // 重置数据库系统配置 到 redis 缓存中
            RedisUtils.setString(CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                    + CacheNameConst.COMMON_RELOAD_DATABASE_CONFIG, CacheNameConst.COMMON_RELOAD_YES);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置数据库配置出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置数据库配置出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 设置 session 中登录用户信息
     *
     * @param id BigInteger
     * @param session Session
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> initCurrentAdmin(@NotNull final BigInteger id, @NotNull final Session session) {
        String result;
        try {
            AdminUserGetVO adminUserGetVO = adminUserService.getById(id);
            session.setAttribute(configConst.getAdminSessionName(), adminUserGetVO);
            result = String.format(">>>>>>>>异步设置Session中登录用户信息成功,用户ID[%s]<<<<<<<<", id);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步设置Session中登录用户信息出现异常,用户ID[{}]<<<<<<<<", id, e);
            }
            result = String.format(">>>>>>>>异步设置Session中登录用户信息出现异常,用户ID[%s],异常信息:%s<<<<<<<<", id, e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 删除 用户-角色
     *
     * @param userId BigInteger
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeByUserId(@NotNull final BigInteger userId) {
        String result;
        try {
            adminRoleService.removeBatchByUserIdAndRoleId(userId, null);
            result = String.format(">>>>>>>>异步根据用户ID删除用户-角色成功,用户ID[%s]<<<<<<<<", userId);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据用户ID删除用户-角色出现异常,用户ID[{}]<<<<<<<<", userId, e);
            }
            result = String.format(">>>>>>>>异步根据用户ID删除用户-角色出现异常,用户ID[%s],异常信息:%s<<<<<<<<", userId, e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 用户id 批量删除 用户-角色
     *
     * @param userIdArray BigInteger[]
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeByUserIdArray(@NotNull final BigInteger[] userIdArray) {
        String result;
        try {
            adminRoleService.removeByUserIdArray(userIdArray);
            result = String.format(">>>>>>>>异步根据用户ID批量删除用户-角色成功,用户ID[%s]<<<<<<<<", Arrays.toString(userIdArray));
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据用户ID批量删除用户-角色出现异常,用户ID[{}]<<<<<<<<", userIdArray, e);
            }
            result = String
                    .format(">>>>>>>>异步根据用户ID批量删除用户-角色出现异常,用户ID[%s],异常信息:%s<<<<<<<<", Arrays.toString(userIdArray),
                            e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 删除 角色-权限
     *
     * @param roleId BigInteger
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeByRoleId(@NotNull final BigInteger roleId) {
        String result;
        try {
            adminRuleService.removeBatchByRoleIdAndRuleIdArray(roleId, null);
            result = String.format(">>>>>>>>异步根据角色ID删除角色-权限成功,角色ID[%s]<<<<<<<<", roleId);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据角色ID删除角色-权限出现异常,角色ID[{}]<<<<<<<<", roleId, e);
            }
            result = String.format(">>>>>>>>异步根据角色ID删除角色-权限出现异常,角色ID[%s],异常信息:%s<<<<<<<<", roleId, e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 批量删除 角色-权限
     *
     * @param roleIdArray BigInteger[]
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeByRoleIdArray(@NotNull final BigInteger[] roleIdArray) {
        String result;
        try {
            adminRuleService.removeByRoleIdArray(roleIdArray);
            result = String.format(">>>>>>>>异步根据角色ID批量删除角色-权限成功,角色ID[%s]<<<<<<<<", Arrays.toString(roleIdArray));
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据角色ID删除角色-权限出现异常,角色ID[{}]<<<<<<<<", roleIdArray, e);
            }
            result = String.format(">>>>>>>>异步根据角色ID删除角色-权限出现异常,角色ID[%s],异常信息:%s<<<<<<<<", Arrays.toString(roleIdArray),
                    e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清除 Config 缓存事件
     *
     * @param id BigInteger
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeConfigCacheById(@NotNull final BigInteger id) {
        String result;
        try {
            adminConfigService.removeCacheById(id);
            result = ">>>>>>>>异步清除Config缓存成功<<<<<<<<";
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步清除Config缓存出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步清除Config缓存出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 重新设置 当前用户角色拥有的权限
     *
     * @param userVO AdminUserGetVO
     * @param session Session
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> initUserRoleHasRules(@NotNull final AdminUserGetVO userVO, @NotNull final Session session) {
        String result;
        try {
            // 所属角色
            AdminRoleBO adminRoleBO = adminRoleService.getByUserId(userVO.getId());
            if (null != adminRoleBO) {
                // 权限
                List<AdminRuleGetVO> rulesVO = new ArrayList<>();
                TreeSet<AdminRuleBO> ruleVOList = adminRuleService.listByRoleId(adminRoleBO.getId());
                if (CollectionUtils.isNotEmpty(ruleVOList)) {
                    for (AdminRuleBO ruleBO : ruleVOList) {
                        if (BigIntegerUtils.gt(ruleBO.getPid(), BigInteger.ZERO)) {
                            AdminRuleGetVO ruleVO = BeanMapperComponent.map(ruleBO, AdminRuleGetVO.class);
                            rulesVO.add(ruleVO);
                        }
                    }
                }
                if (ListUtils.isNotEmpty(rulesVO)) {
                    // list 转为 tree
                    List<AdminRuleGetVO> tree = AdminRuleGetVO.list(rulesVO);
                    if (null != tree) {
                        // 用户权限菜单存放在 session 中
                        session.setAttribute(configConst.getAdminMenuName(), tree);
                    }
                }
            }
            result = String.format(">>>>>>>>异步重新设置当前用户角色拥有的权限成功,用户ID[%s]<<<<<<<<", userVO.getId());
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置当前用户角色拥有的权限出现异常,用户ID[{}]<<<<<<<<", userVO.getId(), e);
            }
            result = String
                    .format(">>>>>>>>异步重新设置当前用户角色拥有的权限出现异常,用户ID[%s],异常信息:%s<<<<<<<<", userVO.getId(), e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 静态文件配置
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> reloadResource() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>重新设置资源信息集合开始<<<<<<<<");
        }
        String result;
        try {
            DynamicData.RESOURCE_MAP = this.initResourceMap();
            servletContext.setAttribute(resourceConst.getMapName(), DynamicData.RESOURCE_MAP);
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>重新设置资源信息集合结束[key={}," + "map.size={}]<<<<<<<<", resourceConst.getMapName(),
                        DynamicData.RESOURCE_MAP.size());
            }
            result = ">>>>>>>>异步重新设置资源信息成功<<<<<<<<";
            // 重置资源信息 到 redis 缓存中
            RedisUtils.setString(CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                    + CacheNameConst.COMMON_RELOAD_RESOURCE, CacheNameConst.COMMON_RELOAD_YES);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置资源信息出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置资源信息出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 上传文件配置
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> reloadUpload() {
        String result;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置七牛上传配置开始<<<<<<<<");
            }
            // 是否启用七牛上传
            boolean qiNiuUpload = this.qiNiuUpload();
            StorageManager.uploadLocal = FileManager.uploadLocal = !qiNiuUpload;
            QiNiuUtils.uploadLocal = !qiNiuUpload;
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置七牛上传配置结束<<<<<<<<");
            }
            result = ">>>>>>>>异步重新设置七牛上传配置成功<<<<<<<<";
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置七牛上传配置出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置七牛上传配置出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 更新系统配置时 重新设置邮箱配置
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> reloadMail() {
        String result;
        try {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置邮箱配置开始<<<<<<<<");
            }
            // 是否启用邮箱
            MailConst.openMail = this.openMail();
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>重新设置邮箱配置结束<<<<<<<<");
            }
            result = ">>>>>>>>异步重新设置邮箱配置成功<<<<<<<<";
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步重新设置邮箱配置出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步重新设置邮箱配置出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空请求记录日志
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> truncateRequestLog() {
        String result;
        try {
            adminRequestLogService.truncate();
            result = ">>>>>>>>异步清空请求记录日志成功<<<<<<<<";
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步清空请求记录日志出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步清空请求记录日志出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 项目启动时 初始化定时任务
     */
    private void initScheduleJobs() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>初始化定时任务<<<<<<<<");
        }
        AdminScheduleJobGetVO scheduleJobWhere = new AdminScheduleJobGetVO();
        scheduleJobWhere.setState(StateEnum.NORMAL.getKey());
        List<AdminScheduleJobGetVO> scheduleJobVOList = adminScheduleJobService.list2Init(scheduleJobWhere);
        if (ListUtils.isNotEmpty(scheduleJobVOList)) {
            int i = 0;
            for (AdminScheduleJobGetVO scheduleJob : scheduleJobVOList) {
                if (CronExpression.isValidExpression(scheduleJob.getCronExpression())
                        && StateEnum.NORMAL.getKey() == scheduleJob.getState()) {
                    CronTrigger cronTrigger = null;
                    try {
                        cronTrigger = QuartzScheduleUtils.getCronTrigger(scheduler);
                    } catch (Exception e) {
                        if (logger.isErrorEnabled()) {
                            logger.error(">>>>>>>>获取定时任务CronTrigger出现异常<<<<<<<<", e.getCause());
                        }
                    }
                    // 如果不存在 则创建
                    if (null == cronTrigger) {
                        try {
                            QuartzScheduleUtils.deleteScheduleJob(scheduler, scheduleJob);
                            QuartzScheduleUtils.addScheduleJob(scheduler, scheduleJob);
                        } catch (Exception e) {
                            if (logger.isErrorEnabled()) {
                                logger.error(">>>>>>>>创建定时任务失败<<<<<<<<", e.getCause());
                            }
                        }
                    } else {
                        try {
                            QuartzScheduleUtils.editScheduleJob(scheduler, scheduleJob);
                        } catch (Exception e) {
                            if (logger.isErrorEnabled()) {
                                logger.error(">>>>>>>>更新定时任务失败<<<<<<<<", e.getCause());
                            }
                        }
                    }
                    i = i + 1;
                }
            }
            if (logger.isInfoEnabled()) {
                logger.info(">>>>>>>>一共创建[{}]条定时任务<<<<<<<<", i);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空所有缓存
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> clearAllCache() {
        String result;
        try {
            RedisUtils.flushAll();
            result = ">>>>>>>>异步清空所有缓存成功<<<<<<<<";
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步清空所有缓存出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步清空所有缓存出现异常,异常信息:%s<<<<<<<<", e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 清空指定缓存
     *
     * @param name String
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> clearAllCache2Pattern(@NotNull final String name) {
        String result;
        try {
            RedisUtils.flushAll2Pattern(name);
            result = String.format(">>>>>>>>异步清空%s缓存成功<<<<<<<<", name);
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步清空" + name + "缓存出现异常<<<<<<<<", e);
            }
            result = String.format(">>>>>>>>异步清空%s缓存出现异常,异常信息:%s<<<<<<<<", name, e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 文章 新增 文章内容
     *
     * @param adminArticleGetVO AdminArticleGetVO
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> addArticleContent(@NotNull final AdminArticleGetVO adminArticleGetVO,
            @NotNull final AdminArticleContentGetVO adminArticleContentGetVO) {
        String result;
        try {
            adminArticleContentGetVO.setId(adminArticleGetVO.getId());
            adminArticleContentService.save(adminArticleContentGetVO);
            result = String.format(">>>>>>>>异步根据文章新增文章内容成功,文章ID[%s]<<<<<<<<", adminArticleGetVO.getId());
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据文章新增文章内容出现异常,文章ID[{}]<<<<<<<<", adminArticleGetVO.getId(), e);
            }
            result = String.format(">>>>>>>>异步根据文章新增文章内容出现异常,文章ID[%s],异常信息:%s<<<<<<<<", adminArticleGetVO.getId(),
                    e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 文章 更新 文章内容
     *
     * @param adminArticleGetVO AdminArticleGetVO
     * @param adminArticleContentGetVO AdminArticleContentGetVO
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> updateArticleContent(@NotNull final AdminArticleGetVO adminArticleGetVO,
            @NotNull final AdminArticleContentGetVO adminArticleContentGetVO) {
        String result;
        try {
            adminArticleContentGetVO.setId(adminArticleGetVO.getId());
            adminArticleContentService.update(adminArticleContentGetVO);
            // 删除博客模块文章缓存
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.BLOG_ARTICLE_CACHE, "getById", adminArticleGetVO.getId()));
            result = String.format(">>>>>>>>异步根据文章更新文章内容成功,文章ID[%s]<<<<<<<<", adminArticleGetVO.getId());
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据文章更新文章内容出现异常,文章ID[{}]<<<<<<<<", adminArticleGetVO.getId(), e);
            }
            result = String.format(">>>>>>>>异步根据文章更新文章内容出现异常,文章ID[%s],异常信息:%s<<<<<<<<", adminArticleGetVO.getId(),
                    e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 删除 文章内容 （更新状态）
     *
     * @param adminArticleGetVO AdminArticleGetVO
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeArticleContent(@NotNull final AdminArticleGetVO adminArticleGetVO) {
        String result;
        try {
            adminArticleContentService.remove2StatusById(adminArticleGetVO.getId());
            // 删除博客模块文章缓存
            RedisUtils.delete(CacheKeyGeneratorUtils
                    .getRedisGenerateKey(CacheNameConst.BLOG_ARTICLE_CACHE, "getById", adminArticleGetVO.getId()));
            result = String.format(">>>>>>>>异步根据文章删除文章内容成功,文章ID[%s]<<<<<<<<", adminArticleGetVO.getId());
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据文章删除文章内容出现异常,文章ID[{}]<<<<<<<<", adminArticleGetVO.getId(), e);
            }
            result = String.format(">>>>>>>>异步根据文章删除文章内容出现异常,文章ID[%s],异常信息:%s<<<<<<<<", adminArticleGetVO.getId(),
                    e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 批量删除 文章内容 （更新状态）
     *
     * @param idArray BigInteger[]
     *
     * @return Future<String>
     */
    @Async("adminThreadPoolTaskExecutor")
    public Future<String> removeBatchArticleContent(@NotNull final BigInteger[] idArray) {
        String result;
        try {
            adminArticleContentService.removeBatch2UpdateStatus(idArray);
            for (BigInteger id : idArray) {
                // 删除博客模块文章缓存
                RedisUtils.delete(CacheKeyGeneratorUtils
                        .getRedisGenerateKey(CacheNameConst.BLOG_ARTICLE_CACHE, "getById", id));
            }
            result = String.format(">>>>>>>>异步根据文章批量删除文章内容成功,文章ID[%s]<<<<<<<<", Arrays.toString(idArray));
        } catch (InvalidSessionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>异步根据文章批量删除文章内容出现异常,文章ID[{}]<<<<<<<<", idArray, e);
            }
            result = String.format(">>>>>>>>异步根据文章批量删除文章内容出现异常,文章ID[%s],异常信息:%s<<<<<<<<", Arrays.toString(idArray),
                    e.getMessage());
        }
        return new AsyncResult<>(result);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 发送简单邮箱
     * @param toMail String 接收邮箱
     * @param title String 标题
     * @param content String 内容
     */
    @Async("adminThreadPoolTaskExecutor")
    public void sendSimpleEmail(@NotNull final String toMail, @NotNull final String title,
            @NotNull final String content) {
        MailComponent.sendSimpleEmail(toMail, title, content);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 初始化资源 Map 供页面使用
     *
     * @return Map
     */
    private Map<String, String> initResourceMap() {
        // 是否启用七牛cdn
        boolean qiNiuCdn = false;
        // 七牛 cnd 地址
        String qiNiuCdnUrl = null;
        // 得到数据库配置的上传配置
        AdminConfigGetVO adminConfigGetVO = adminConfigService.getBySole2Init(databaseConst.getQiniuCdnName());
        if (null != adminConfigGetVO) {
            if (StateEnum.NORMAL.getKey() == adminConfigGetVO.getState()) {
                qiNiuCdn = true;
            }
            if (ConfigTypeEnum.FUN.getKey() == adminConfigGetVO.getCategory()) {
                if (StringUtils.isNotBlank(adminConfigGetVO.getContent())) {
                    // 得到json数据
                    Map configMap = FastJsonUtils.parseJson2Map(adminConfigGetVO.getContent());
                    if (MapUtils.isNotEmpty(configMap)) {
                        String status = String.valueOf(configMap.get("status"));
                        if (StringUtils.isNotBlank(status) && String.valueOf(StateEnum.NORMAL.getKey())
                                .equals(status)) {
                            qiNiuCdnUrl = String.valueOf(configMap.get("url"));
                        }
                    }
                }
            }
        }

        // 设置的 web 路径
        String webPath;
        if (StringUtils.isNotBlank(contextPath)) {
            webPath = contextPath;
        } else {
            webPath = "/";
        }

        // web 地址
        String webUrl;

        if (qiNiuCdn) {
            // 七牛 CDN
            if (StringUtils.isNotBlank(qiNiuCdnUrl)) {
                webUrl = qiNiuCdnUrl;
            } else {
                webUrl = webPath;
            }
        } else {
            webUrl = webPath;
        }

        Map<String, String> map = new HashMap<>(16);
        map.put(resourceConst.getResourceName(), webUrl + resourceConst.getResource());
        // 默认主题
        if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.DEFAULT_THEME)) {
            map.put(resourceConst.getDefaultThemeName(), webUrl + resourceConst.getDefaultTheme());
            map.put(resourceConst.getDefaultVendorsName(), webUrl + resourceConst.getDefaultVendors());
            map.put(resourceConst.getDefaultAppName(), webUrl + resourceConst.getDefaultApp());
            map.put(resourceConst.getDefaultBaseName(), webUrl + resourceConst.getDefaultBase());
        } else if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.ELITE_THEME)) {
            // elite 主题
            map.put(resourceConst.getEliteThemeName(), webUrl + resourceConst.getEliteTheme());
            map.put(resourceConst.getEliteAppName(), webUrl + resourceConst.getEliteApp());
            map.put(resourceConst.getEliteBaseName(), webUrl + resourceConst.getEliteBase());
        } else {
            map.put(resourceConst.getDefaultThemeName(), webUrl + resourceConst.getDefaultTheme());
            map.put(resourceConst.getDefaultVendorsName(), webUrl + resourceConst.getDefaultVendors());
            map.put(resourceConst.getDefaultAppName(), webUrl + resourceConst.getDefaultApp());
            map.put(resourceConst.getDefaultBaseName(), webUrl + resourceConst.getDefaultBase());
        }
        return map;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 系统启动时 数据库配置信息
     *
     */
    private void initConfig() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置数据库配置开始<<<<<<<<");
        }
        this.initConfigMap();
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置数据库配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 是否启用邮箱
     * @return boolean true(是)/false(否)
     */
    private boolean openMail() {
        // 是否启用邮箱
        boolean openMail = false;
        // 得到数据库配置的配置
        AdminConfigGetVO configVO = adminConfigService.getBySole2Init(databaseConst.getMailName());
        if (null != configVO) {
            if (ConfigTypeEnum.FUN.getKey() == configVO.getCategory()) {
                if (null != configVO.getContent() && StringUtils.isNotBlank(configVO.getContent())) {
                    Map map = FastJsonUtils.parseJson2Map(configVO.getContent());
                    if (MapUtils.isNotEmpty(map)) {
                        String status = (String) map.get("status");
                        if (String.valueOf(StateEnum.NORMAL.getKey()).equals(status)) {
                            openMail = true;
                        }
                    }
                }
            }
        }
        return openMail;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 读取数据库配置
     */
    private void initConfigMap() {
        AdminConfigGetVO configWhere = new AdminConfigGetVO();
        configWhere.setState(StateEnum.NORMAL.getKey());
        List<AdminConfigGetVO> list = adminConfigService.list2Init(configWhere);
        if (ListUtils.isNotEmpty(list)) {
            Map<String, AdminConfigGetVO> map = new HashMap<>(16);
            for (AdminConfigGetVO adminConfigGetVO : list) {
                map.put(adminConfigGetVO.getSole(), adminConfigGetVO);
            }
            DynamicData.CONFIG_MAP = map;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 七牛上传
     * @return boolean true(是)/false(否)
     */
    private boolean qiNiuUpload() {
        // 是否启用七牛上传
        boolean qiNiuUpload = false;
        // 得到数据库配置的配置
        AdminConfigGetVO configVO = adminConfigService.getBySole2Init(databaseConst.getQiniuUploadName());
        if (null != configVO) {
            if (ConfigTypeEnum.FUN.getKey() == configVO.getCategory()) {
                if (null != configVO.getContent() && StringUtils.isNotBlank(configVO.getContent())) {
                    Map map = FastJsonUtils.parseJson2Map(configVO.getContent());
                    if (MapUtils.isNotEmpty(map)) {
                        String status = (String) map.get("status");
                        if (String.valueOf(StateEnum.NORMAL.getKey()).equals(status)) {
                            qiNiuUpload = true;
                        }
                    }
                }
            }
        }
        return qiNiuUpload;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动系统时 静态文件配置
     *
     */
    private void initResource() {
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置资源信息集合开始<<<<<<<<");
        }
        DynamicData.RESOURCE_MAP = this.initResourceMap();
        servletContext.setAttribute(resourceConst.getMapName(), DynamicData.RESOURCE_MAP);
        if (logger.isInfoEnabled()) {
            logger.info(">>>>>>>>设置资源信息集合结束[key={}," + "map.size={}]<<<<<<<<", resourceConst.getMapName(),
                    DynamicData.RESOURCE_MAP.size());
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 启动启动时 上传文件配置
     *
     */
    private void initUpload() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置七牛上传配置开始<<<<<<<<");
        }
        // 是否启用七牛上传
        boolean qiNiuUpload = this.qiNiuUpload();
        StorageManager.uploadLocal = FileManager.uploadLocal = !qiNiuUpload;
        QiNiuUtils.uploadLocal = !qiNiuUpload;
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置七牛上传配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 系统启动时  设置邮箱配置
     *
     */
    private void initMail() {
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置邮箱配置开始<<<<<<<<");
        }
        // 是否启用邮箱
        MailConst.openMail = this.openMail();
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>设置邮箱配置结束<<<<<<<<");
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End AdminControllerAsyncTask class

/* End of file AdminControllerAsyncTask.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/task/AdminControllerAsyncTask.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
