/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.admin.common.constant;


/**
 * 请求日志 常量类
 *
 * @author ErYang
 */
public class RequestLogConstant {


    /**
     * 后台管理模块
     */
    public static final String ADMIN_MODEL = "message.aop.requestLog.model.admin";

    /**
     * 主页
     */
    public static final String ADMIN_MODEL_INDEX_VIEW = "message.aop.requestLog.model.admin.message.main.view";

    /**
     * 用户
     */
    public static final String ADMIN_MODEL_USER_INDEX = "message.aop.requestLog.model.admin.system.user.index";
    public static final String ADMIN_MODEL_USER_ADD = "message.aop.requestLog.model.admin.system.user.add";
    public static final String ADMIN_MODEL_USER_SAVE = "message.aop.requestLog.model.admin.system.user.save";
    public static final String ADMIN_MODEL_USER_EDIT = "message.aop.requestLog.model.admin.system.user.edit";
    public static final String ADMIN_MODEL_USER_UPDATE = "message.aop.requestLog.model.admin.system.user.update";
    public static final String ADMIN_MODEL_USER_VIEW = "message.aop.requestLog.model.admin.system.user.view";
    public static final String ADMIN_MODEL_USER_DELETE = "message.aop.requestLog.model.admin.system.user.delete";
    public static final String ADMIN_MODEL_USER_DELETE_BATCH = "message.aop.requestLog.model.admin.system.user.delete.batch";
    public static final String ADMIN_MODEL_USER_AUTHORIZATION = "message.aop.requestLog.model.admin.system.user.authorization";
    public static final String ADMIN_MODEL_USER_AUTHORIZATION_SAVE = "message.aop.requestLog.model.admin.system.user.authorization.save";
    public static final String ADMIN_MODEL_USER_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.system.user.recycleBin";
    public static final String ADMIN_MODEL_USER_RECOVER = "message.aop.requestLog.model.admin.system.user.recover";
    public static final String ADMIN_MODEL_USER_RECOVER_BATCH = "message.aop.requestLog.model.admin.system.user.recover.batch";
    public static final String ADMIN_MODEL_USER_PASSWORD = "message.aop.requestLog.model.admin.system.user.password";
    public static final String ADMIN_MODEL_USER_PASSWORD_UPDATE = "message.aop.requestLog.model.admin.system.user.password.update";
    public static final String ADMIN_MODEL_USER_NICK_NAME = "message.aop.requestLog.model.admin.system.user.nickName";
    public static final String ADMIN_MODEL_USER_NICK_NAME_UPDATE = "message.aop.requestLog.model.admin.system.user.nickName.update";

    /**
     * 系统配置
     */
    public static final String ADMIN_MODEL_CONFIG_INDEX = "message.aop.requestLog.model.admin.system.config.index";
    public static final String ADMIN_MODEL_CONFIG_ADD = "message.aop.requestLog.model.admin.system.config.add";
    public static final String ADMIN_MODEL_CONFIG_SAVE = "message.aop.requestLog.model.admin.system.config.save";
    public static final String ADMIN_MODEL_CONFIG_EDIT = "message.aop.requestLog.model.admin.system.config.edit";
    public static final String ADMIN_MODEL_CONFIG_UPDATE = "message.aop.requestLog.model.admin.system.config.update";
    public static final String ADMIN_MODEL_CONFIG_VIEW = "message.aop.requestLog.model.admin.system.config.view";
    public static final String ADMIN_MODEL_CONFIG_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.system.config.recycleBin";
    public static final String ADMIN_MODEL_CONFIG_CLEAR_CACHE = "message.aop.requestLog.model.admin.system.config.clearCache";

    /**
     * 代码生成
     */
    public static final String ADMIN_MODEL_GENERATE_VIEW = "message.aop.requestLog.model.admin.system.generate.view";
    public static final String ADMIN_MODEL_GENERATE_CODE_VIEW = "message.aop.requestLog.model.admin.system.generate.code.view";
    public static final String ADMIN_MODEL_GENERATE_SAVE = "message.aop.requestLog.model.admin.system.generate.save";

    /**
     * 角色
     */
    public static final String ADMIN_MODEL_SYSTEM_ROLE_INDEX = "message.aop.requestLog.model.admin.system.role.index";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_ADD = "message.aop.requestLog.model.admin.system.role.add";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_SAVE = "message.aop.requestLog.model.admin.system.role.save";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_EDIT = "message.aop.requestLog.model.admin.system.role.edit";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_UPDATE = "message.aop.requestLog.model.admin.system.role.update";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_VIEW = "message.aop.requestLog.model.admin.system.role.view";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_DELETE = "message.aop.requestLog.model.admin.system.role.delete";
    public static final String ADMIN_MODEL_ROLE_AUTHORIZATION = "message.aop.requestLog.model.admin.system.role.authorization";
    public static final String ADMIN_MODEL_ROLE_AUTHORIZATION_SAVE = "message.aop.requestLog.model.admin.system.role.authorization.save";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_DELETE_BATCH = "message.aop.requestLog.model.admin.system.role.delete.batch";
    public static final String ADMIN_MODEL_SYSTEM_ROLE_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.system.role.recycleBin";

    /**
     * 权限
     */
    public static final String ADMIN_MODEL_SYSTEM_RULE_INDEX = "message.aop.requestLog.model.admin.system.rule.index";
    public static final String ADMIN_MODEL_SYSTEM_RULE_ADD = "message.aop.requestLog.model.admin.system.rule.add";
    public static final String ADMIN_MODEL_SYSTEM_RULE_SAVE = "message.aop.requestLog.model.admin.system.rule.save";
    public static final String ADMIN_MODEL_RULE_ADD_CHILDREN = "message.aop.requestLog.module.admin.rule.add.children";
    public static final String ADMIN_MODEL_SYSTEM_RULE_EDIT = "message.aop.requestLog.model.admin.system.rule.edit";
    public static final String ADMIN_MODEL_SYSTEM_RULE_UPDATE = "message.aop.requestLog.model.admin.system.rule.update";
    public static final String ADMIN_MODEL_SYSTEM_RULE_VIEW = "message.aop.requestLog.model.admin.system.rule.view";
    public static final String ADMIN_MODEL_SYSTEM_RULE_DELETE = "message.aop.requestLog.model.admin.system.rule.delete";
    public static final String ADMIN_MODEL_SYSTEM_RULE_DELETE_BATCH = "message.aop.requestLog.model.admin.system.rule.delete.batch";
    public static final String ADMIN_MODEL_SYSTEM_RULE_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.system.rule.recycleBin";

    /**
     * 请求日志
     */
    public static final String ADMIN_MODEL_SYSTEM_REQUESTLOG_INDEX = "message.aop.requestLog.model.admin.system.requestLog.index";
    public static final String ADMIN_MODEL_SYSTEM_REQUESTLOG_VIEW = "message.aop.requestLog.model.admin.system.requestLog.view";
    public static final String ADMIN_MODEL_SYSTEM_REQUESTLOG_DELETE = "message.aop.requestLog.model.admin.system.requestLog.delete";
    public static final String ADMIN_MODEL_SYSTEM_REQUESTLOG_DELETE_BATCH = "message.aop.requestLog.model.admin.system.requestLog.delete.batch";
    public static final String ADMIN_MODEL_REQUESTLOG_TRUNCATE = "message.aop.requestLog.module.admin.requestLog.truncate";

    /**
     * 定时任务
     */
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_INDEX = "message.aop.requestLog.model.admin.system.scheduleJob.index";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_ADD = "message.aop.requestLog.model.admin.system.scheduleJob.add";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_SAVE = "message.aop.requestLog.model.admin.system.scheduleJob.save";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_EDIT = "message.aop.requestLog.model.admin.system.scheduleJob.edit";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_UPDATE = "message.aop.requestLog.model.admin.system.scheduleJob.update";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_VIEW = "message.aop.requestLog.model.admin.system.scheduleJob.view";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_DELETE = "message.aop.requestLog.model.admin.system.scheduleJob.delete";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_DELETE_BATCH = "message.aop.requestLog.model.admin.system.scheduleJob.delete.batch";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_RUN = "message.aop.requestLog.model.admin.system.scheduleJob.run";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_PAUSE = "message.aop.requestLog.model.admin.system.scheduleJob.pause";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOB_RESUME = "message.aop.requestLog.model.admin.system.scheduleJob.resume";

    /**
     * 定时任务记录
     */
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOBLOG_INDEX = "message.aop.requestLog.model.admin.system.scheduleJobLog.index";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOBLOG_VIEW = "message.aop.requestLog.model.admin.system.scheduleJobLog.view";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOBLOG_DELETE = "message.aop.requestLog.model.admin.system.scheduleJobLog.delete";
    public static final String ADMIN_MODEL_SYSTEM_SCHEDULEJOBLOG_DELETE_BATCH = "message.aop.requestLog.model.admin.system.scheduleJobLog.delete.batch";

    /**
     * logback日志
     */
    public static final String ADMIN_MODEL_LOGGING_LOGGINGEVENT_INDEX = "message.aop.requestLog.model.admin.system.loggingEvent.index";
    public static final String ADMIN_MODEL_LOGGING_LOGGINGEVENT_VIEW = "message.aop.requestLog.model.admin.system.loggingEvent.view";
    /**
     * 文章分类
     */
    public static final String ADMIN_MODEL_CMS_CATEGORY_INDEX = "message.aop.requestLog.model.admin.cms.category.index";
    public static final String ADMIN_MODEL_CMS_CATEGORY_ADD = "message.aop.requestLog.model.admin.cms.category.add";
    public static final String ADMIN_MODEL_CMS_CATEGORY_SAVE = "message.aop.requestLog.model.admin.cms.category.save";
    public static final String ADMIN_MODEL_CMS_CATEGORY_EDIT = "message.aop.requestLog.model.admin.cms.category.edit";
    public static final String ADMIN_MODEL_CMS_CATEGORY_UPDATE = "message.aop.requestLog.model.admin.cms.category.update";
    public static final String ADMIN_MODEL_CMS_CATEGORY_VIEW = "message.aop.requestLog.model.admin.cms.category.view";
    public static final String ADMIN_MODEL_CMS_CATEGORY_DELETE = "message.aop.requestLog.model.admin.cms.category.delete";
    public static final String ADMIN_MODEL_CMS_CATEGORY_DELETE_BATCH = "message.aop.requestLog.model.admin.cms.category.delete.batch";
    public static final String ADMIN_MODEL_CMS_CATEGORY_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.cms.category.recycleBin";
    public static final String ADMIN_MODEL_CMS_CATEGORY_ADD_ARTICLE = "message.aop.requestLog.model.admin.cms.category.add.article";
    /**
     * 文章
     */
    public static final String ADMIN_MODEL_CMS_ARTICLE_INDEX = "message.aop.requestLog.model.admin.cms.article.index";
    public static final String ADMIN_MODEL_CMS_ARTICLE_ADD = "message.aop.requestLog.model.admin.cms.article.add";
    public static final String ADMIN_MODEL_CMS_ARTICLE_SAVE = "message.aop.requestLog.model.admin.cms.article.save";
    public static final String ADMIN_MODEL_CMS_ARTICLE_EDIT = "message.aop.requestLog.model.admin.cms.article.edit";
    public static final String ADMIN_MODEL_CMS_ARTICLE_UPDATE = "message.aop.requestLog.model.admin.cms.article.update";
    public static final String ADMIN_MODEL_CMS_ARTICLE_VIEW = "message.aop.requestLog.model.admin.cms.article.view";
    public static final String ADMIN_MODEL_CMS_ARTICLE_DELETE = "message.aop.requestLog.model.admin.cms.article.delete";
    public static final String ADMIN_MODEL_CMS_ARTICLE_DELETE_BATCH = "message.aop.requestLog.model.admin.cms.article.delete.batch";
    public static final String ADMIN_MODEL_CMS_ARTICLE_RECYCLE_BIN_INDEX = "message.aop.requestLog.model.admin.cms.article.recycleBin";
    public static final String ADMIN_MODEL_CMS_ARTICLE_UPLOADCOVER = "message.aop.requestLog.model.admin.cms.article.uploadCover";

    /**
     * 博客
     */
    public static final String ADMIN_MODEL_CMS_BLOG_INDEX = "message.aop.requestLog.model.admin.cms.blog.index";
    public static final String ADMIN_MODEL_CMS_BLOG_VIEW = "message.aop.requestLog.model.admin.cms.blog.view";
    /**
     * 电子邮箱
     */
    public static final String ADMIN_MODEL_SYSTEM_MAIL_INDEX = "message.aop.requestLog.model.admin.system.mail.index";
    public static final String ADMIN_MODEL_SYSTEM_MAIL_SEND_TEST = "message.aop.requestLog.model.admin.system.mail.save.test";
    /**
     * 电子邮箱记录
     */
    public static final String ADMIN_MODEL_SYSTEM_MAILLOG_INDEX = "message.aop.requestLog.model.admin.system.mailLog.index";
    public static final String ADMIN_MODEL_SYSTEM_MAILLOG_VIEW = "message.aop.requestLog.model.admin.system.mailLog.view";
    /**
     * 清除缓存
     */
    public static final String ADMIN_MODEL_SYSTEM_CACHE_INDEX = "message.aop.requestLog.model.admin.system.cache.index";
    public static final String ADMIN_MODEL_SYSTEM_CACHE_SAVE = "message.aop.requestLog.model.admin.system.cache.save";


}

// -----------------------------------------------------------------------------------------------------

// End RequestLogConstant class

/* End of file RequestLogConstant.java */
/* Location; ./src/main/java/wang/encoding/mroot/admin/common/constant/RequestLogConstant.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
