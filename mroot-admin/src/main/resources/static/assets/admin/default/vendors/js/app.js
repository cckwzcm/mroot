/**
 核心脚本处理
 */
var App = function () {


    // 处理图片懒加载
    var handleImgLazyLoad = function () {
        if (!$().lazyload) {
            return;
        }
        $("img").lazyload({
            effect: "fadeIn"
        });
    };

    // -------------------------------------------------------------------------------------------------

//* 核心处理完成 *//

    return {

        // 主要函数 处理 主题
        init: function () {
            handleImgLazyLoad();
        }

    };

}();

jQuery(document).ready(function () {
    App.init()
});
