<#-- /* 页面级别插件开始 */ -->
<@OVERRIDE name="PAGE_PLUGIN">

</@OVERRIDE>
<#-- /* 页面级别插件结束 */ -->

<#--/* 页面级别script开始 */-->
<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
    <#include "/elite/scriptplugin/jqueryvalidate.ftl">
    <#include "/elite/scriptplugin/formvalidation.ftl">
</@OVERRIDE>
<#--/* 页面级别script结束 */-->
