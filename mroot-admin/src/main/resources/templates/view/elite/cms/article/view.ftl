<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.id")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${article.id}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.categoryId")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr category.title></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.title></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.description")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.description></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.articleContent.form.content")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr articleContent.content></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.cover")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"> <#if article.cover??>
                                        <img style="width: 120px;height: 120px;" src="${article.cover}">
                                    <#else>
                                        <@defaultStr article.cover></@defaultStr>
                                    </#if></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.pageView")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.pageView></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.display")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole article.display></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.reward")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole article.reward></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.priority")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.priority></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.reprint")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@boole article.reprint></@boole></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.linkUri")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.linkUri></@defaultStr></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.state")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@status article.status></@status></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreate")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat article.gmtCreate></@dateFormat></p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtCreateIp")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@defaultStr article.ip></@defaultStr></p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.gmtModified")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static"><@dateFormat article.gmtModified></@dateFormat></p>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@viewFormOperate></@viewFormOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/elite/scriptplugin/tool.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
