<@OVERRIDE name="PAGE_SCRIPT_STYLE">
    <link rel="stylesheet"
          href="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webuploader/webuploader.css">
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_article-form" action="${saveArticle}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.categoryId")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="categoryId"
                                        data-placeholder="${I18N("message.cms.article.form.categoryId.text.help")}">
                                    <option value="">${I18N("message.cms.article.form.categoryId.text.help")}</option>
                                    <#list treeCategories as topCategory>

                                        <option <#if (category.id == topCategory.id)>selected</#if>
                                                value="${topCategory.id}">
                                            ${topCategory.title}
                                        </option>

                                        <#list topCategory.childrenList as childCategory>
                                            <option <#if (category.id == childCategory.id)>selected</#if>
                                                    value="${childCategory.id}">
                                                &nbsp;&nbsp;&lfloor;&nbsp;${childCategory.title}
                                            </option>
                                            <#list childCategory.childrenList as categoryList>
                                                <option <#if (category.id == categoryList.id)>selected</#if>
                                                        value="${categoryList.id}">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lfloor;&nbsp;&lfloor;&nbsp;${categoryList.title}</option>
                                            </#list>
                                        </#list>
                                    </#list>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.article.form.categoryId.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title"
                                       placeholder="${I18N("message.form.title.text")}"
                                       value="${article.title}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.title.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.description")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                               <textarea class="form-control" rows="5" name="description"
                                         placeholder="${I18N("message.cms.article.form.description.text")}">${article.description}</textarea>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.article.form.description.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.articleContent.form.content")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <script type="text/pain" id="ja_article-content"
                                        name="content">${articleContent.content}</script>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.articleContent.form.content.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.cover")}</label>
                            <div class="col-md-9">
                                <div class="custom-dropzone custom-dropzone-success">
                                    <div class="custom-dropzone-msg dz-message" id="js_cover-upload">
                                        <h3 class="custom-dropzone-msg-title">
                                            ${I18N("message.cms.article.form.cover.text")}
                                        </h3>
                                        <span class="help-block"><small
                                                    class="form-control-feedback">${I18N("message.cms.article.form.cover.text.help")}</small></span>
                                    </div>
                                    <div id="js_cover-upload-complete"></div>
                                    <input type="hidden" id="js_cover-image" name="cover">
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.display")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       checked
                                       name="displayStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.reward")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       checked
                                       name="rewardStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.priority")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="priority"
                                       placeholder="${I18N("message.cms.article.form.priority.text")}"
                                       value="${article.priority}"
                                       id="js_priority"
                                       data-bts-button-down-class="btn btn-white btn-outline"
                                       data-bts-button-up-class="btn btn-white btn-outline"
                                >
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.article.form.priority.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.reprint")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.on.btn")}"
                                       data-off-text="${I18N("message.form.off.btn")}"
                                       name="reprintStr"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.cms.article.form.linkUri")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="linkUri"
                                       placeholder="${I18N("message.cms.article.form.linkUri.text")}"
                                       value="${article.linkUri}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.cms.article.form.linkUri.text.help")}</small></span>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/cms/article/articlejs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">

    <#include "/elite/scriptplugin/ueditor.ftl">

    <script src="${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webuploader/webuploader.min.js"></script>

    <script>

        // 图片上传
        jQuery(function () {
            var $list = $('#js_cover-upload-complete'),
                // 优化 retina, 在 retina 下这个值是2
                ratio = window.devicePixelRatio || 1,
                // 缩略图大小
                thumbnailWidth = 120 * ratio,
                thumbnailHeight = 120 * ratio,
                // Web Uploader实例
                uploader;

            // 初始化Web Uploader
            uploader = WebUploader.create({
                // 自动上传
                auto: true,
                // swf文件路径
                swf: '${GLOBAL_RESOURCE_MAP['ELITE_BASE']}/plugins/webuploader/' + '/Uploader.swf',
                // 文件接收服务端。
                server: '${CONTEXT_PATH}/cms/article/uploadcover',

                // 选择文件的按钮。可选。
                // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                pick: '#js_cover-upload',

                // 只允许选择文件，可选。
                accept: {
                    title: 'Images',
                    extensions: 'gif,jpg,jpeg,bmp,png',
                    mimeTypes: 'image/*'
                }

            });

            // 当有文件添加进来的时候
            uploader.on('fileQueued', function (file) {

                var $li = $('<div id="' + file.id + '"' +
                    ' class="dz-preview">' +
                    '<div class="dz-image">' +
                    '<img src="">' +
                    '</div>' +
                    '<div class="dz-details">' +
                    '<div class="dz-filename"><span>' + file.name + '</span></div>' +
                    '</div>' +
                    '<div class="dz-progress"><span class="dz-upload"></span></div>' +
                    '</div>');

                var $img = $li.find('img');

                // $list为容器jQuery实例
                $list.html('');
                $list.append($li);

                // 创建缩略图
                uploader.makeThumb(file, function (error, src) {
                    if (error) {
                        $img.replaceWith('<span>${I18N("message.upload.image.file.thumb.error")}</span>');
                        return;
                    }
                    $img.attr('src', src);
                }, thumbnailWidth, thumbnailHeight);
            });

            // 文件上传过程中创建进度条实时显示
            uploader.on('uploadProgress', function (file, percentage) {
                var $li = $('#' + file.id),
                    $percent = $li.find('.dz-upload');

                // 避免重复创建
                if (!$percent.length) {
                    $percent = $('<span class="dz-upload"></span>')
                        .appendTo($li)
                        .find('.dz-progress');
                }

                $percent.css('width', percentage * 100 + '%');
            });

            // 文件上传成功
            uploader.on('uploadSuccess', function (file, response) {
                $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.succeed")}');
                $('#js_cover-image').val(response.message);
            });

            // 文件上传失败
            uploader.on('uploadError', function (file, reason) {
                $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.fail")}');
            });

            // 完成上传完了，成功或者失败，删除进度条
            uploader.on('uploadComplete', function (file) {
                $('#' + file.id).find('.dz-upload').removeAttr('style');
                var coverImage = $('#js_cover-image').val();
                if (!coverImage) {
                    $('#' + file.id).find('.dz-filename span').text('${I18N("message.upload.file.fail")}');
                }
            });
        });

        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            // 百度编辑器
            var ue = UE.getEditor('ja_article-content', {initialFrameHeight: 500});

            EliteTool.highlightTouchSpin('#js_priority', 1);

            EliteFormValidation.formValidationArticle();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
