<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_generate-form" action="${save}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <input type="hidden" name="table" value="${table}">

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.currentTable")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${table}</p>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.model")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="modelPackageName"
                                       placeholder="${I18N("message.generateCode.form.model.span")}"
                                       value="wang.encoding.mroot.domain.entity">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.generateCode.form.model.span")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.mapper")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="mapperPackageName"
                                       placeholder="${I18N("message.generateCode.form.mapper.span")}"
                                       value="wang.encoding.mroot.dao">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.generateCode.form.mapper.span")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.service")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="service"
                                       placeholder="${I18N("message.generateCode.form.service.span")}"
                                       value="wang.encoding.mroot.service">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.generateCode.form.service.span")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.controller")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="controller"
                                       placeholder="${I18N("message.generateCode.form.controller.span")}"
                                       value="wang.encoding.mroot.controller">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.generateCode.form.controller.span")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.generateCode.form.prefix")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input class="js_switch" type="checkbox"
                                       data-on-color="success" data-off-color="info"
                                       data-on-text="${I18N("message.form.state.normal.btn")}"
                                       data-off-text="${I18N("message.form.state.disable.btn")}"
                                       name="tablePrefix"/>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/role/rolejs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteFormValidation.formValidationGenerate();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
