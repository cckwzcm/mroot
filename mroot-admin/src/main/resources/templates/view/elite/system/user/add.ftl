<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_user-form" action="${save}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.category")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <#list userTypeMap?keys as key>
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input id="js_category-${key}" type="radio"
                                                   value="${key}" name="category"
                                                   <#if !(user.category)??>checked</#if>
                                                    <#if (user.category == key)>checked</#if>
                                                   class="custom-control-input">
                                            <label class="custom-control-label"
                                                   for="js_category-${key}">${userTypeMap[key]}</label>
                                        </div>
                                    </fieldset>
                                </#list>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.username")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="username"
                                       placeholder="${I18N("message.system.user.form.username.text")}"
                                       value="${user.username}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.username.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.password")}</label>
                            <div class="col-md-9">
                                <input type="password" class="form-control" name="password"
                                       placeholder="${I18N("message.system.user.form.password.text")}"
                                       value="${user.password}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.password.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.nickName")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="nickName"
                                       placeholder="${I18N("message.system.user.form.nickName.text")}"
                                       value="${user.nickName}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.nickName.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.email")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="email"
                                       placeholder="${I18N("message.system.user.form.email.text")}"
                                       value="${user.email}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.email.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.phone")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="phone"
                                       placeholder="${I18N("message.system.user.form.phone.text")}"
                                       value="${user.phone}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.phone.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.realName")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="realName"
                                       placeholder="${I18N("message.system.user.form.realName.text")}"
                                       value="${user.realName}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.realName.text.help")}</small></span>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/user/userjs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteFormValidation.formValidationUser();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
