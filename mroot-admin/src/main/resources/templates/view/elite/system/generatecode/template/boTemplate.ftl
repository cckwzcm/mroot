/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系   <707069100@qq.com>      <http//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${boPackageName}.${classPrefix}.${classNameLowerCase};


import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import lombok.EqualsAndHashCode;
import org.hibernate.validator.constraints.Range;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Past;
import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * ${classComment}实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
public class Admin${className}BO implements Serializable {

private static final long serialVersionUID = XL;

<#list generateModels as var>
    <#if "state" != var.camelCaseName && "title" != var.camelCaseName
    && "sole" != var.camelCaseName && "category" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "sort" != var.camelCaseName && "remark" != var.camelCaseName>
        <#if dataId == var.name>
    /**
    * ID
    */
          private ${var.type} ${var.camelCaseName};
        <#else>
      /**
    * ${var.comment}
    */
         private ${var.type} ${var.camelCaseName};
    </#if>
    </#if>
    <#if "category" == var.camelCaseName>
   /**
    * ${var.comment}
    */
     @NotNull(message = "validation.category.range")
    @Range(min = 1, max = 4, message = "validation.category.range")
    private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "sole" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9_]{2,80}$", message = "validation.sole.pattern")
      private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "title" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Pattern(regexp = "^[a-zA-Z0-9_\\u4e00-\\u9fa5]{2,80}$", message = "validation.title.pattern")
     private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "state" == var.camelCaseName>
   /**
    * ${var.comment}
    */
   @NotNull(message = "validation.state.range")
    @Range(min = 1, max = 3, message = "validation.state.range")
     private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "gmtCreate" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Past(message = "validation.gmtCreate.past")
    private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "gmtCreateIp" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @NotNull(message = "validation.gmtCreateIp.pattern")
     private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "gmtModified" == var.camelCaseName >
   /**
    * ${var.comment}
    */
     @Past(message = "validation.gmtModified.past")
    private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "sort" == var.camelCaseName>
   /**
    * ${var.comment}
    */
    @Range(min = 0, message = "validation.sort.range")
  private ${var.type} ${var.camelCaseName};
    </#if>
    <#if "remark" == var.camelCaseName>
   /**
    * ${var.comment}
    */
     @Pattern(regexp = "^[a-zA-Z0-9，。、\\u4e00-\\u9fa5]{0,200}$", message = "validation.remark.pattern")
   private ${var.type} ${var.camelCaseName};
    </#if>
</#list>

   // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ${className}BO class

/* End of file ${className}BO.java */
/* Location: ./src/main/java/wang/encoding/mroot/bo/admin/entity/${classPrefix}/${classNameLowerCase}/${className}BO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
