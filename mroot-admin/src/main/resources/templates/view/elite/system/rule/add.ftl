<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_rule-form" action="${save}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.rule.form.pid")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="pid"
                                        data-placeholder="${I18N("message.system.rule.form.pid.text.help")}">
                                    <option value="1">${I18N("message.system.rule.form.pid.text.help")}</option>
                                    <#list treeRules as topRules>
                                        <option value="${topRules.id}">
                                            ${topRules.title}
                                        </option>
                                        <#list topRules.childrenList as childRules>
                                            <option value="${childRules.id}">
                                                &nbsp;&nbsp;&lfloor;&nbsp;${childRules.title}
                                            </option>
                                            <#list childRules.childrenList as leftRules>
                                                <option value="${leftRules.id}">
                                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&lfloor;&nbsp;&lfloor;&nbsp;${leftRules.title}</option>
                                            </#list>
                                        </#list>
                                    </#list>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.rule.form.pid.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.category")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <#list ruleTypeMap?keys as key>
                                    <fieldset>
                                        <div class="custom-control custom-radio">
                                            <input id="js_category-${key}" type="radio"
                                                   value="${key}" name="category"
                                                   <#if !(rule.category)??>checked</#if>
                                                    <#if (rule.category == key)>checked</#if>
                                                   class="custom-control-input">
                                            <label class="custom-control-label"
                                                   for="js_category-${key}">${ruleTypeMap[key]}</label>
                                        </div>
                                    </fieldset>
                                </#list>
                                <span class="help-block"><small
                                            class="form-control-feedback"></small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.title")}<span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="title"
                                       placeholder="${I18N("message.form.title.text")}"
                                       value="${rule.title}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.title.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.rule.form.url")}
                                <span
                                        class="text-danger">*</span></label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="url"
                                       placeholder="${I18N("message.system.rule.form.url.text")}"
                                       value="${rule.url}">
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.rule.form.url.text.help")}</small></span>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.sort.text")}</label>
                            <div class="col-md-9">
                                <input type="text" class="form-control" name="sort"
                                       placeholder="${I18N("message.form.sort.text")}"
                                       value="${rule.sort}"
                                       id="js_sort"
                                       data-bts-button-down-class="btn btn-white btn-outline"
                                       data-bts-button-up-class="btn btn-white btn-outline"
                                >
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.sort.text.help")}</small></span>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.form.remark")}</label>
                            <div class="col-md-9">
                                <textarea class="form-control" rows="5" name="remark"
                                          placeholder="${I18N("message.form.remark.text")}">${rule.remark}</textarea>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.form.remark.text.help")}</small></span>
                            </div>
                        </div>

                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/rule/rulejs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');

            EliteTool.highlightTouchSpin('#js_sort', ${maxSort});

            EliteFormValidation.formValidationRule();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
