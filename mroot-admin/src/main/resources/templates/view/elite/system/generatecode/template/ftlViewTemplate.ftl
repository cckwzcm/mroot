<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}

    <div class="col-12">
        ${r'<#include "/elite/common/formalert.ftl">'}
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${r"${"}I18N("message.form.head.title")${r"}"}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered">

                    <div class="form-body m-t-20">

                        ${r'<@formTokenAndRefererUrl></@formTokenAndRefererUrl>'}


                        <#list generateModels as var>

                            <#if "id" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.id")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r"${"}${classFirstLowerCaseName}.id${r"}"}</p>
                                    </div>
                                </div>

                            <#elseif "sole" == var.camelCaseName>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.sole")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.sole${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            <#elseif "title" == var.camelCaseName>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.title")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.title${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            <#elseif "state" == var.camelCaseName>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.state")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@status '}${classFirstLowerCaseName}.status${r'></@status>'}</p>
                                    </div>
                                </div>

                            <#elseif "sort" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.sort")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.sort${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            <#elseif "remark" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.remark")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.remark${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            <#elseif "gmtCreate" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.gmtCreate")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@dateFormat '}${classFirstLowerCaseName}.gmtCreate${r'></@dateFormat>'}</p>
                                    </div>
                                </div>
                            <#elseif "gmtCreateIp" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.gmtCreateIp")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.ip${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            <#elseif "gmtModified" == var.camelCaseName >

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.form.gmtModified")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@dateFormat '}${classFirstLowerCaseName}.gmtModified${r'></@dateFormat>'}</p>
                                    </div>
                                </div>
                            <#else>

                                <div class="form-group row">
                                    <label class="control-label text-right col-md-3">${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}")${r"}"}</label>
                                    <div class="col-md-9">
                                        <p class="form-control-static">${r'<@defaultStr '}${classFirstLowerCaseName}.${var.camelCaseName}${r'></@defaultStr>'}</p>
                                    </div>
                                </div>
                            </#if>
                        </#list>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        ${r'<@viewFormOperate></@viewFormOperate>'}
                    </div>

                </form>

            </div>
        </div>


    </div>

${r'</@OVERRIDE>'}

${r'<@OVERRIDE name="PAGE_SCRIPT">'}
${r'<#include "/elite/scriptplugin/tool.ftl">'}
${r'</@OVERRIDE>'}

${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${r"${"}navIndex${r"}"}');

        });
    </script>

${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/elite/common/base.ftl"/>'}
