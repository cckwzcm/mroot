<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="col-12">
        <#include "/elite/common/formalert.ftl">
    </div>

    <div class="col-12">

        <div class="card">

            <div class="card-header bg-info">
                <h4 class="m-b-0 text-white">${I18N("message.form.head.title")}</h4>
            </div>

            <div class="card-body">

                <form class="form-horizontal form-bordered" id="js_user-authorization-form"
                      action="${authorizationSave}"
                      method="post" autocomplete="off">

                    <div class="form-body m-t-20">

                        <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                        <input name="id" type="hidden" value="${user.id}">


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.username")}</label>
                            <div class="col-md-9">
                                <p class="form-control-static">${user.username}</p>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="control-label text-right col-md-3">${I18N("message.system.user.form.role")}</label>
                            <div class="col-md-9">
                                <select class="form-control custom-select js_select2" name="roleId"
                                        data-placeholder="${I18N("message.system.user.form.role.text.help")}">
                                    <option value="">${I18N("message.system.user.form.role.text.help")}</option>
                                    <#list roleList as item>
                                        <option <#if item.id == role.id>selected</#if> value="${item.id}">
                                            ${item.title}
                                        </option>
                                    </#list>
                                </select>
                                <span class="help-block"><small
                                            class="form-control-feedback">${I18N("message.system.user.form.role.text.help")}</small></span>
                            </div>
                        </div>


                    </div>

                    <div class="form-actions">
                        <#-- 提交按钮 -->
                        <@formOperate></@formOperate>
                    </div>

                </form>

            </div>
        </div>


    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/elite/system/user/userjs.ftl">
</@OVERRIDE>

<#include "/elite/scriptplugin/form.ftl">

<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            EliteTool.highlight_top_nav('${navIndex}');


            EliteFormValidation.formValidationUserAuthorization();

        });
    </script>

</@OVERRIDE>

<@EXTENDS name="/elite/common/base.ftl"/>
