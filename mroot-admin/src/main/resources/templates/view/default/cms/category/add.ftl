<@OVERRIDE name="MAIN_CONTENT">


    <#include "/default/common/pagealert.ftl">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.form.head.title")}
    </h3>
    </div>
    </div>
    </div>

<form class="m-form m-form--state m-form--fit m-form--label-align-right" id="category_form"
action="${save}"
      method="post" autocomplete="off">
    <div class="m-portlet__body">

    <#include "/default/common/formalert.ftl">

    <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.pid")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <div class="m-select2 m-select2--air">
    <select id="pidSelect" class="form-control m-select2" name="pid">
    <option value="0">${I18N("message.cms.category.form.pid.text.help")}</option>
    <#list treeCategories as topCategory>

        <option value="${topCategory.id}">
        ${topCategory.title}
        </option>

        <#list topCategory.childrenList as childCategory>
            <option value="${childCategory.id}">
        &nbsp;&nbsp;&lfloor;&nbsp;${childCategory.title}
            </option>
        </#list>
    </#list>
    </select>
    <span class="m-form__help">${I18N("message.cms.category.form.pid.text.help")}</span>
    </div>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.category")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">

    <div class="m-select2 m-select2--air">
<select id="typeSelect" class="form-control m-select2" name="category"
data-placeholder="${I18N("message.form.category.text.help")}">
<option></option>
    <#list categoryTypeMap?keys as key>
        <option <#if (category.category == key)>selected</#if> value="${key}">
        ${categoryTypeMap[key]}
        </option>
    </#list>
    </select>
    <span class="m-form__help">${I18N("message.form.category.text.help")}</span>
    </div>
<div class="m--space-10"></div>

    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.sole")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input type="text" class="form-control m-input" name="sole"
placeholder="${I18N("message.form.sole.text")}"
value="${category.sole}">
    <span class="m-form__help">${I18N("message.form.sole.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.title")}&nbsp;*
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input type="text" class="form-control m-input" name="title"
placeholder="${I18N("message.form.title.text")}"
value="${category.title}">
    <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
    </div>
    </div>


<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.audit")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<div>
						<span class="m-switch m-switch--icon m-switch--info">
							<label>
                                <input type="checkbox" name="auditStr">
                                <span></span>
                            </label>
						</span>
</div>
    <span class="m-form__help">${I18N("message.cms.category.form.audit.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.allow")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<div>
						<span class="m-switch m-switch--icon m-switch--info">
							<label>
                                <input type="checkbox" checked name="allowStr">
                                <span></span>
                            </label>
						</span>
</div>
    <span class="m-form__help">${I18N("message.cms.category.form.allow.text.help")}</span>
    </div>
    </div>


    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.display")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<div>
						<span class="m-switch m-switch--icon m-switch--info">
							<label>
                                <input type="checkbox" checked name="displayStr">
                                <span></span>
                            </label>
						</span>
</div>
    <span class="m-form__help">${I18N("message.cms.category.form.display.text.help")}</span>
    </div>
    </div>


    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.sort")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<input id="sort" type="text" class="form-control m-input" name="sort"
placeholder="${I18N("message.form.sort.text")}"
value="${category.sort}">
    <span class="m-form__help">${I18N("message.form.sort.text.help")}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.remark")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
<textarea class="form-control m-input" id="remark" name="remark"
placeholder="${I18N("message.form.remark.text")}">${category.remark}</textarea>
    <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
    </div>
    </div>

    </div>

    <@formOperate></@formOperate>

    </form>

    </div>


</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/cms/category/categoryjs.ftl">}
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script src="${GLOBAL_RESOURCE_MAP['VENDORS']}/plugins/select2/i18n/zh-CN.js"></script>
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');

        formvalidation.formValidationCategory();

        Tool.highlightSelect2('#pidSelect', <@i18nType language></@i18nType>
            , '${I18N("message.form.select.text")}');

        Tool.highlightSelect2('#typeSelect', <@i18nType language></@i18nType>,
            '${I18N("message.form.select.text")}');

        Tool.highlightTouchSpin('#sort', ${maxSort});

        autosize($('#remark'));
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
