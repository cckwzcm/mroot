<@OVERRIDE name="MAIN_CONTENT">

    <div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
    <div class="m-portlet__head-caption">
    <div class="m-portlet__head-title">
    <h3 class="m-portlet__head-text">
    ${I18N("message.form.head.title")}
    </h3>
    </div>
    </div>
    </div>

    <form class="m-form m-form--state m-form--fit m-form--label-align-right">
    <div class="m-portlet__body">

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.id")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static">${category.id}</span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.category")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.type></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.pid")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr pCategory.title></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.sole")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.sole></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.title")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.title></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.sort")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.sort></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.state")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr  category.status></@defaultStr></span>
    </div>
    </div>

<div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.audit")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@boole category.audit></@boole></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.allow")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@boole category.allow></@boole></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.cms.category.form.display")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@boole category.display></@boole></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.remark")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.remark></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtCreate")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@dateFormat  category.gmtCreate></@dateFormat></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtCreateIp")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@defaultStr category.ip></@defaultStr></span>
    </div>
    </div>

    <div class="form-group m-form__group row">
    <label class="col-form-label col-lg-3 col-sm-12">
    ${I18N("message.form.gmtModified")}
    </label>
    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-form__control-static"><@dateFormat  category.gmtModified></@dateFormat></span>
    </div>
    </div>

    </div>

    <@viewFormOperate></@viewFormOperate>

    </form>

    </div>

</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${navIndex}');
    });
</script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
