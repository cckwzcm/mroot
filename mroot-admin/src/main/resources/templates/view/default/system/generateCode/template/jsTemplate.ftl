<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * ${classComment}提示信息
     */
    var ${className}Validation = function () {

        <#list generateModels as var>
            <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
            "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
            && "gmtModified" != var.camelCaseName
            && "sort" != var.camelCaseName && "remark" != var.camelCaseName
            && "state" != var.camelCaseName && "sole" !=var.camelCaseName && "title" !=var.camelCaseName>
var ${var.camelCaseName}_pattern = '${r"${"}I18N("jquery.validation.${classPrefix}.${classFirstLowerCaseName}.${var.camelCaseName}.pattern")${r"}"}';
            </#if>
            <#if "category" == var.camelCaseName >
             var category_range = '${r"${"}I18N("jquery.validation.category.range")${r"}"}';
            </#if>
            <#if "sole" == var.camelCaseName >
             var sole_pattern = '${r"${"}I18N("jquery.validation.sole.pattern")${r"}"}';
            </#if>
            <#if "title" == var.camelCaseName >
              var title_pattern = '${r"${"}I18N("jquery.validation.title.pattern")${r"}"}';
            </#if>
            <#if "sort" == var.camelCaseName >
                     var sort_range = '${r"${"}I18N("jquery.validation.sort.range")${r"}"}';
            </#if>
            <#if "state" == var.camelCaseName >
                     var state_range = '${r"${"}I18N("jquery.validation.state.range")${r"}"}';
            </#if>
            <#if "remark" == var.camelCaseName >
              var remark_pattern = '${r"${"}I18N("jquery.validation.remark.pattern")${r"}"}';
            </#if>
        </#list>
    return {

                    <#list generateModels as var>
                        <#if "id" != var.camelCaseName && "category" != var.camelCaseName &&
                        "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName
                        && "gmtModified" != var.camelCaseName
                        && "sort" != var.camelCaseName && "remark" != var.camelCaseName
                        && "state" != var.camelCaseName && "sole" !=var.camelCaseName && "title" !=var.camelCaseName>
                                get${var.camelCaseName}Pattern: function () {
                                    return ${var.camelCaseName}_pattern;
                                },
                        </#if>
                        <#if "category" == var.camelCaseName >
               getCategoryRange: function () {
                   return category_range;
               },
                        </#if>
                        <#if "sole" == var.camelCaseName >
                 getSolePattern: function () {
                     return sole_pattern;
                 },
                        </#if>
                        <#if "title" == var.camelCaseName >
              getTitlePattern: function () {
                  return title_pattern;
              },
                        </#if>
                        <#if "state" == var.camelCaseName >
                        getStateRange: function () {
                            return state_range;
                        },
                        </#if>
                        <#if "sort" == var.camelCaseName >
                        getSortRange: function () {
                            return sort_range;
                        },
                        </#if>
                        <#if "remark" == var.camelCaseName >
                getRemarkPattern: function () {
                    return remark_pattern;
                }
                        </#if>
                    </#list>

        // -------------------------------------------------------------------------------------------------

    }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
