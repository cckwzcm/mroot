<#-- /* 主要部分 */ -->
${r'<@OVERRIDE name="MAIN_CONTENT">'}


${r'<#include "/default/common/pagealert.ftl">'}

<#-- 内容开始 -->
<div class="m-portlet m-portlet--mobile">

    <div class="m-portlet__head">
        <div class="m-portlet__head-caption">
            <div class="m-portlet__head-title">
                <h3 class="m-portlet__head-text">
                ${r"${"}I18N("message.form.head.title")${r"}"}
                </h3>
            </div>
        </div>
    </div>

<#-- 表单开始 -->
    <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="${classFirstLowerCaseName}_form"
          action="${r"${"}save${r"}"}"
          method="post" autocomplete="off">
        <div class="m-portlet__body">

        ${r'<#include "/default/common/formalert.ftl">'}

        ${r'<@formTokenAndRefererUrl></@formTokenAndRefererUrl>'}

<#list generateModels as var>
    <#if "id" != var.camelCaseName && "state" != var.camelCaseName &&
    "gmtCreate" != var.camelCaseName && "gmtCreateIp" != var.camelCaseName  && "sole" != var.camelCaseName
    && "title" != var.camelCaseName
    && "gmtModified" != var.camelCaseName && "category" != var.camelCaseName &&  "sort" != var.camelCaseName && "remark" != var.camelCaseName>
             <div class="form-group m-form__group row">
                 <label class="col-form-label col-lg-3 col-sm-12">
                     ${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}")${r"}"}&nbsp;*
                 </label>
                 <div class="col-lg-4 col-md-9 col-sm-12">
                     <input type="text" class="form-control m-input" name="${var.camelCaseName}"
                            placeholder="${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text")${r"}"}"
                            value="${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}" >
                     <span class="m-form__help">${r"${"}I18N("message.${classPrefix}.${classFirstLowerCaseName}.form.${var.camelCaseName}.text.help")${r"}"}</span>
                 </div>
             </div>
    </#if>
    <#if "category" == var.camelCaseName >
           <div class="form-group m-form__group row">
               <label class="col-form-label col-lg-3 col-sm-12">
                   ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}&nbsp;*
               </label>
               <div class="col-lg-4 col-md-9 col-sm-12">
                   <div class="m-radio-inline">
                            <label class="m-radio m-radio--state-success">
                                <input type="radio" name="category"
                                <span></span>
                            </label>
                   </div>
                   <span class="m-form__help">${r"${"}I18N("message.form.${var.camelCaseName}.text.help")${r"}"}</span>
               </div>
           </div>
    </#if>
    <#if "sole" == var.camelCaseName >
                  <div class="form-group m-form__group row">
                      <label class="col-form-label col-lg-3 col-sm-12">
                          ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}&nbsp;*
                      </label>
                      <div class="col-lg-4 col-md-9 col-sm-12">
                          <input type="text" class="form-control m-input" name="${var.camelCaseName}"
                                 placeholder="${r"${"}I18N("message.form.${var.camelCaseName}.text")${r"}"}"
                          value="${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}" >
                          <span class="m-form__help">${r"${"}I18N("message.form.${var.camelCaseName}.text.help")${r"}"}</span>
                      </div>
                  </div>
    </#if>
    <#if "title" == var.camelCaseName >
                  <div class="form-group m-form__group row">
                      <label class="col-form-label col-lg-3 col-sm-12">
                          ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}&nbsp;*
                      </label>
                      <div class="col-lg-4 col-md-9 col-sm-12">
                          <input type="text" class="form-control m-input" name="${var.camelCaseName}"
                                 placeholder="${r"${"}I18N("message.form.${var.camelCaseName}.text")${r"}"}"
                          value="${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}" >
                          <span class="m-form__help">${r"${"}I18N("message.form.${var.camelCaseName}.text.help")${r"}"}</span>
                      </div>
                  </div>
    </#if>
    <#if "sort" == var.camelCaseName >
         <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>
                  <div class="form-group m-form__group row">
                      <label class="col-form-label col-lg-3 col-sm-12">
                          ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                      </label>
                      <div class="col-lg-4 col-md-9 col-sm-12">
                          <input id="sort" type="text" class="form-control m-input" name="${var.camelCaseName}"
                                 placeholder="${r"${"}I18N("message.form.${var.camelCaseName}.text")${r"}"}"
                          value="${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}" >
                          <span class="m-form__help">${r"${"}I18N("message.form.${var.camelCaseName}.text.help")${r"}"}</span>
                      </div>
                  </div>
    </#if>
    <#if "remark" == var.camelCaseName >
                  <div class="form-group m-form__group row">
                      <label class="col-form-label col-lg-3 col-sm-12">
                          ${r"${"}I18N("message.form.${var.camelCaseName}")${r"}"}
                      </label>
                      <div class="col-lg-4 col-md-9 col-sm-12">
                          <textarea id="remark" class="form-control m-input" name="${var.camelCaseName}"
                          placeholder="${r"${"}I18N("message.form.${var.camelCaseName}.text")${r"}"}" >${r"${"}${classFirstLowerCaseName}.${var.camelCaseName}${r"}"}</textarea>
                          <span class="m-form__help">${r"${"}I18N("message.form.${var.camelCaseName}.text.help")${r"}"}</span>
                      </div>
                  </div>
    </#if>
</#list>
        </div>

    <#-- 提交按钮 -->
    ${r'<@formOperate></@formOperate>'}

    </form>
<#-- 表单结束 -->

</div>
<#-- 内容结束 -->


${r'</@OVERRIDE>'}
${r'<@OVERRIDE name="PAGE_MESSAGE">'}
${r'<#include'} "/default/${classPrefix}/${classNameLowerCase}/${classNameLowerCase}js.ftl">${r'}'}
${r'</@OVERRIDE>'}
${r'<#include "/default/scriptplugin/form.ftl">'}
${r'<@OVERRIDE name="CUSTOM_SCRIPT">'}
<script>
    jQuery(document).ready(function () {
        // 顶部导航高亮
        Tool.highlight_top_nav('${r"${"}navIndex${r"}"}');
        formvalidation.formValidation${className}();

        autosize($('#remark'));
    });
</script>
<#-- /* /.页面级别script结束 */ -->
${r'</@OVERRIDE>'}

${r'<@EXTENDS name="/default/common/base.ftl"/>'}
