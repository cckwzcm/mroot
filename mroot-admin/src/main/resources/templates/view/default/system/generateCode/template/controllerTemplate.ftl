/*
* // +-------------------------------------------------------------------------------------------------
* // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
* // +-------------------------------------------------------------------------------------------------
* // |                             独在异乡为异客         每逢佳节倍思亲
* // +-------------------------------------------------------------------------------------------------
* // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
* // +-------------------------------------------------------------------------------------------------
*/

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------



package ${controllerPackageName}.${classPrefix}.${classNameLowerCase};



import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import wang.encoding.mroot.admin.common.annotation.RequestLogAnnotation;
import wang.encoding.mroot.admin.common.constant.RequestLogConstant;
import wang.encoding.mroot.admin.common.controller.AdminBaseController;
import wang.encoding.mroot.common.annotation.FormToken;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.component.LocaleMessageSourceComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import ${adminServicePackageName}.${classPrefix}.Admin${className}Service;
import ${voPackageName}.${classPrefix}.${classNameLowerCase}.Admin${className}GetVO;

import java.math.BigInteger;
import java.time.Instant;
import java.util.Date;


/**
* 后台 ${classComment} 控制器
*
*@author ErYang
*/
@RestController
@RequestMapping("/${classPrefix}/${classNameLowerCase}")
public class ${className}Controller extends AdminBaseController {


         /**
     * 模块
     */
    private static final String MODULE_NAME = "/${classPrefix}/${classNameLowerCase}";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/${classPrefix}/${classNameLowerCase}";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "${classFirstLowerCaseName}";
    /**
     * 列表页面
     */
    private static final String INDEX = "/index";
    private static final String INDEX_URL = MODULE_NAME + INDEX;
    private static final String INDEX_VIEW = VIEW_PATH + INDEX;
    /**
     * 新增
     */
    private static final String ADD = "/add";
    private static final String ADD_URL = MODULE_NAME + ADD;
    private static final String ADD_VIEW = VIEW_PATH + ADD;
    /**
     * 保存
     */
    private static final String SAVE = "/save";
    private static final String SAVE_URL = MODULE_NAME + SAVE;
    /**
     * 修改
     */
    private static final String EDIT = "/edit";
    private static final String EDIT_URL = MODULE_NAME + EDIT;
    private static final String EDIT_VIEW = VIEW_PATH + EDIT;
    /**
     * 更新
     */
    private static final String UPDATE = "/update";
    private static final String UPDATE_URL = MODULE_NAME + UPDATE;
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_URL = MODULE_NAME + VIEW;
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;
    /**
     * 删除
     */
    private static final String DELETE = "/delete";
    private static final String DELETE_URL = MODULE_NAME + DELETE;

    private static final String DELETE_BATCH = "/deletebatch";
    private static final String DELETE_BATCH_URL = MODULE_NAME + DELETE_BATCH;
    /**
     * 回收站
     */
    private static final String RECYCLE_BIN_INDEX = "/recyclebin";
    private static final String RECYCLE_BIN_INDEX_URL = MODULE_NAME + RECYCLE_BIN_INDEX;
    private static final String RECYCLE_BIN_INDEX_VIEW = VIEW_PATH + RECYCLE_BIN_INDEX;
    /**
     * 恢复
     */
    private static final String RECOVER = "/recover";
    private static final String RECOVER_URL = MODULE_NAME + RECOVER;

    private static final String RECOVER_BATCH = "/recoverbatch";
    private static final String RECOVER_BATCH_URL = MODULE_NAME + RECOVER_BATCH;

    private final Admin${className}Service admin${className}Service;

    @Autowired
    public ${className}Controller(Admin${className}Service admin${className}Service) {
        this.admin${className}Service = admin${className}Service;
    }

    // -------------------------------------------------------------------------------------------------

      /**
     * 列表页面
     *
     * @return ModelAndView
     */
     @RequiresPermissions(value = INDEX_URL)
    @RequestMapping(INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_INDEX)
     public ModelAndView index() throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(INDEX_VIEW, INDEX_URL);

Admin${className}GetVO admin${className}GetVO = new Admin${className}GetVO();
        String title = super.request.getParameter("title");
        if (StringUtils.isNotBlank(title)) {
            admin${className}GetVO.setTitle(title);
            modelAndView.addObject("title", title);
        }
        admin${className}GetVO.setState(StateEnum.NORMAL.getKey());

    Page<Admin${className}GetVO> pageInt = new Page<>();
     IPage<Admin${className}GetVO> pageAdmin = admin${className}Service
    .list2page(super.initPage(pageInt), admin${className}GetVO, Admin${className}GetVO.ID, false);
    modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

        modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
        modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
        modelAndView.addObject(AdminBaseController.ADD_NAME, contextPath + ADD_URL);
        modelAndView.addObject(AdminBaseController.EDIT_NAME, contextPath + EDIT_URL);
        modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
        modelAndView.addObject(AdminBaseController.DELETE_NAME, contextPath + DELETE_URL);
        modelAndView.addObject(AdminBaseController.DELETE_BATCH_NAME, contextPath + DELETE_BATCH_URL);
        modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 添加页面
    *
    * @return ModelAndView
    */
    @RequiresPermissions(ADD_URL)
    @RequestMapping(ADD)
     @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_ADD)
    @FormToken(init = true)
    public ModelAndView add() throws ControllerException {
    ModelAndView modelAndView = super.initModelAndView(ADD_VIEW, ADD_URL);

    // 设置上个请求地址
    super.initRefererUrl(MODULE_NAME);

        modelAndView.addObject(AdminBaseController.SAVE_NAME, contextPath + SAVE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

    return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 处理保存
    *
    * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO
    * @return Object
    */
    @RequiresPermissions(SAVE_URL)
    @RequestMapping(SAVE)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_SAVE)
    @FormToken(remove = true)
    public Object save(Admin${className}GetVO ${classFirstLowerCaseName}GetVO) throws ControllerException {
    if (super.isAjaxRequest()) {
    ResultData failResult = ResultData.fail();

    // 创建 Admin${className}GetVO 对象
    Admin${className}GetVO admin${className}GetVO = this.initAddData(${classFirstLowerCaseName}GetVO);

    // Hibernate Validation  验证数据
    String validationResult = admin${className}Service.validation${className}(admin${className}GetVO);
    if (StringUtils.isNotBlank(validationResult)) {
    return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
    }

    // 验证数据唯一性
    boolean flag = this.validationAddData(admin${className}GetVO, failResult);
    if (!flag) {
    return super.initErrorValidationJSONObject(failResult);
    }

    // 新增 ${classComment}
    boolean result = admin${className}Service.save(admin${className}GetVO);
    return super.initSaveJSONObject(result);

        } else {
    return super.initErrorRedirectUrl();
        }
        }

    // -------------------------------------------------------------------------------------------------

    /**
    * 编辑页面
    *
    * @return ModelAndView
    */
    @RequiresPermissions(EDIT_URL)
    @RequestMapping(EDIT + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_EDIT)
    @FormToken(init = true)
    public ModelAndView edit(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
    throws ControllerException {
    ModelAndView modelAndView = super.initModelAndView(EDIT_VIEW, EDIT_URL);


    BigInteger idValue = super.getId(id);
    if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
    return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
    }
    // 数据真实性
    Admin${className}GetVO ${classFirstLowerCaseName}GetVO = admin${className}Service.getById(idValue);
    if (null == ${classFirstLowerCaseName}GetVO) {
    return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
    }
    if (StateEnum.DELETE.getKey() == ${classFirstLowerCaseName}GetVO.getState()) {
    return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
    }

    modelAndView.addObject(VIEW_MODEL_NAME,  ${classFirstLowerCaseName}GetVO);
    // 设置上个请求地址
    super.initRefererUrl(MODULE_NAME);

    // 正常状态
    modelAndView.addObject("stateNormal", StateEnum.NORMAL.getKey());
        modelAndView.addObject(AdminBaseController.UPDATE_NAME, contextPath + UPDATE_URL);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

    return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 处理更新
    *
    * @return Object
    */
    @RequiresPermissions(UPDATE_URL)
    @RequestMapping(UPDATE)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_UPDATE)
    @FormToken(remove = true)
    public Object update(Admin${className}GetVO ${classFirstLowerCaseName}GetVO) throws ControllerException {
    if (super.isAjaxRequest()) {
    ResultData failResult = ResultData.fail();
    // 验证数据
    BigInteger idValue = super.getId();
    if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
    return super.initErrorCheckJSONObject(failResult);
    }

    // 数据真实性
    Admin${className}GetVO ${classFirstLowerCaseName}GetVOBefore = admin${className}Service.getById(idValue);
    if (null == ${classFirstLowerCaseName}GetVOBefore) {
    return super.initErrorCheckJSONObject(failResult);
    }
    if (StateEnum.DELETE.getKey() == ${classFirstLowerCaseName}GetVOBefore.getState()) {
    return super.initErrorCheckJSONObject(failResult);
    }

// 创建 Admin${className}GetVO 对象
Admin${className}GetVO validation${className} = BeanMapperComponent.map(${classFirstLowerCaseName}GetVOBefore, Admin${className}GetVO.class);
validation${className}.setCategory(${classFirstLowerCaseName}GetVOBefore.getCategory());
String stateStr = super.getStatusStr();
if (StringUtils.isNotBlank(stateStr) && stateStr.equals(configConst.getBootstrapSwitchEnabled())) {
validation${className}.setState(StateEnum.NORMAL.getKey());
} else {
validation${className}.setState(StateEnum.DISABLE.getKey());
}

    this.initEditData(validation${className},${classFirstLowerCaseName}GetVO);

    // Hibernate Validation 验证数据
    String validationResult = admin${className}Service.validation${className}(validation${className});
    if (StringUtils.isNotBlank(validationResult)) {
    return super.initErrorHibernateValidationJSONObject(failResult, validationResult);
    }


    // 验证数据唯一性
    boolean flag = this.validationEditData(validation${className}, ${classFirstLowerCaseName}GetVOBefore, failResult);
    if (!flag) {
    return super.initErrorValidationJSONObject(failResult);
    }

    // 修改 ${classComment}
    boolean result = admin${className}Service.update(validation${className});
    return super.initUpdateJSONObject(result);
        }else{
    return super.initErrorRedirectUrl();
        }
}

    // -------------------------------------------------------------------------------------------------

    /**
    * 查看页面
    *
    * @param id                 String
    * @param redirectAttributes RedirectAttributes
    * @return ModelAndView
    */
    @RequiresPermissions(VIEW_URL)
    @RequestMapping(VIEW + "/{id}")
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_VIEW)
    public ModelAndView view(@PathVariable(ID_NAME) String id, RedirectAttributes redirectAttributes)
    throws ControllerException {
    ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW, VIEW_URL);

    BigInteger idValue = super.getId(id);
    // 验证数据
    if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
    return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
    }
    // 数据真实性
    Admin${className}GetVO ${classFirstLowerCaseName}GetVO = admin${className}Service.getById(idValue);
    if (null == ${classFirstLowerCaseName}GetVO) {
    return initValidationResult2Get(modelAndView, redirectAttributes, INDEX_URL);
    }
    modelAndView.addObject(VIEW_MODEL_NAME, ${classFirstLowerCaseName}GetVO);
    // 设置上个请求地址
    super.initRefererUrl(MODULE_NAME);
        modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);

    return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 删除
    *
    * @param id String
    * @return JSONObject
    */
    @RequiresPermissions(DELETE_URL)
    @RequestMapping(DELETE + "/{id}")
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_DELETE)
    public JSONObject delete(@PathVariable(ID_NAME) String id) throws ControllerException {
    if (super.isAjaxRequest()) {
    BigInteger idValue = super.getId(id);
    // 验证数据
    if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
    return super.initErrorCheckJSONObject();
    }
    // 验证真实性
    Admin${className}GetVO ${classFirstLowerCaseName}GetVO = admin${className}Service.getById(idValue);
    if (null == ${classFirstLowerCaseName}GetVO) {
    return super.initErrorCheckJSONObject();
    }

    // 删除 ${classComment}
    boolean result = admin${className}Service.remove2StatusById(${classFirstLowerCaseName}GetVO.getId());
    return super.initDeleteJSONObject(result);
        }

    return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 批量删除
    *
    * @return JSONObject
    */
    @RequiresPermissions(DELETE_BATCH_URL)
    @RequestMapping(DELETE_BATCH)
    @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
            title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_DELETE_BATCH)
    public JSONObject deleteBatch() throws ControllerException {
    if (super.isAjaxRequest()) {
    return super.returnDeleteJSONObject(Admin${className}Service.class, admin${className}Service);
    }
    return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

    /**
    * 回收站页面
    *
    * @return ModelAndView
    */
    @RequiresPermissions(RECYCLE_BIN_INDEX_URL)
    @RequestMapping(RECYCLE_BIN_INDEX)
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
    title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECYCLE_BIN_INDEX)
    public ModelAndView recycleBin() throws ControllerException {
    ModelAndView modelAndView = super.initModelAndView(RECYCLE_BIN_INDEX_VIEW, RECYCLE_BIN_INDEX_URL);


    Admin${className}GetVO admin${className}GetVO = new Admin${className}GetVO();
    String title = super.request.getParameter("title");
    if (StringUtils.isNotBlank(title)) {
    admin${className}GetVO.setTitle(title);
    modelAndView.addObject("title", title);
    }
    admin${className}GetVO.setState(StateEnum.DELETE.getKey());

        Page<Admin${className}GetVO> pageInt = new Page<>();
    IPage<Admin${className}GetVO> pageAdmin = admin${className}Service
        .list2page(super.initPage(pageInt), admin${className}GetVO, Admin${className}GetVO.ID, false);
        modelAndView.addObject(VIEW_PAGE_NAME, pageAdmin);

                modelAndView.addObject(AdminBaseController.MODULE, MODULE_NAME);
                modelAndView.addObject(AdminBaseController.INDEX_NAME, contextPath + INDEX_URL);
                modelAndView.addObject(AdminBaseController.RECYCLE_BIN_NAME, contextPath + RECYCLE_BIN_INDEX_URL);
                modelAndView.addObject(AdminBaseController.VIEW_NAME, contextPath + VIEW_URL);
                modelAndView.addObject(AdminBaseController.NAV_INDEX_URL_NAME, contextPath + INDEX_URL);
                return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

        /**
        * 恢复
        *
        * @param id String
        * @return JSONObject
        */
        @RequiresPermissions(RECOVER_URL)
        @RequestMapping(RECOVER + "/{id}")
        @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
    title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECOVER)
        public JSONObject recover(@PathVariable(ID_NAME) String id) {
        if (super.isAjaxRequest()) {
        BigInteger idValue = super.getId(id);
        // 验证数据
        if (null == idValue || BigIntegerUtils.gt(BigInteger.ZERO, idValue)) {
        super.initErrorCheckJSONObject();
        }

        Admin${className}GetVO ${classFirstLowerCaseName}GetVO = admin${className}Service.getById(idValue);
        if (null != ${classFirstLowerCaseName}GetVO) {
        // 恢复 ${classComment}
        boolean result = admin${className}Service.recoverById(${classFirstLowerCaseName}GetVO.getId());
        return super.initRecoverJSONObject(result);
        } else {
        super.initErrorCheckJSONObject();
        }
    }
        return super.initReturnErrorJSONObject();
    }

    // -------------------------------------------------------------------------------------------------

        /**
        * 批量恢复
        *
        * @return JSONObject
        */
        @RequiresPermissions(RECOVER_BATCH_URL)
        @RequestMapping(RECOVER_BATCH)
        @ResponseBody
    @RequestLogAnnotation(model = RequestLogConstant.ADMIN_MODEL,
    title = RequestLogConstant.ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECOVER_BATCH)
        public JSONObject recoverBatch() throws ControllerException {
        if (super.isAjaxRequest()) {
        return super.returnRecoverJSONObject(Admin${className}Service.class, admin${className}Service);
        }
        return super.initReturnErrorJSONObject();
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 合法性
        *
        * @param admin${className}GetVO Admin${className}GetVO   ${classComment}
        * @return Admin${className}GetVO
        */
        private Admin${className}GetVO initAddData(@NotNull final Admin${className}GetVO admin${className}GetVO) {
        Admin${className}GetVO add${className}GetVO = BeanMapperComponent.map(admin${className}GetVO, Admin${className}GetVO.class);
        // IP
        add${className}GetVO.setGmtCreateIp(super.getIp());
        add${className}GetVO.setState(StateEnum.NORMAL.getKey());
        add${className}GetVO.setGmtCreate(Date.from(Instant.now()));
        return add${className}GetVO;
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 合法性
        *
        * @param edit${className}GetVO Admin${className}GetVO  ${classComment}
        * @param form${className}GetVO Admin${className}GetVO  ${classComment}
        */
        private void initEditData(@NotNull final Admin${className}GetVO edit${className}GetVO,@NotNull final Admin${className}GetVO form${className}GetVO) {
        <#list generateModels as var>
            edit${className}GetVO.set${var.firstCapitalizeCamelCaseName}(form${className}GetVO.get${var.firstCapitalizeCamelCaseName}());
        </#list>
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 数据的唯一性
        *
        * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO 配置
        * @param failResult  ResultData
        * @return boolean true(通过)/false(未通过)
        */
        private boolean validationAddData(@NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO,
        @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        Admin${className}GetVO titleExist = admin${className}Service.getByTitle(${classFirstLowerCaseName}GetVO.getTitle());
        if (null != titleExist) {
        message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
        failResult.set(GlobalMessage.MESSAGE, message);
        return false;
        }

        // 标识是否存在
        Admin${className}GetVO soleExist = admin${className}Service.getBySole(${classFirstLowerCaseName}GetVO.getSole());
        if (null != soleExist) {
        message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
        failResult.set(GlobalMessage.MESSAGE, message);
        return false;
        }
        return true;
        }

        // -------------------------------------------------------------------------------------------------

        /**
        * 验证数据 数据的唯一性
        *
        * @param ${classFirstLowerCaseName}GetVO Admin${className}GetVO 配置
        * @param failResult  ResultData
        * @return boolean true(通过)/false(未通过)
        */
        private boolean validationEditData(@NotNull final Admin${className}GetVO new${className}GetVO,
        @NotNull final Admin${className}GetVO ${classFirstLowerCaseName}GetVO, @NotNull final ResultData failResult) {
        String message;

        // 名称是否存在
        boolean titleExist = admin${className}Service
        .propertyUnique(Admin${className}GetVO.TITLE, new${className}GetVO.getTitle(), ${classFirstLowerCaseName}GetVO.getTitle());
        if (!titleExist) {
        message = LocaleMessageSourceComponent.getMessage(MESSAGE_TITLE_EXIST_NAME);
        failResult.set(GlobalMessage.MESSAGE, message);
        return false;
        }

        // 标识是否存在
        boolean soleExist = admin${className}Service
        .propertyUnique(Admin${className}GetVO.SOLE, new${className}GetVO.getSole(), ${classFirstLowerCaseName}GetVO.getSole());
        if (!soleExist) {
        message = LocaleMessageSourceComponent.getMessage(MESSAGE_SOLE_EXIST_NAME);
        failResult.set(GlobalMessage.MESSAGE, message);
        return false;
        }
        return true;
        }

        // -------------------------------------------------------------------------------------------------

            /**
        * ${classComment}
        */
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_INDEX =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.index";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_ADD =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.add";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_SAVE =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.save";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_EDIT =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.edit";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_UPDATE =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.update";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_VIEW =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.view";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_DELETE =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.delete";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_DELETE_BATCH =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.delete.batch";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECYCLE_BIN_INDEX =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recycleBin";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECOVER =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recover";
        public static final String  ADMIN_MODEL_${classPrefixUppercase}_${classNameUppercase}_RECOVER_BATCH =
        "message.aop.requestLog.model.admin.${classPrefix}.${classFirstLowerCaseName}.recover.batch";

        }

// -----------------------------------------------------------------------------------------------------

        // End ${className}Controller class

        /* End of file ${className}Controller.java */
        /* Location: ./src/main/java/wang/encoding/mroot/admin/controller/${classPrefix}/${classNameLowerCase}/${className}Controller.java */

        // -----------------------------------------------------------------------------------------------------
        // +----------------------------------------------------------------------------------------------------
        // |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
        // +----------------------------------------------------------------------------------------------------
        // -----------------------------------------------------------------------------------------------------
