<@OVERRIDE name="MAIN_CONTENT">


    <#include "/default/common/pagealert.ftl">

<#-- 内容开始 -->
    <div class="m-portlet m-portlet--mobile">

        <div class="m-portlet__head">
            <div class="m-portlet__head-caption">
                <div class="m-portlet__head-title">
                    <h3 class="m-portlet__head-text">
                        ${I18N("message.form.head.title")}
                    </h3>
                </div>
            </div>
        </div>

        <#-- 表单开始 -->
        <form class="m-form m-form--state m-form--fit m-form--label-align-right" id="config_form"
              action="${update}"
              method="post" autocomplete="off">
            <div class="m-portlet__body">

                <#include "/default/common/formalert.ftl">

                <input name="id" type="hidden" value="${config.id}">

                <@formTokenAndRefererUrl></@formTokenAndRefererUrl>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.category")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">

                        <div class="m-select2 m-select2--air">
                            <select id="typeSelect" class="form-control m-select2" name="category"
                                    data-placeholder="${I18N("message.form.category.text.help")}">
                                <option></option>
                                <#list configTypeMap?keys as key>
                                    <option <#if (config.category == key)>selected</#if> value="${key}">
                                        ${configTypeMap[key]}
                                    </option>
                                </#list>
                            </select>
                            <span class="m-form__help">${I18N("message.form.category.text.help")}</span>
                        </div>
                        <div class="m--space-10"></div>

                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.sole")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control m-input" name="sole"
                               placeholder="${I18N("message.form.sole.text")}"
                               value="${config.sole}" readonly>
                        <span class="m-form__help">${I18N("message.form.sole.text.help")}</span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.title")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
                        <input type="text" class="form-control m-input" name="title"
                               placeholder="${I18N("message.form.title.text")}"
                               value="${config.title}"
                        >
                        <span class="m-form__help">${I18N("message.form.title.text.help")}</span>
                    </div>
                </div>

                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.system.config.form.content")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
<textarea class="form-control" rows="3" name="content" id="content"
          placeholder="${I18N("message.system.config.form.content.text")}"
>${config.content}</textarea>
                        <span class="m-form__help">${I18N("message.system.config.form.content.text.help")}</span>
                    </div>
                </div>

                <div class="m-form__group form-group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.state")}&nbsp;*
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
    <span class="m-switch m-switch--icon m-switch--info">
    <label>
<input type="checkbox" <#if (stateNormal == config.state)>checked</#if> name="stateStr">
<span></span>
    </label>
    </span>
                    </div>
                </div>

                <div class="m-form__seperator m-form__seperator--dashed m-form__seperator--space"></div>


                <div class="form-group m-form__group row">
                    <label class="col-form-label col-lg-3 col-sm-12">
                        ${I18N("message.form.remark")}
                    </label>
                    <div class="col-lg-4 col-md-9 col-sm-12">
<textarea class="form-control" rows="3" name="remark" id="remark"
          placeholder="${I18N("message.form.remark.text")}"
>${config.remark}</textarea>
                        <span class="m-form__help">${I18N("message.form.remark.text.help")}</span>
                    </div>
                </div>


            </div>

            <#-- 提交按钮 -->
            <@formOperate></@formOperate>

        </form>
        <#-- 表单结束 -->

    </div>
<#-- 内容结束 -->

</@OVERRIDE>
<@OVERRIDE name="PAGE_MESSAGE">
    <#include "/default/system/config/configjs.ftl">}
</@OVERRIDE>
<#include "/default/scriptplugin/form.ftl">
<@OVERRIDE name="CUSTOM_SCRIPT">
    <script>
        jQuery(document).ready(function () {

            // 顶部导航高亮
            Tool.highlight_top_nav('${navIndex}');

            formvalidation.formValidationConfig();

            Tool.highlightSelect2('#typeSelect', <@i18nType language></@i18nType>
                , '${I18N("message.form.select.text")}');


            autosize($('#value'));

            autosize($('#remark'));
        });
    </script>
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
