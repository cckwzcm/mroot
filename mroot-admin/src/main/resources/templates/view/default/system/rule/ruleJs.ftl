<script>

    /**
     * 权限提示信息
     */
    var RuleValidation = function () {

        var pid_range = '${I18N("jquery.validation.system.rule.pid.range")}';
        var type_range = '${I18N("jquery.validation.type.range")}';
        var name_pattern = '${I18N("jquery.validation.sole.pattern")}';
        var title_pattern = '${I18N("jquery.validation.title.pattern")}';
        var url_pattern = '${I18N("jquery.validation.system.rule.url.pattern")}';
        var sort_range = '${I18N("jquery.validation.sort.range")}';
        var remark_pattern = '${I18N("jquery.validation.remark.pattern")}';

        return {

            getPidRange: function () {
                return pid_range;
            },

            getTypeRange: function () {
                return type_range;
            },

            getNamePattern: function () {
                return name_pattern;
            },

            getTitlePattern: function () {
                return title_pattern;
            },

            getUrlPattern: function () {
                return url_pattern;
            },

            getSortRange: function () {
                return sort_range;
            },

            getRemarkPattern: function () {
                return remark_pattern;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // -------------------------------------------------------------------------------------------------

</script>
