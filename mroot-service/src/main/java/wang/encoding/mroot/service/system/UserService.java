/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system;


import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseService;
import wang.encoding.mroot.domain.entity.system.UserDO;

/**
 * 用户 Service
 *
 * @author ErYang
 */
public interface UserService extends BaseService<UserDO> {


    /**
     * 登录
     *
     * @param username String 用户名
     * @param password String 密码
     * @return ResultData
     */
    UserDO login(@NotNull final String username, @NotNull final String password);

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过用户和密码查询
     *
     * @param username String 用户名
     * @param password String 密码
     * @return UserDO
     */
    UserDO getByUsernameAndPassword(@NotNull final String username, @NotNull final String password);

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End UserService interface

/* End of file UserService.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/UserService.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
