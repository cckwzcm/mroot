/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl;


import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.dao.system.ConfigDAO;
import wang.encoding.mroot.domain.entity.system.ConfigDO;
import wang.encoding.mroot.service.system.ConfigService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * 配置 Service 实现类
 *
 * @author ErYang
 */
@Service
public class ConfigServiceImpl extends BaseServiceImpl<ConfigDAO, ConfigDO> implements ConfigService {


    @Value("${spring.datasource.dynamic.datasource.master.url}")
    private String datasourceUrl;


    /**
     * 根据条件得到表集合 用于分页 代码生成器专用
     *
     * @param page Page
     * @param tableArray List<String>
     * @return IPage
     */
    @Override
    public IPage<Map<String, String>> listTableByMap(@NotNull final Page<Map<String, String>> page,
            final List<String> tableArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("dbName", this.getDbName());
        if (ListUtils.isNotEmpty(tableArray)) {
            params.put("tableArray", tableArray);
        }
        List<Map<String, String>> total = baseMapper.listTableByMap(params);
        page.setTotal((long) total.size());
        params.put("beginRow", (page.getCurrent() - 1) * page.getSize());
        params.put("pageSize", page.getSize());
        total = baseMapper.listTableByMap(params);
        page.setRecords(total);
        return page;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表集合 代码生成器专用
     *
     * @param tableName String 表名
     * @return List
     */
    @Override
    public List<Map<String, String>> getTableByTableName(@NotNull final String tableName) {
        Map<String, String> params = new HashMap<>(16);
        params.put("dbName", this.getDbName());
        params.put("tableName", tableName);
        return baseMapper.getTableByTableName(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据条件得到表详情 代码生成器专用
     *
     * @param tableName String
     * @return List
     */
    @Override
    public List<Map<String, String>> getTableDetailByTableName(@NotNull final String tableName) {
        Map<String, String> params = new HashMap<>(16);
        params.put("tableName", tableName);
        params.put("dbName", getDbName());
        return baseMapper.getTableDetailByMap(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到数据库名称
     */
    private String getDbName() {
        String url = datasourceUrl.substring(0, datasourceUrl.indexOf("?") + 1);
        return url.substring(url.lastIndexOf("/") + 1, datasourceUrl.indexOf("?"));
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigServiceImpl class

/* End of file ConfigServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/impl/ConfigServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
