/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.system.impl;


import org.apache.commons.lang3.ArrayUtils;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.service.BaseServiceImpl;
import wang.encoding.mroot.common.util.collection.CollectionUtils;
import wang.encoding.mroot.dao.system.RuleDAO;
import wang.encoding.mroot.domain.entity.system.RuleDO;
import wang.encoding.mroot.service.system.RuleService;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TreeSet;

/**
 * 权限 Service 实现类
 *
 * @author ErYang
 */
@Service
public class RuleServiceImpl extends BaseServiceImpl<RuleDAO, RuleDO> implements RuleService {


    /**
     * 根据角色 ID 查询权限
     *
     * @param roleId BigInteger 角色id
     * @return TreeSet<RuleDO>
     */
    @Override
    public TreeSet<RuleDO> listByRoleId(@NotNull final BigInteger roleId) {
        TreeSet<RuleDO> ruleSet = baseMapper.listByRoleId(roleId);
        if (CollectionUtils.isNotEmpty(ruleSet)) {
            return ruleSet;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0、类型小于 4 权限集合
     *
     * @return List
     */
    @Override
    public List<RuleDO> listPidGt0AndTypeLt4() {
        List<RuleDO> list = baseMapper.listPidGt0AndTypeLt4();
        if (CollectionUtils.isNotEmpty(list)) {
            return list;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 得到 pid 大于 0 权限集合
     *
     * @return List
     */
    @Override
    public List<RuleDO> listPidGt0() {
        return baseMapper.listPidGt0();
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据角色 id 查询 pid 大于 0 权限集合
     *
     * @param roleId BigInteger 角色id
     *
     * @return List
     */
    @Override
    public List<RuleDO> listByRoleIdAndPidGt0(@NotNull final BigInteger roleId) {
        return baseMapper.listByRoleIdAndPidGt0(roleId);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色 id 和 权限 id 批量新增 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    @Override
    public int saveBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId,
            @NotNull final BigInteger[] ruleIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("roleId", roleId);
        if (ArrayUtils.isNotEmpty(ruleIdArray)) {
            params.put("ruleIdArray", ruleIdArray);
        }
        return baseMapper.saveBatchByRoleIdAndRuleIdArray(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 和 权限id 批量删除 角色-权限 表
     *
     * @param roleId BigInteger
     * @param ruleIdArray BigInteger[]
     *
     * @return int
     */
    @Override
    public int removeBatchByRoleIdAndRuleIdArray(@NotNull final BigInteger roleId,
            @NotNull final BigInteger[] ruleIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        params.put("roleId", roleId);
        if (ArrayUtils.isNotEmpty(ruleIdArray)) {
            params.put("ruleIdArray", ruleIdArray);
        }
        return baseMapper.removeBatchByRoleIdAndRuleIdArray(params);
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 根据 角色id 批量删除 角色-权限 表
     *
     * @param roleIdArray BigInteger[]
     *
     * @return int
     */
    @Override
    public int removeByRoleIdArray(@NotNull final BigInteger[] roleIdArray) {
        Map<String, Object> params = new HashMap<>(16);
        if (ArrayUtils.isNotEmpty(roleIdArray)) {
            params.put("roleIdArray", roleIdArray);
            return baseMapper.removeByRoleIdArray(params);
        } else {
            return 0;
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End RuleServiceImpl class

/* End of file RuleServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/system/impl/RuleServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
