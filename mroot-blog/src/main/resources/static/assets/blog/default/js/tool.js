var BlogTool = function () {

    /**
     * 改变语言
     */
    var handleLanguage = function () {
        // ajax 请求 简体中文
        $('#js_language-zh').on('click', function () {
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: 'POST',
                success: function () {
                    location.reload();
                },
                error: function () {
                    EliteTool.showSweetAlertError({
                        title: AlertMessage.getMessageAlertNetworkError()
                    });
                }
            });
        });

        // -------------------------------------------------------------------------------------------------

        // ajax 请求 英文
        $('#js_language-en').on('click', function () {
            var url = $(this).attr('data-url');
            $.ajax({
                url: url,
                method: 'POST',
                success: function () {
                    location.reload();
                },
                error: function () {
                    EliteTool.showSweetAlertError({
                        title: AlertMessage.getMessageAlertNetworkError()
                    });
                }
            });
        });

        // -------------------------------------------------------------------------------------------------

    };

    return {

        // 主要模块初始化函数
        init: function () {
            handleLanguage();
        },

        // -------------------------------------------------------------------------------------------------

        // 顶部导航高亮
        highlight_top_nav: function (url) {
            $('#mainNav').find('a[href="' + url + '"]').addClass('active');
            //$('#mainNav').find('a[href="' + url + '"]').parent('li').addClass('active');
            $('#mainNav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .children('a').addClass('active');

        },

        // -------------------------------------------------------------------------------------------------

        // 左侧导航高亮
        highlight_left_nav: function (url) {
            $('#js_left-nav').find('a[href="' + url + '"]').addClass('active');
            $('#js_left-nav').find('a[href="' + url + '"]').parent('li').parent('ul').parent('li')
                .children('a').addClass('active');
        },

        // -------------------------------------------------------------------------------------------------

        /**
         * pagination 分页
         * @param options
         */
        pagination: function (options) {
            $('#paginate').pagination({
                // 页面跳转的目标位置
                url: options.url,
                // 总条数
                totalRow: options.totalRow,
                // 每页显示条数
                pageSize: options.pageSize,
                // 当前页
                pageNumber: options.pageNumber,
                // 页面跳转时需要同时传递给服务端的自定义参数设置
                params: options.params
            });
        }

        // -------------------------------------------------------------------------------------------------

    };
}();

jQuery(document).ready(function () {
    BlogTool.init()
});
