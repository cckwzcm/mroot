<#-- 表单验证国际化提示信息 -->
<script>

    /**
     * 提示信息
     */
    var BlogAlertMessage = function () {

        var message_alert_pagination_pageSize = '${I18N("message.alert.pagination.pageSize")}';
        var message_alert_pagination_totalRow = '${I18N("message.alert.pagination.totalRow")}';
        var message_alert_pagination_count = '${I18N("message.alert.pagination.count")}';

        return {

            getMessageAlertPaginationPageSize: function () {
                return message_alert_pagination_pageSize;
            },
            getMessageAlertPaginationTotalRow: function () {
                return message_alert_pagination_totalRow;
            },
            getMessageAlertPaginationCount: function () {
                return message_alert_pagination_count;
            }

            // -------------------------------------------------------------------------------------------------

        }
    }();

    // ------------------------------------------------------------------------

</script>
