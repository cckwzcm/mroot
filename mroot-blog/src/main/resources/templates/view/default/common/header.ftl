<#-- Header 开始 -->
<header id="header" class="header-transparent header-transparent-dark-bottom-border header-effect-shrink"
        data-plugin-options="{'stickyEnabled': true, 'stickyEffect': 'shrink', 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyChangeLogo': true, 'stickyStartAt': 30, 'stickyHeaderContainerHeight': 70}">
    <div class="header-body border-top-0 bg-dark box-shadow-none">
        <div class="header-container container">
            <div class="header-row">
                <div class="header-column">
                    <div class="header-row">
                        <div class="header-logo">
                            <a href="${CONTEXT_PATH}/">
                                <img alt="${I18N("message.blog.meta.base")}" width="82" height="40"
                                     src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/logo.png">
                            </a>
                        </div>
                    </div>
                </div>
                <div class="header-column justify-content-end">
                    <div class="header-row">
                        <div class="header-nav header-nav-links header-nav-dropdowns-dark header-nav-light-text order-2 order-lg-1">
                            <div class="header-nav-main header-nav-main-mobile-dark header-nav-main-square header-nav-main-dropdown-no-borders header-nav-main-effect-2 header-nav-main-sub-effect-1">
                                <nav class="collapse">

                                    <ul class="nav nav-pills" id="mainNav">
                                        <li>
                                            <a class="nav-link"
                                               href="${CONTEXT_PATH}/"><span>${I18N("message.blog.index")}</span></a>
                                        </li>
                                        <#list treeCategories as topCategory>
                                            <#if topCategory.childrenList>
                                                <li class="dropdown dropdown-primary">
                                                    <a class="dropdown-toggle nav-link"
                                                       href="javascript:void(0)"><span>${topCategory.title}</span></a>
                                                    <ul class="dropdown-menu">
                                                        <#list topCategory.childrenList as childCategory>
                                                            <li>
                                                                <a class="dropdown-item"
                                                                   href="${CONTEXT_PATH}/cms/category/cate/${childCategory.aesId}">
                                                                    ${childCategory.title}
                                                                </a>
                                                            </li>
                                                        </#list>
                                                    </ul>
                                                </li>
                                            <#else>
                                                <li>
                                                    <a class="nav-link"
                                                       href="${CONTEXT_PATH}/cms/category/cate/${topCategory.aesId}"><span>${topCategory.title}</span></a>
                                                </li>
                                            </#if>
                                        </#list>

                                        <li>
                                            <a class="nav-link"
                                               href="${CONTEXT_PATH}/about/index#19"><span>${I18N("message.blog.about")}</span></a>
                                        </li>


                                    </ul>

                                </nav>
                            </div>
                            <button class="btn header-btn-collapse-nav" data-toggle="collapse"
                                    data-target=".header-nav-main nav">
                                <i class="fas fa-bars"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<#-- Header 结束 -->
