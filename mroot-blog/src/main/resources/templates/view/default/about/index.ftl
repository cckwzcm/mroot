<#-- /* 头部标题 */ -->
<@OVERRIDE name="HEAD_TITLE">
    <#if headTitle??>
        <title>关于本站_${headTitle}</title>
    <#else>
        <title>关于本站_${I18N("message.blog.metaTitle")}</title>
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_KEYWORDS">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content??>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content}">
    <#else>
        <meta name="keywords"
              content="${I18N("message.blog.meta.base")},贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud">
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_DESCRIPTION">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content??>
        <meta name="description"
              content="${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content},${I18N("message.blog.meta.base")}">
    <#else>
        <meta name="description"
              content="IT网站,技术博客,${I18N("message.blog.meta.base")}">
    </#if>
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="row pt-1 pb-4 mb-3">

        <div class="col-lg-8">
            <h2 class="font-weight-bold text-color-dark">- 捐助说明</h2>
            <p>如果本站的内容对你有所帮助，可以进行捐助。</p>

            <div class="col">


                <ul class="nav nav-pills sort-source mb-4" data-sort-id="portfolio" data-option-key="filter"
                    data-plugin-options="{'layoutMode': 'masonry', 'filter': '*'}">

                    <li class="nav-item active" data-option-value=".19"><a
                                class="nav-link active btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">19</a></li>

                    <li class="nav-item" data-option-value=".39"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">39</a></li>

                    <li class="nav-item" data-option-value=".59"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">59</a></li>

                    <li class="nav-item" data-option-value=".99"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">99</a></li>

                    <li class="nav-item" data-option-value=".199"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">199</a></li>

                    <li class="nav-item" data-option-value=".499"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">499</a></li>

                    <li class="nav-item" data-option-value=".999"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">999</a></li>

                    <li class="nav-item" data-option-value=".1999"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">1999</a></li>

                    <li class="nav-item" data-option-value=".5000"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">5000</a></li>

                    <li class="nav-item" data-option-value=".other"><a
                                class="nav-link btn btn-outline custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase mr-2 mb-2"
                                href="javascript:void(0)">其他数额</a></li>
                </ul>

                <div class="sort-destination-loader sort-destination-loader-showing">
                    <div class="row portfolio-list sort-destination" data-sort-id="portfolio">

                        <#-- 19 -->
                        <div class="col-lg-6 isotope-item 19">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_19.png"
                                                         class="img-fluid" alt="支付宝捐助19元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助19元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 19">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_19.png"
                                                         class="img-fluid" alt="微信捐助19元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助19元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 39 -->
                        <div class="col-lg-6 isotope-item 39">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_39.png"
                                                         class="img-fluid" alt="支付宝捐助39元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助39元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 39">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_39.png"
                                                         class="img-fluid" alt="微信捐助39元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助39元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 59 -->
                        <div class="col-lg-6 isotope-item 59">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_59.png"
                                                         class="img-fluid" alt="支付宝捐助59元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助59元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 59">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_59.png"
                                                         class="img-fluid" alt="微信捐助59元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助59元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 99 -->
                        <div class="col-lg-6 isotope-item 99">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_99.png"
                                                         class="img-fluid" alt="支付宝捐助99元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助99元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 99">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_99.png"
                                                         class="img-fluid" alt="微信捐助99元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助99元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 199 -->
                        <div class="col-lg-6 isotope-item 199">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_199.png"
                                                         class="img-fluid" alt="支付宝捐助199元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助199元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 199">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_199.png"
                                                         class="img-fluid" alt="微信捐助199元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助199元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 499 -->
                        <div class="col-lg-6 isotope-item 499">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_499.png"
                                                         class="img-fluid" alt="支付宝捐助499元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助499元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 499">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_499.png"
                                                         class="img-fluid" alt="微信捐助499元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助499元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 999 -->
                        <div class="col-lg-6 isotope-item 999">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_999.png"
                                                         class="img-fluid" alt="支付宝捐助999元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助999元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 999">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_999.png"
                                                         class="img-fluid" alt="微信捐助999元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助999元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 1999 -->
                        <div class="col-lg-6 isotope-item 1999">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_1999.png"
                                                         class="img-fluid" alt="支付宝捐助1999元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助1999元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 1999">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_1999.png"
                                                         class="img-fluid" alt="微信捐助1999元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助1999元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- 5000 -->
                        <div class="col-lg-6 isotope-item 5000">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao_5000.png"
                                                         class="img-fluid" alt="支付宝捐助5000元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助5000元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item 5000">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin_5000.png"
                                                         class="img-fluid" alt="微信捐助5000元">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助5000元</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>

                        <#-- other -->
                        <div class="col-lg-6 isotope-item other">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/zhifubao.png"
                                                         class="img-fluid" alt="支付宝捐助">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">支付宝捐助</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>
                        <div class="col-lg-6 isotope-item other">
                            <a href="javascript:void(0)" class="text-decoration-none">
											<span class="thumb-info custom-thumb-info-style-1 mb-4 pb-1 thumb-info-hide-wrapper-bg">
												<span class="thumb-info-wrapper m-0">
													<img src="${GLOBAL_RESOURCE_MAP['DEFAULT']}/img/pay/weixin.png"
                                                         class="img-fluid" alt="微信捐助">
												</span>
												<span class="thumb-info-caption bg-color-secondary p-3 pt-4 pb-4">
													<span class="custom-thumb-info-title">
														<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-4">微信捐助</span>
														<span class="custom-thumb-info-desc font-weight-light">感谢有你</span>
													</span>
												</span>
											</span>
                            </a>
                        </div>


                    </div>
                </div>

            </div>
        </div>

        <div class="col-lg-8">
            <h2 class="font-weight-bold text-color-dark">- 版权说明</h2>
            <p>如果本站的内容涉及版权，请及时联系本站进行删除，给您带来的不便我们深表歉意，敬请谅解！</p>
        </div>

        <div class="col-lg-4">

            <div class="row mb-4">
                <div class="col">
                    <div class="feature-box feature-box-style-2">
                        <div class="feature-box-icon mt-1">
                            <i class="fab fa-qq"></i>
                        </div>
                        <div class="feature-box-info">
                            <h2 class="font-weight-bold text-color-dark">- QQ</h2>
                            <p class="text-4">
                                <a href="javascript:void(0)" class="text-decoration-none">707069100</a><br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                    <div class="feature-box feature-box-style-2">
                        <div class="feature-box-icon mt-1">
                            <i class="fab fa-weixin"></i>
                        </div>
                        <div class="feature-box-info">
                            <h2 class="font-weight-bold text-color-dark">- 微信</h2>
                            <p class="text-4">
                                <a href="javascript:void(0)" class="text-decoration-none">707069100</a><br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mb-4">
                <div class="col">
                    <div class="feature-box feature-box-style-2">
                        <div class="feature-box-icon mt-1">
                            <i class="icon-envelope icons"></i>
                        </div>
                        <div class="feature-box-info">
                            <h2 class="font-weight-bold text-color-dark">- Email</h2>
                            <p class="text-4">
                                <a href="javascript:void(0)" class="text-decoration-none">707069100@qq.com</a><br>
                            </p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    </div>

</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">

    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            BlogTool.highlight_top_nav('${navIndex}#19');
        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
