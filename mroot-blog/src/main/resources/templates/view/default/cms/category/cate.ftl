<#-- /* 头部标题 */ -->
<@OVERRIDE name="HEAD_TITLE">
    <title>${category.title}_${I18N("message.blog.metaTitle")}</title>
</@OVERRIDE>
<@OVERRIDE name="META_KEYWORDS">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content??>
        <meta name="keywords"
              content="${category.title},${pCategory.title},${I18N("message.blog.meta.base")},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_KEYWORDS'].content}">
    <#else>
        <meta name="keywords"
              content="${category.title},${pCategory.title},${I18N("message.blog.meta.base")},贰阳,开发者,源码,IT网站,技术博客,分享,Java,JavaWeb,Spring,Spring Boot,Spring Cloud">
    </#if>
</@OVERRIDE>
<@OVERRIDE name="META_DESCRIPTION">
    <#if GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content??>
        <meta name="description"
              content="${category.title},${pCategory.title},${GLOBAL_DATABASE_CONFIG_MAP['BLOG_INDEX_META_DESCRIPTION'].content},${I18N("message.blog.meta.base")}">
    <#else>
        <meta name="description"
              content="${category.title},${pCategory.title},IT网站,技术博客,${I18N("message.blog.meta.base")}">
    </#if>
</@OVERRIDE>
<#-- /* 主要部分 */ -->
<@OVERRIDE name="MAIN_CONTENT">

    <div class="row">

        <@listPage></@listPage>

    </div>

</@OVERRIDE>

<@OVERRIDE name="PAGE_SCRIPT">
    <#include "/default/scriptplugin/pagination.ftl">
</@OVERRIDE>

<@OVERRIDE name="CUSTOM_SCRIPT">

    <script>
        jQuery(document).ready(function () {
            // 顶部导航高亮
            BlogTool.highlight_top_nav('${navIndex}');

            // 分页
            BlogTool.pagination({
                url: '${index}',
                totalRow: '${page.total}',
                pageSize: '${page.size}',
                pageNumber: '${page.current}',
                params: function () {
                }
            });

        });
    </script>
<#-- /* /.页面级别script结束 */ -->
</@OVERRIDE>

<@EXTENDS name="/default/common/base.ftl"/>
