/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.constant;


import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

/**
 * 系统配置文件 页面变量名称使用
 *
 * @author ErYang
 */
@Component
@PropertySource("classpath:config.properties")
@ConfigurationProperties(prefix = "config")
@EnableCaching
@Getter
@Setter
public class ConfigConst {

    /**
     * 模式变量名称
     */
    private String profileName;
    /**
     * 地址变量名称
     */
    private String contextPathName;
    /**
     * 端口号变量名称
     */
    private String serverPortName;
    /**
     * 首页地址变量名称
     */
    private String blogIndexUrlName;
    /**
     * 改变语言
     */
    private String blogLanguageUrlName;
    /**
     * 后台静态文件地址变量名称
     */
    private String blogStaticPathUrlName;
    /**
     * i18n变量名称
     */
    private String i18nName;
    /**
     * freemarker 共享对象变量名称
     */
    private String freemarkerSharedVariableBlockName;
    private String freemarkerSharedVariableOverrideName;
    private String freemarkerSharedVariableExtendsName;
    /**
     * 分页显示条数
     */
    private String blogPageSize;
    /**
     * 国际化语言名称
     */
    private String i18nLanguageName;

    /**
     * 数据库系统配置信息集合名称
     */
    private String databaseConfigMapName;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ConfigConst class

/* End of file ConfigConst.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/constant/ConfigConst.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
