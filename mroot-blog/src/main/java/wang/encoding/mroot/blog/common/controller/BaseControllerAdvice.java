/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.controller;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.constant.ResourceConst;
import wang.encoding.mroot.common.business.GlobalMessage;
import wang.encoding.mroot.common.business.ResultData;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.constant.ShareConst;
import wang.encoding.mroot.common.util.HttpRequestUtils;

import javax.servlet.http.HttpServletRequest;

/**
 * Controller 增强器
 *
 * @author ErYang
 */
@ControllerAdvice
@Slf4j
public class BaseControllerAdvice {


    /**
     * 默认视图目录
     */
    private static final String DEFAULT_VIEW = "/default";

    /**
     * 错误视图
     */
    private static final String ERROR_EXCEPTION_VIEW = "/error/500";

    /**
     * 错误视图 开发环境
     */
    private static final String DEV_ERROR_EXCEPTION_VIEW = "/error/5002dev";

    private final ShareConst shareConst;


    private final ResourceConst resourceConst;

    @Autowired
    public BaseControllerAdvice(ShareConst shareConst, ResourceConst resourceConst) {
        this.shareConst = shareConst;
        this.resourceConst = resourceConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 应用到所有 @RequestMapping 注解方法 在其执行之前初始化数据绑定器
     *
     * @param binder WebDataBinder
     */
    @InitBinder
    public void initBinder(final WebDataBinder binder) {
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 拦截捕捉自定义异常 BaseException.class
     *
     * @param request HttpServletRequest
     * @param exception BaseException
     * @return Object
     */
    @ResponseBody
    @ExceptionHandler(value = {RuntimeException.class})
    public Object baseExceptionHandler(final HttpServletRequest request, final RuntimeException exception) {
        if (logger.isErrorEnabled()) {
            if (null != exception.getMessage()) {
                logger.error(exception.getMessage(), exception.getCause());
            } else {
                logger.error(">>>>>>>>得到抛出的异常信息<<<<<<<<", exception.getCause());
            }
        }
        // ajax 请求
        if (HttpRequestUtils.isAjaxRequest(request)) {
            ResultData result = ResultData.error();
            result.setError().set(GlobalMessage.MESSAGE, GlobalMessage.EXCEPTION_INFO);
            result.toFastJson();
            return result;
        } else {
            // 跳转页面
            ModelAndView modelAndView = new ModelAndView();
            if (ProfileComponent.isDevProfile()) {
                // 开发环境
                modelAndView.setViewName(this.getCurrentThemePath() + DEV_ERROR_EXCEPTION_VIEW);
                if (null != exception.getMessage() && StringUtils.isNotBlank(exception.getMessage())) {
                    modelAndView.addObject(shareConst.getExceptionCode(), exception.getMessage());
                }
                if (null != exception.getCause()) {
                    modelAndView.addObject(shareConst.getExceptionMessage(), exception.getCause());
                }
            } else {
                modelAndView.setViewName(this.getCurrentThemePath() + ERROR_EXCEPTION_VIEW);
            }
            // 上一页地址
            String url = request.getHeader("Referer");
            if (StringUtils.isNotBlank(url)) {
                modelAndView.addObject("refererUrl", url);
            }
            return modelAndView;
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 获得当前视图主题
     * @return String
     */
    private String getCurrentThemePath() {
        // 默认主题
        if (resourceConst.getCurrentTheme().equalsIgnoreCase(ResourceConst.DEFAULT_THEME)) {
            return DEFAULT_VIEW;
        } else {
            return DEFAULT_VIEW;
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseControllerAdvice class

/* End of file BaseControllerAdvice.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/common/controller/BaseControllerAdvice.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
