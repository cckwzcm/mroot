/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.interceptor;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.support.RequestContextUtils;
import wang.encoding.mroot.blog.common.constant.ConfigConst;
import wang.encoding.mroot.blog.common.task.BlogControllerAsyncTask;
import wang.encoding.mroot.blog.common.task.BlogControllerAsyncTaskResultUtils;
import wang.encoding.mroot.common.constant.CacheNameConst;
import wang.encoding.mroot.common.redis.util.RedisUtils;
import wang.encoding.mroot.common.util.HttpRequestUtils;
import wang.encoding.mroot.common.util.collection.ListUtils;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.vo.blog.entity.cms.category.BlogCategoryGetVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;
import java.util.concurrent.Future;

/**
 * 默认拦截器
 *
 * @author ErYang
 */
@Component
@Slf4j
public class BaseInterceptor implements HandlerInterceptor {


    /**
     * .标识
     */
    private static final String DOT_IDENTIFY = ".";
    /**
     * .标识
     */
    private static final String CHAR = "UTF-8";


    private final ConfigConst configConst;

    private final BlogCategoryService categoryService;

    private final BlogControllerAsyncTask blogControllerAsyncTask;

    private final BlogControllerAsyncTaskResultUtils blogControllerAsyncTaskResultUtils;

    @Autowired
    public BaseInterceptor(ConfigConst configConst, BlogCategoryService categoryService,
            BlogControllerAsyncTask blogControllerAsyncTask,
            BlogControllerAsyncTaskResultUtils blogControllerAsyncTaskResultUtils) {
        this.configConst = configConst;
        this.categoryService = categoryService;
        this.blogControllerAsyncTask = blogControllerAsyncTask;
        this.blogControllerAsyncTaskResultUtils = blogControllerAsyncTaskResultUtils;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {

        response.setCharacterEncoding(CHAR);
        String uri = request.getRequestURI();

        // 静态请求直接跳出
        if (uri.contains(DOT_IDENTIFY)) {
            return true;
        }
        if (HttpRequestUtils.isAjaxRequest(request)) {
            return true;
        }
        if (logger.isDebugEnabled()) {
            logger.debug(">>>>>>>>BaseInterceptor在请求[{}]处理之前" + "进行调用(Controller方法调用之前)<<<<<<<<", uri);
        }

        // 从 redis 缓存中 是否 重置数据库系统配置
        String config = String.valueOf(RedisUtils.getString(
                CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                        + CacheNameConst.COMMON_RELOAD_DATABASE_CONFIG));
        if (StringUtils.isNotBlank(config) && config.equalsIgnoreCase(CacheNameConst.COMMON_RELOAD_YES)) {
            // 重置数据库系统配置
            // 数据库系统配置
            Future<String> asyncResult = blogControllerAsyncTask.reloadConfig();
            blogControllerAsyncTaskResultUtils.doBlogAsyncResult(asyncResult, "博客系统数据库系统配置");
            //  redis 缓存中 的标识改为 no
            RedisUtils.setString(CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                    + CacheNameConst.COMMON_RELOAD_DATABASE_CONFIG, CacheNameConst.COMMON_RELOAD_NO);
        }

        // 从 redis 缓存中 是否 重置资源信息
        String resource = String.valueOf(RedisUtils.getString(
                CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                        + CacheNameConst.COMMON_RELOAD_RESOURCE));
        if (StringUtils.isNotBlank(resource) && resource.equalsIgnoreCase(CacheNameConst.COMMON_RELOAD_YES)) {
            // 重置资源信息
            // 资源信息
            Future<String> asyncResult = blogControllerAsyncTask.reloadResource();
            blogControllerAsyncTaskResultUtils.doBlogAsyncResult(asyncResult, "博客系统资源信息");
            //  redis 缓存中 的标识改为 no
            RedisUtils.setString(CacheNameConst.COMMON_PREFIX + CacheNameConst.REDIS_CACHE_SEPARATOR
                    + CacheNameConst.COMMON_RELOAD_RESOURCE, CacheNameConst.COMMON_RELOAD_NO);
        }

        // 文章分类 数据
        List<BlogCategoryGetVO> list = categoryService.listTypeLt3();
        if (ListUtils.isNotEmpty(list)) {
            request.setAttribute("treeCategories", list);
        }
        // 只有返回true才会继续向下执行，返回false取消当前请求
        return true;
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void postHandle(HttpServletRequest request, HttpServletResponse response, Object handler,
            ModelAndView modelAndView) {
        // 国际化
        Optional<LocaleResolver> localeResolverOptional = Optional
                .ofNullable(RequestContextUtils.getLocaleResolver(request));
        if (localeResolverOptional.isPresent()) {
            LocaleResolver localeResolver = localeResolverOptional.get();
            String i18n = localeResolver.resolveLocale(request).getLanguage();
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>BaseInterceptor国际化[{}]<<<<<<<<", i18n);
            }
            request.setAttribute(configConst.getI18nLanguageName(), i18n);
        }
        String uri = request.getRequestURI();
        // 静态请求直接跳出
        if (!uri.contains(DOT_IDENTIFY) && !HttpRequestUtils.isAjaxRequest(request)) {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>BaseInterceptor请求[{}]处理之后进行调用," + "但是在视图被渲染之前(Controller方法调用之后)调用<<<<<<<<", uri);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler,
            Exception ex) {
        String uri = request.getRequestURI();
        // 静态请求直接跳出
        if (!uri.contains(DOT_IDENTIFY) && !HttpRequestUtils.isAjaxRequest(request)) {
            if (logger.isDebugEnabled()) {
                logger.debug(">>>>>>>>BaseInterceptor在整个请求[{}]结束之后被调用,"
                        + "也就是在DispatcherServlet渲染了对应的视图之后执行(主要是用于进行资源清理工作)<<<<<<<<", uri);
            }
        }
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BaseInterceptor class

/* End of file BaseInterceptor.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/interceptor/BaseInterceptor.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
