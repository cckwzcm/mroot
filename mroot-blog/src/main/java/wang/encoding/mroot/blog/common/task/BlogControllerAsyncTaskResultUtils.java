/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http;//encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系;   <707069100@qq.com>      <http;//weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.common.task;


import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.ProfileComponent;
import wang.encoding.mroot.common.enums.MailPatternEnum;
import wang.encoding.mroot.common.enums.MailSendResultEnum;
import wang.encoding.mroot.common.enums.MailTypeEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.util.concurrent.ThreadUtils;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.common.util.net.NetUtils;
import wang.encoding.mroot.common.util.time.ClockUtils;
import wang.encoding.mroot.plugin.mail.constant.MailConst;
import wang.encoding.mroot.plugin.mail.constant.MailTemplateConst;
import wang.encoding.mroot.service.blog.system.BlogMailLogService;
import wang.encoding.mroot.vo.blog.entity.system.maillog.BlogMailLogGetVO;


import java.time.Instant;
import java.util.Date;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Future;

/**
 * 异步调用
 *
 * @author ErYang
 */
@Slf4j
@Component
public class BlogControllerAsyncTaskResultUtils {


    private final MailTemplateConst mailTemplateConst;

    private final BlogMailLogService blogMailLogService;

    private final BlogControllerAsyncTask blogControllerAsyncTask;

    private final MailConst mailConst;

    @Autowired
    public BlogControllerAsyncTaskResultUtils(MailTemplateConst mailTemplateConst,
            BlogMailLogService blogMailLogService, BlogControllerAsyncTask blogControllerAsyncTask,
            MailConst mailConst) {
        this.mailTemplateConst = mailTemplateConst;
        this.blogMailLogService = blogMailLogService;
        this.blogControllerAsyncTask = blogControllerAsyncTask;
        this.mailConst = mailConst;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 处理异步返回结果
     *
     * @param asyncResult Future
     * @param title String
     */
    @Async("blogThreadPoolTaskExecutor")
    public void doBlogAsyncResult(@NotNull final Future<String> asyncResult, @NotNull final String title) {
        long startTime = ClockUtils.currentTimeMillis();
        // 每隔 2000 ms 执行一次 判断一下这个异步调用的方法是否全都执行完了
        // 如果异步方法全部执行完 跳出循环
        // 使用 Future 的 isDone() 方法返回该方法是否执行完成
        while (!asyncResult.isDone()) {
            ThreadUtils.sleep(2000);
        }
        // 异常情况
        boolean executionFlag = false;
        String result;
        try {
            result = asyncResult.get();
        } catch (InterruptedException | ExecutionException e) {
            if (logger.isErrorEnabled()) {
                logger.error(">>>>>>>>获取[{}]执行结果出现异常<<<<<<<<", title, e);
            }
            executionFlag = true;
            result = "获取[" + title + "]执行结果出现异常";
        }
        // 发送邮件
        String content = String
                .format(mailTemplateConst.getSystemMonitoringContent(), title, ClockUtils.elapsedTime(startTime),
                        result);
        // 异常情况
        if (executionFlag) {
            this.sendSimpleEmail(mailTemplateConst.getToMail(), mailTemplateConst.getSystemMonitoringTitle(), content);
        }
        if (ProfileComponent.isDevProfile()) {
            // 开发环境
            this.sendSimpleEmail(mailTemplateConst.getToMail(), mailTemplateConst.getSystemMonitoringTitle(), content);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 发送简单邮箱
     * @param toMail String 接收邮箱
     * @param title String 标题
     * @param content String 内容
     */
    private void sendSimpleEmail(@NotNull final String toMail, @NotNull final String title,
            @NotNull final String content) {
        if (null != MailConst.openMail && MailConst.openMail) {
            BlogMailLogGetVO addMailLogGetVO = new BlogMailLogGetVO();
            addMailLogGetVO.setToMail(toMail);
            addMailLogGetVO.setTitle(title);
            addMailLogGetVO.setContent(content);
            long startTime = ClockUtils.currentTimeMillis();
            this.initBlogMailLogGetVOData(addMailLogGetVO);
            blogControllerAsyncTask.sendSimpleEmail(toMail, title, content);
            addMailLogGetVO.setTimes(Math.toIntExact(ClockUtils.elapsedTime(startTime)));
            blogMailLogService.save(addMailLogGetVO);
        }
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 构建  BlogMailLogGetVO
     *
     * @param addMailLogGetVO BlogMailLogGetVO
     */
    private void initBlogMailLogGetVOData(@NotNull final BlogMailLogGetVO addMailLogGetVO) {
        addMailLogGetVO.setCategory(MailTypeEnum.SYSTEM_MONITORING.getKey());
        addMailLogGetVO.setCategoryDescription(MailTypeEnum.SYSTEM_MONITORING.getValue());
        addMailLogGetVO.setPattern(MailPatternEnum.SIMPLE.getKey());
        addMailLogGetVO.setPatternDescription(MailPatternEnum.SIMPLE.getValue());
        addMailLogGetVO.setFromMail(mailConst.getFromMailAddress());
        addMailLogGetVO.setTitle(mailTemplateConst.getTestTitle());
        addMailLogGetVO.setSendResult(MailSendResultEnum.SEND_SUCCEED.getValue());
        // IP
        addMailLogGetVO.setGmtCreateIp(IpUtils.ipv4StringToInt(NetUtils.getLocalHost()));
        addMailLogGetVO.setState(StateEnum.NORMAL.getKey());
        addMailLogGetVO.setGmtCreate(Date.from(Instant.now()));
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogControllerAsyncTaskResultUtils class

/* End of file BlogControllerAsyncTaskResultUtils.java */
/* Location; ./src/main/java/wang/encoding/mroot/blog/common/task/BlogControllerAsyncTaskResultUtils.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
