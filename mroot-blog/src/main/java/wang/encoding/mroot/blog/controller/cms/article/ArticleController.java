/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.blog.controller.cms.article;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import wang.encoding.mroot.blog.common.controller.BlogBaseController;
import wang.encoding.mroot.common.component.AesManageComponent;
import wang.encoding.mroot.common.enums.BooleanEnum;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.exception.ControllerException;
import wang.encoding.mroot.common.util.number.BigIntegerUtils;
import wang.encoding.mroot.common.util.number.NumberUtils;
import wang.encoding.mroot.service.blog.cms.BlogArticleContentService;
import wang.encoding.mroot.service.blog.cms.BlogArticleService;
import wang.encoding.mroot.service.blog.cms.BlogCategoryService;
import wang.encoding.mroot.vo.blog.entity.cms.article.BlogArticleGetVO;
import wang.encoding.mroot.vo.blog.entity.cms.articlecontent.BlogArticleContentGetVO;
import wang.encoding.mroot.vo.blog.entity.cms.category.BlogCategoryGetVO;


import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;


/**
 * 博客 文章 控制器
 *
 *@author ErYang
 */
@RestController
@RequestMapping("/cms/article")
public class ArticleController extends BlogBaseController {


    /**
     * 模块
     */
    private static final String MODULE_NAME = "/cms/article";
    /**
     * 视图目录
     */
    private static final String VIEW_PATH = "/cms/article";
    /**
     * 对象名称
     */
    private static final String VIEW_MODEL_NAME = "article";
    /**
     * 查看
     */
    private static final String VIEW = "/view";
    private static final String VIEW_VIEW = VIEW_PATH + VIEW;

    /**
     * 模块
     */
    private static final String CATEGORY_MODULE_NAME = "/cms/category";
    private static final String CATE = "/cate";
    private static final String CATE_URL = CATE;

    private final BlogArticleService articleService;
    private final BlogCategoryService categoryService;
    private final BlogArticleContentService articleContentService;

    @Autowired
    public ArticleController(BlogArticleService articleService, BlogCategoryService categoryService,
            BlogArticleContentService articleContentService) {
        this.articleService = articleService;
        this.categoryService = categoryService;
        this.articleContentService = articleContentService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 查看页面
     *
     * @param id                 String
     * @return ModelAndView
     */
    @RequestMapping(VIEW + "/{id}")
    public ModelAndView view(@PathVariable(ID_NAME) String id) throws ControllerException {
        ModelAndView modelAndView = super.initModelAndView(VIEW_VIEW);
        // 验证数据
        if (StringUtils.isBlank(id)) {
            return initErrorRedirectUrl();
        }
        // 解密
        String idStr = AesManageComponent.decrypt(id);
        if (!NumberUtils.isNumber(idStr)) {
            return initErrorRedirectUrl();
        }
        BigInteger idValue = new BigInteger(idStr);
        // 数据真实性
        BlogArticleGetVO articleGetVO = articleService.getById(idValue);
        if (null == articleGetVO || StateEnum.NORMAL.getKey() != articleGetVO.getState()
                || BooleanEnum.YES.getKey() != articleGetVO.getDisplay()) {
            return initErrorRedirectUrl();
        }
        modelAndView.addObject(VIEW_MODEL_NAME, articleGetVO);
        // 文章内容
        BlogArticleContentGetVO articleContentGetVO = articleContentService.getById(idValue);
        modelAndView.addObject("articleContent", articleContentGetVO);
        // 文章分类
        BlogCategoryGetVO categoryGetVO = categoryService.getById(articleGetVO.getCategoryId());
        modelAndView.addObject("category", categoryGetVO);
        BlogCategoryGetVO pCategory = categoryService.getById(categoryGetVO.getPid());
        if (null != pCategory) {
            modelAndView.addObject("pCategory", pCategory);
        }

        // 最新文章
        BlogArticleGetVO newArticleGetVO = new BlogArticleGetVO();
        newArticleGetVO.setState(StateEnum.NORMAL.getKey());
        newArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
        List<BlogArticleGetVO> newList = articleService.list(newArticleGetVO, 5, BlogArticleGetVO.ID, false);
        modelAndView.addObject("newList", newList);

        // 最热文章
        BlogArticleGetVO hotArticleGetVO = new BlogArticleGetVO();
        hotArticleGetVO.setState(StateEnum.NORMAL.getKey());
        hotArticleGetVO.setDisplay(BooleanEnum.YES.getKey());
        List<BlogArticleGetVO> hotList = articleService.list(hotArticleGetVO, 5, BlogArticleGetVO.PAGE_VIEW, false);
        modelAndView.addObject("hotList", hotList);

        // 更新阅读量
        BlogArticleGetVO noCacheArticle = articleService.getByIdNoCache(articleGetVO.getId());
        BlogArticleGetVO articleGetVOUpdate = new BlogArticleGetVO();
        articleGetVOUpdate.setId(noCacheArticle.getId());
        articleGetVOUpdate.setPageView(noCacheArticle.getPageView() + 1);
        articleService.update(articleGetVOUpdate);

        modelAndView.addObject("pageView", articleGetVOUpdate.getPageView());
        // 是的状态
        modelAndView.addObject("booleanYes", BooleanEnum.YES.getKey());
        modelAndView.addObject(NAV_INDEX_URL_NAME,
                contextPath + CATEGORY_MODULE_NAME + CATE_URL + "/" + categoryGetVO.getAesId());
        return modelAndView;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End ArticleController class

/* End of file ArticleController.java */
/* Location: ./src/main/java/wang/encoding/mroot/blog/controller/cms/article/ArticleController.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
