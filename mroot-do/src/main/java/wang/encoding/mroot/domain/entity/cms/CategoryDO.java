/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.cms;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;
import java.math.BigInteger;
import java.util.Date;


/**
 * 后台文章分类实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("cms_category")
public class CategoryDO extends Model<CategoryDO> implements Serializable {


    private static final long serialVersionUID = -5043320872715108739L;
    /**
     * ID
     */
    @TableId(value = "id", type = IdType.AUTO)
    private BigInteger id;
    /**
     * 父级分类
     */
    private BigInteger pid;
    /**
     * 1主分类,2次分类,3分类
     */
    private Integer category;
    /**
     * 标识
     */
    private String sole;
    /**
     * 名称
     */
    private String title;
    /**
     * 发布的文章是否需要审核,1需要,2不需要
     */
    private Integer audit;
    /**
     * 是否允许发布内容,1允许,2不允许
     */
    private Integer allow;
    /**
     * 是否前台可见,1前后台都可见,2后台可见
     */
    private Integer display;
    /**
     * 排序
     */
    private Integer sort;
    /**
     * 状态(1是正常,2是禁用,3是删除)
     */
    private Integer state;
    /**
     * 备注
     */
    private String remark;
    /**
     * 创建时间
     */
    private Date gmtCreate;
    /**
     * 创建IP
     */
    private Integer gmtCreateIp;
    /**
     * 修改时间
     */
    private Date gmtModified;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End CategoryDO class

/* End of file CategoryDO.java */
/* Location: ./src/main/java/wang/encoding/mroot/domain/entity/cms/CategoryDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
