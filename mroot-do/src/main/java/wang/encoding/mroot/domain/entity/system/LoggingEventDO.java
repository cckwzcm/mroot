/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.domain.entity.system;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


import java.io.Serializable;
import java.math.BigInteger;


/**
 * 后台logback日志实体类
 *
 * @author ErYang
 */
@Getter
@Setter
@ToString
@EqualsAndHashCode(callSuper = false)
@TableName("logging_event")
public class LoggingEventDO extends Model<LoggingEventDO> implements Serializable {


    private static final long serialVersionUID = -2188343447604984140L;

    /**
     * 日志ID
     */
    @TableId(value = "event_id", type = IdType.AUTO)
    private BigInteger eventId;
    /**
     * 类型
     */
    private String levelString;
    /**
     * 文件名称
     */
    private String callerFilename;
    /**
     * 日志名称
     */
    private String loggerName;
    /**
     * 线程名称
     */
    private String threadName;
    /**
     * 类名
     */
    private String callerClass;
    /**
     * 方法
     */
    private String callerMethod;
    /**
     * 参数
     */
    private String arg0;
    /**
     * 参数
     */
    private String arg1;
    /**
     * 参数
     */
    private String arg2;
    /**
     * 参数
     */
    private String arg3;
    /**
     * 日志信息
     */
    private String formattedMessage;
    /**
     * 引用
     */
    private String referenceFlag;
    /**
     * 代码行
     */
    private String callerLine;
    /**
     * 创建时间
     */
    private BigInteger timestmp;

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End LoggingEventDO class

/* End of file LoggingEventDO.java */
/* Location: ./src/main/java/wang/encoding/mroot/domain/entity/system/LoggingEventDO.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
