/*
 * // +-------------------------------------------------------------------------------------------------
 * // |                 有你就好 [ 有节骨乃坚，无心品自端 ]     <http://encoding.wang>
 * // +-------------------------------------------------------------------------------------------------
 * // |                             独在异乡为异客         每逢佳节倍思亲
 * // +-------------------------------------------------------------------------------------------------
 * // |                 联系:   <707069100@qq.com>      <http://weibo.com/513778937>
 * // +-------------------------------------------------------------------------------------------------
 */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                   ErYang出品 属于小极品          共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------


package wang.encoding.mroot.service.blog.system.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Service;
import wang.encoding.mroot.common.annotation.NotNull;
import wang.encoding.mroot.common.component.BeanMapperComponent;
import wang.encoding.mroot.common.enums.StateEnum;
import wang.encoding.mroot.common.enums.UserTypeEnum;
import wang.encoding.mroot.common.util.net.IpUtils;
import wang.encoding.mroot.domain.entity.system.UserDO;
import wang.encoding.mroot.service.blog.system.BlogUserService;
import wang.encoding.mroot.service.system.UserService;
import wang.encoding.mroot.vo.blog.entity.system.user.BlogUserGetVO;

import java.math.BigInteger;

/**
 * 博客 用户 Service 实现类
 *
 * @author ErYang
 */
@Service
public class BlogUserServiceImpl implements BlogUserService {

    private final UserService userService;


    @Autowired
    @Lazy
    public BlogUserServiceImpl(UserService userService) {
        this.userService = userService;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * 通过 ID 查询
     * @param id BigInteger
     * @return BlogUserGetVO
     */
    @Override
    public BlogUserGetVO getById(@NotNull final BigInteger id) {
        UserDO userDO = userService.getTById(id);
        if (null != userDO) {
            BlogUserGetVO userGetVO = this.userDO2BlogUserGetVO(userDO);
            userGetVO.setIp(IpUtils.intToIpv4String(userGetVO.getGmtCreateIp()));
            return userGetVO;
        }
        return null;
    }

    // -------------------------------------------------------------------------------------------------

    /**
     * UserDO 转为 BlogUserGetVO
     *
     * @param userDO UserDO
     * @return BlogUserGetVO
     */
    private BlogUserGetVO userDO2BlogUserGetVO(@NotNull final UserDO userDO) {
        BlogUserGetVO userGetVO = BeanMapperComponent.map(userDO, BlogUserGetVO.class);
        if (null != userGetVO.getState()) {
            userGetVO.setStatus(StateEnum.getValueByKey(userGetVO.getState()));
        }
        if (null != userGetVO.getCategory()) {
            userGetVO.setType(UserTypeEnum.getValueByKey(userGetVO.getCategory()));
        }
        return userGetVO;
    }

    // -------------------------------------------------------------------------------------------------

}

// -----------------------------------------------------------------------------------------------------

// End BlogUserServiceImpl class

/* End of file BlogUserServiceImpl.java */
/* Location: ./src/main/java/wang/encoding/mroot/service/blog/system/impl/BlogUserServiceImpl.java */

// -----------------------------------------------------------------------------------------------------
// +----------------------------------------------------------------------------------------------------
// |                           ErYang出品 属于小极品  O(∩_∩)O~~   共同学习    共同进步
// +----------------------------------------------------------------------------------------------------
// -----------------------------------------------------------------------------------------------------
